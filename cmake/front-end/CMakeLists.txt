# Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 2.8)
project(vxl C)

set(VXL_SOURCE_DIR ${PROJECT_SOURCE_DIR}/../../src)
set(VXL_FRONT-END_SOURCE_DIR ${VXL_SOURCE_DIR}/front-end)

include_directories(${VXL_SOURCE_DIR} ${VXL_FRONT-END_SOURCE_DIR})

################################################################################
# Configure and define targets
################################################################################
set(VXL_FILES_SRC vxl_scene.c vxl_system.c)
set(VXL_FILES_INC vxl_c.h vxl_system_c.h)
set(VXL_FILES_INC_API vxl.h)
set(VXL_FILES_DOC COPYING README.md)

rcmake_prepend_path(VXL_FILES_SRC ${VXL_FRONT-END_SOURCE_DIR})
rcmake_prepend_path(VXL_FILES_INC ${VXL_FRONT-END_SOURCE_DIR})
rcmake_prepend_path(VXL_FILES_INC_API ${VXL_FRONT-END_SOURCE_DIR})
rcmake_prepend_path(VXL_FILES_DOC ${PROJECT_SOURCE_DIR}/../../)

add_library(vxl SHARED ${VXL_FILES_SRC} ${VXL_FILES_INC} ${VXL_FILES_INC_API})
set_target_properties(vxl PROPERTIES
  DEFINE_SYMBOL VXL_SHARED_BUILD
   COMPILE_FLAGS ${OpenMP_C_FLAGS}
  LINK_FLAGS ${OpenMP_C_FLAGS}
  VERSION ${VXL_VERSION}
  SOVERSION ${VXL_VERSION_MAJOR})
target_link_libraries(vxl m RSys vxl-be)

rcmake_setup_devel(vxl VxL ${VXL_VERSION} vx/vxl_version.h)

################################################################################
# Add tests
################################################################################
if(NOT NO_TEST)
  macro(build_test _name)
    add_executable(${_name}
      ${VXL_SOURCE_DIR}/${_name}.c
      ${VXL_SOURCE_DIR}/test_vxl_utils.h)
    target_link_libraries(${_name} vxl)
  endmacro(build_test)

  macro(new_test _name)
    build_test(${_name})
    add_test(${_name} ${_name})
  endmacro(new_test)

  build_test(test_vxl_ray_trace)
  add_test(test_vxl_ray_trace_incore test_vxl_ray_trace incore)
  add_test(test_vxl_ray_trace_oocore test_vxl_ray_trace oocore)

  build_test(test_vxl_scene)
  add_test(test_vxl_scene_incore test_vxl_ray_trace incore)
  add_test(test_vxl_scene_oocore test_vxl_ray_trace oocore)

  build_test(test_vxl_ambient_occlusion)
  add_test(test_vxl_ambient_occlusion_incore test_vxl_ambient_occlusion incore)
  add_test(test_vxl_ambient_occlusion_oocore test_vxl_ambient_occlusion oocore)

  new_test(test_vxl_system)
  set_target_properties(test_vxl_system PROPERTIES LINK_FLAGS ${OpenMP_C_FLAGS})

endif(NOT NO_TEST)

################################################################################
# Define output & install directories
################################################################################
install(TARGETS vxl
  ARCHIVE DESTINATION bin
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)
install(FILES ${VXL_FILES_INC_API} DESTINATION include/vx)
install(FILES ${VXL_FILES_DOC} DESTINATION share/doc/vxl/)

