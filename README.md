# VoXel Library

This library manages a set of volumetric elements, i.e. voxels, storing a set
of attributes. A voxel is actually a geometrical primitive that once combined
with several voxels can easily define complex shapes. The target users of VxL
are thus low level software engineers that have to define and access
efficiently to detailed geometries, whose the data and the topology can be
highly heterogeneous.

VxL implements a voxelization process that generates voxels from triangular
meshes. Each emitted voxel is a cell intersected by at least one triangle, of
an axis aligned grid surrounding the shapes whose definition is defined by the
user. Aside from implicitly encoding the position of the triangles that it
intersects, a voxel saves the average of their color and their normal. For a
given position onto a triangle, the value of these aforementioned attributes
are retrieved from the host application with callbacks. The user can thus
define a per triangle color and normal - or shade them spatially onto a same
primitive with normal maps, textures or procedural shaders - independently of
VxL.

The generated voxels are then partitioned into a sparse octree, i.e. an octree
where only not empty nodes are explicitly stored. This sparse hierarchical
data structure not only speeds up the access to a specific voxel but also
naturally encodes the level of details of the generated voxels. VxL provides
several ray tracing kernels inside this Sparse Voxel Octree (SVO), optimized
for incoherent and coherent workloads. In addition of discreet ray-tracing,
rays with differential directions are also supported allowing to stop the SVO
traversal when the ray footprint is greater than the intersected octree node.

The hundreds of millions of voxels required to represent a complex data set is
particularly challenging for the memory sub-system. The overall number of
voxels that can be efficiently managed is thus highly dependent on available
system memory. Anyway, VxL supports out-of-core voxelization and ray tracing to
process data that are too large to fit in the main memory.

## How to build

VxL is a C89 library compatible with GNU/Linux 64-bits. It was successfully
built on GCC. It relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also depends on
the [RSys](https://gitlab.com/vaplv/rsys/) and the
[RSIMD](https://gitlab.com/vaplv/rsimd/) libraries. Finally, it uses
[OpenMP](http://openmp.org) to parallelize some processes while the SSE2
instructions set is used to optimize some routines.

First ensure that GCC 4.7 or higher and CMake 2.8 or higher are installed on your
system. Then install the RCMake package, the RSys and RSIMD libraries.
Generate the project from the cmake/CMakeLists.txt file by appending
the RCMake, RSys and RSIMD install directories to the `CMAKE_PREFIX_PATH`
variable. Finally, use CMake to edit, build, test and install VxL
as any CMake project. Refer to the CMake
[documentation](https://cmake.org/documentation/) for more informations.

    ~ $ git clone https://gitlab.com/vaplv/vxl.git
    ~ $ cd vxl
    ~ vxl $ mkdir build; cd build
    ~ vxl/build $ cmake ../cmake \
    > -DCMAKE_PREFIX_PATH="<RCMAKE_INSTALL_PATH>;<RSYS_INSTALL_PATH>;<RSIMD_INSTALL_PATH>" \
    > -DCMAKE_INSTALL_PATH=<VXL_INSTALL_PATH>
    ~ vxl/build $ make; make install

## Release notes

### Version 0.5

- Add out-of-core voxelization. The memory location parameter of the
  `vxl_scene_setup` function controls where scene voxels are stored: in core or
  out-of-core. The performance penalty of the out-of-core voxelization is quite
  low, roughly null or up to ~30% depending on the hard-drive efficiency.
- Add out-of-core ray tracing. The provided implementation maps the whole
  out-of-core data in the user space memory and let the kernel load/unload them
  dynamically.
- Reduce the memory consumption at the setup of the scene: emitted voxels are
  directly registered into the SVO rather than stored in an intermediary pool
  of voxels.

### Version 0.4

- Partition the SVO data in fixed size memory blocks.
- Store the attributes only for non empty nodes. Previously, partially empty
  node stored the attributes for all its children, even for its empty child
  nodes.
- Reduce the memory footprint of the voxel normal by quantifying it on 32-bits.
- Reduce the memory requirements during the SVO build process: submitted voxels
  are freed once registered into the SVO.
- Parallelize the building of the SVO.
- Add to the `vxl_system_create` API a parameter on how many threads VxL should
  use. This threads count is truncated to the number of available processors
  returned by the `omp_get_num_procs` directive. The constant
  `VXL_NTHREADS_DEFAULT` forces VxL to use as many threads as available
  processors.

### Version 0.3

- Remove the useless and time consuming voxel pool instantiation functionality.
- Full rewrite and parallelise the voxelization algorithm to improve the
  previous awfull performances by an order of magnitude.
- Improve the SVO building time up to two orders of magnitude.
- Reduce the memory consumption of the SVO building process and purge its
  intermediary data structure to reduce the memory footprint of VxL.

## License

VxL is Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr). It is a free
software released under the GPL v3+ license. You are welcome to redistribute it
under certain conditions; refer to the COPYING file for details.
