/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"
#include "vxl_system_c.h"
#include "back-end/vxl_be.h"

#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/clock_time.h>

#include <string.h>

struct vxl_scene {
  struct vxl_be_svo* svo;
  struct vxl_system* sys;
  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
scene_release(ref_T* ref)
{
  struct vxl_scene* scn = NULL;
  struct vxl_system* sys = NULL;
  ASSERT(ref);

  scn = CONTAINER_OF(ref, struct vxl_scene, ref);
  VXL(scene_clear(scn));
  sys = scn->sys;
  if(scn->svo) VXL(be_svo_ref_put(scn->svo));
  MEM_RM(sys->allocator, scn);
  VXL(system_ref_put(sys));
}

/*******************************************************************************
 * Scene API functions
 ******************************************************************************/
VXL_DECLARE_REF_FUNCS(scene)

res_T
vxl_scene_create(struct vxl_system* sys, struct vxl_scene** out_scn)
{
  struct vxl_scene* scn = NULL;
  res_T res = RES_OK;

  if(!sys || !out_scn) {
    res = RES_BAD_ARG;
    goto error;
  }
  scn = MEM_CALLOC(sys->allocator, 1, sizeof(struct vxl_scene));
  if(!scn) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&scn->ref);
  VXL(system_ref_get(sys));
  scn->sys = sys;

  res = vxl_be_svo_create(sys->dev, &scn->svo);
  if(res != RES_OK) goto error;

exit:
  if(out_scn) *out_scn = scn;
  return res;
error:
  if(scn) {
    VXL(scene_ref_put(scn));
    scn = NULL;
  }
  goto exit;
}

res_T
vxl_scene_clear(struct vxl_scene* scn)
{
  if(!scn) return RES_BAD_ARG;
  if(scn->svo) VXL(be_svo_clear(scn->svo));
  return RES_OK;
}

res_T
vxl_scene_setup
  (struct vxl_scene* scn,
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const size_t definition, /* Definition along the X, Y and Z axis */
   const enum vxl_mem_location memloc,
   struct time* vxlzr_timer,
   struct time* build_timer)
{
  res_T res = RES_OK;

  if(!scn || !meshes || !nmeshes || !definition) {
    res = RES_BAD_ARG;
    goto error;
  }

  VXL(scene_clear(scn));

  res = vxl_be_svo_build
    (scn->svo, definition, meshes, nmeshes, memloc, vxlzr_timer, build_timer);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

res_T
vxl_scene_get_desc(struct vxl_scene* scn, struct vxl_scene_desc* desc)
{
  struct vxl_be_svo_desc svo_desc;
  res_T res;

  if(!scn || !desc)
    return RES_BAD_ARG;

  res = vxl_be_svo_get_desc(scn->svo, &svo_desc);
  if(res != RES_OK) return res;

  desc->voxels_count = svo_desc.voxels_count;
  f3_set(desc->lower, svo_desc.lower);
  f3_set(desc->upper, svo_desc.upper);
  desc->definition = svo_desc.definition;
  desc->mem_size = svo_desc.mem_size + sizeof(struct vxl_scene);
  return RES_OK;
}

res_T
vxl_scene_trace_ray
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const float org[3],
   const float dir[3],
   const float range[2],
   struct vxl_hit* hit)
{
  if(!scn) return RES_BAD_ARG;
  if(!scn->svo) return RES_BAD_ARG; /* The scene is not built */
  return vxl_be_svo_trace_ray(scn->svo, rt_state, org, dir, range, hit);
}

res_T
vxl_scene_trace_dray
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const float org[3],
   const float dir[3],
   const float dir_dx[3],
   const float dir_dy[3],
   const float range[2],
   struct vxl_hit* hit)
{
  if(!scn) return RES_BAD_ARG;
  if(!scn->svo) return RES_BAD_ARG; /* The scene is not built */
  return vxl_be_svo_trace_dray
    (scn->svo, rt_state, org, dir, dir_dx, dir_dy, range, hit);
}

res_T
vxl_scene_trace_rays
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits)
{
  if(!scn) return RES_BAD_ARG;
  if(!nrays) return RES_OK;
  if(!scn->svo) return RES_BAD_ARG; /* The scene is not built */
  return vxl_be_svo_trace_rays
    (scn->svo, rt_state, nrays, mask, orgs, dirs, ranges, hits);
}

res_T
vxl_scene_trace_drays
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* dirs_dx,
   const float* dirs_dy,
   const float* ranges,
   struct vxl_hit* hits)
{
  if(!scn) return RES_BAD_ARG;
  if(!nrays) return RES_OK;
  if(!scn->svo) return RES_BAD_ARG; /* The scene is not built */
  return vxl_be_svo_trace_drays(scn->svo, rt_state, nrays, mask, orgs,
    dirs, dirs_dx, dirs_dy, ranges, hits);
}

