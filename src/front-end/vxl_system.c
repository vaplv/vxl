/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"
#include "vxl_system_c.h"
#include "back-end/vxl_be.h"

#include <rsys/mem_allocator.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
system_release(ref_T* ref)
{
  struct vxl_system* sys = NULL;
  ASSERT(ref);
  sys = CONTAINER_OF(ref, struct vxl_system, ref);
  if(sys->dev) VXL(be_device_ref_put(sys->dev));
  MEM_RM(sys->allocator, sys);
}

/*******************************************************************************
 * System API functions
 ******************************************************************************/
VXL_DECLARE_REF_FUNCS(system)

res_T
vxl_system_create
  (struct mem_allocator* allocator,
   const unsigned nthreads_hint,
   struct vxl_system** out_sys)
{
  struct mem_allocator* mem_allocator = NULL;
  struct vxl_system* sys = NULL;
  res_T res = RES_OK;

  if(!out_sys) {
    res = RES_BAD_ARG;
    goto error;
  }

  mem_allocator = allocator ? allocator : &mem_default_allocator;
  sys = MEM_CALLOC(mem_allocator, 1, sizeof(struct vxl_system));
  if(!sys) {
    res = RES_MEM_ERR;
    goto error;
  }
  sys->allocator = mem_allocator;
  ref_init(&sys->ref);

  res = vxl_be_device_create(allocator, nthreads_hint, &sys->dev);
  if(res != RES_OK) goto error;

exit:
  if(out_sys) *out_sys = sys;
  return res;
error:
  if(sys) {
    VXL(system_ref_put(sys));
    sys = NULL;
  }
  goto exit;
}

res_T
vxl_system_get_nthreads(const struct vxl_system* sys, unsigned* nthreads)
{
  if(!sys) return RES_BAD_ARG;
  return vxl_be_device_get_nthreads(sys->dev, nthreads);
}

