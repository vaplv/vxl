/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_H
#define VXL_H

#include <rsys/float3.h>
#include <rsys/rsys.h>
#include <float.h>

/* Library symbol management */
#ifdef VXL_SHARED_BUILD /* Build vxl as a shared library */
  #define VXL_API extern EXPORT_SYM
#elif defined(VXL_STATIC_BUILD) /* Use/build vxl as a static library */
  #define VXL_API extern LOCAL_SYM
#else /* Use the vxl shared library */
  #define VXL_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the vxl function `Func'
 * returns an error. One should use this macro on vxl function calls for which
 * no explicit error checking is performed */
#ifndef NDEBUG
  #define VXL(Func) ASSERT(vxl_##Func == RES_OK)
#else
  #define VXL(Func) vxl_##Func
#endif

/* Syntactic sugar used to inform VxL to use as many threads as CPU cores */
#define VXL_NTHREADS_DEFAULT (~0u)

struct mem_allocator;
struct time;

/*******************************************************************************
 * Public types
 ******************************************************************************/
/* Properties of the traced rays */
enum vxl_rays_flag {
  VXL_RAYS_SINGLE_ORIGIN = BIT(0), /* The rays have the same origin */
  VXL_RAYS_SINGLE_DIRECTION = BIT(1), /* The rays have the same direction */
  VXL_RAYS_SINGLE_RANGE = BIT(2) /* The rays have the same range */
};

/* Hint of the coherency of the rays to trace. The ray tracing coherency is
 * both impacted by the rays data and the voxel size: the more the intersected
 * voxels are large with respect to the footprint of a ray bundle to trace, the
 * more the ray tracing is coherent */
enum vxl_rt_coherency {
  VXL_RT_COHERENCY_HIGH,
  VXL_RT_COHERENCY_MEDIUM,
  VXL_RT_COHERENCY_LOW,
  VXL_RT_COHERENCY_MIXED,
  VXL_RT_COHERENCY_NONE
};

/* Voxel Faces. The value of the lower face along the i^th axis is 2^i and the
 * value of its upper face is 2^i + 1 with i in [0, 3) */
enum vxl_face {
  VXL_FACE_LEFT,
  VXL_FACE_RIGHT,
  VXL_FACE_BOTTOM,
  VXL_FACE_TOP,
  VXL_FACE_BACK,
  VXL_FACE_FRONT
};

/* Memory location of the voxels */
enum vxl_mem_location {
  VXL_MEM_INCORE, /* In core <=> In main memory */
  VXL_MEM_OOCORE /* Out-of-Core <=> Move onto the hard drive */
};

/* Default range along the ray which an intersection can occur into */
static const float VXL_RAY_RANGE_DEFAULT[2] = {0.f, FLT_MAX};

/* Intersection point along a ray */
struct vxl_hit {
  float normal[3]; /* Normalized normal */
  float distance; /* Distance from the ray origin to the impacted voxel */
  float size; /* World space size of the voxel along the X, Y and Z axis */
  unsigned char color[3]; /* RGB color */
  enum vxl_face face; /* Intersected voxel face */
};

static const struct vxl_hit VXL_HIT_DEFAULT = {
  {FLT_MAX, FLT_MAX, FLT_MAX}, FLT_MAX, FLT_MAX, {255, 255, 255}, VXL_FACE_FRONT
};

/* Helper macro that defines if a vxl_hit is a valid intersection or not */
#define VXL_HIT_NONE(Hit) ((Hit)->distance==FLT_MAX || IS_INF((Hit)->distance))

/* Descriptor of a triangular mesh */
struct vxl_mesh_desc {
  void (*get_ids) /* Get the indices toward the 3 vertices of `itri' */
    (const uint64_t itri, uint64_t ids[3], void* data);
  void (*get_pos) /* Get the 3D position of the vertex `ivtx' */
    (const uint64_t ivtx, float pos[3], void* data);
  void (*get_normal) /* Get the normal of `itri' at the `uvw' coordinates */
    (const uint64_t itri, const float uvw[3], float normal[3], void* data);
  void (*get_color) /* Get the color of `itri' at the `uvw' coordinates */
    (const uint64_t itri, const float uvw[3], unsigned char col[3], void* data);
  void* data; /* Data set as the last argument of the callback functions */
  uint64_t ntris; /* Number of triangles */
};

/* Default get_color functor of a mesh */
static INLINE void
VXL_MESH_GET_COLOR_DEFAULT
  (const uint64_t itri, const float uvw[3], unsigned char col[3], void* data)
{
  (void)itri, (void)uvw, (void)data;
  ASSERT(col);
  col[0] = col[1] = col[2] = 0xFF;
}

/* Default get_normal functor of a mesh */
static INLINE void
VXL_MESH_GET_NORMAL_DEFAULT
  (const uint64_t itri, const float uvw[3], float normal[3], void* data)
{
  (void)itri, (void)uvw, (void)data;
  ASSERT(normal);
  normal[0] = 1.f, normal[1] = 0.f, normal[2] = 0.f;
}

static const struct vxl_mesh_desc VXL_MESH_DESC_NULL = {
  NULL, NULL, NULL, NULL, NULL, 0
};

/* Ray tracing description */
struct vxl_rt_desc {
  uint32_t lod_min; /* Minimum Level of detail */
  uint32_t lod_max; /* Maximum Level of detail */
  enum vxl_rt_coherency coherency; /* Hint on the level of RT coherency */
  char simd; /* Enable the SIMD optimisation if available */
};

#define VXL_RT_DESC_DEFAULT__ {UINT32_MAX, UINT32_MAX, VXL_RT_COHERENCY_NONE, 0}
static const struct vxl_rt_desc VXL_RT_DESC_DEFAULT = VXL_RT_DESC_DEFAULT__;

/* Descriptor of a scene */
struct vxl_scene_desc {
  float lower[3], upper[3]; /* World space AABB */
  size_t voxels_count; /* Total number of voxels in the scene */
  size_t mem_size; /* Memory size in Bytes of the scene */
  uint16_t definition; /* Definition of the voxelized scene along X, Y and Z */
};
#define VXL_SCENE_DESC_NULL__ {                                                \
  { FLT_MAX, FLT_MAX, FLT_MAX}, /* Lower bound */                              \
  {-FLT_MAX,-FLT_MAX,-FLT_MAX}, /* Upper boubd */                              \
  0, /* # voxels */                                                            \
  0, /* Memory size */                                                         \
  0 /* # Scene discretization aloing the X, Y and Z axis */                    \
}
static const struct vxl_scene_desc VXL_SCENE_DESC_NULL = VXL_SCENE_DESC_NULL__;

/* Opaque vxl types */
struct vxl_system;
struct vxl_scene;

BEGIN_DECLS /* Begin the declaration of the VoXel Library API */

/*******************************************************************************
 * The system is the Voxel Library handler.
 ******************************************************************************/
VXL_API res_T
vxl_system_create
  (struct mem_allocator* allocator, /* May be NULL <=> Use default allocator */
   const unsigned nthreads_hint, /* Hint on the number of threads to use */
   struct vxl_system** sys);

VXL_API res_T
vxl_system_ref_get
  (struct vxl_system* sys);

VXL_API res_T
vxl_system_ref_put
  (struct vxl_system* sys);

/* Return the maximum number of threads supported by the system */
VXL_API res_T
vxl_system_get_nthreads
  (const struct vxl_system* sys,
   unsigned* nthreads);

/*******************************************************************************
 * The scene is a collection of voxels generated from a set of meshes that can
 * be acceded through ray-tracing.
 ******************************************************************************/
VXL_API res_T
vxl_scene_create
  (struct vxl_system* sys,
   struct vxl_scene** scene);

VXL_API res_T
vxl_scene_ref_get
  (struct vxl_scene* scene);

VXL_API res_T
vxl_scene_ref_put
  (struct vxl_scene* scene);

/* Remove all voxels from the scene */
VXL_API res_T
vxl_scene_clear
  (struct vxl_scene* scn);

/* Voxelized the submitted geometry and build the voxel data structure */
VXL_API res_T
vxl_scene_setup
  (struct vxl_scene* scn,
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const size_t definition, /* Definition along the X, Y and Z axis */
   const enum vxl_mem_location mem_location,
   struct time* voxelization_timer, /* May be NULL */
   struct time* build_timer); /* May be NULL */

/* Retrieve informations on the current built scene */
VXL_API res_T
vxl_scene_get_desc
  (struct vxl_scene* scn,
   struct vxl_scene_desc* desc);

VXL_API res_T
vxl_scene_trace_ray
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const float ray_origin[3],
   const float ray_direction[3], /* Must be normalized */
   const float ray_range[2], /* range[0] >= range[1] <=> ray is disabled */
   struct vxl_hit* hit);

VXL_API res_T
vxl_scene_trace_dray
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const float ray_origin[3],
   const float ray_dir[3], /* Must be normalized */
   const float ray_dir_dx[3], /* Must be normalized */
   const float ray_dir_dy[3], /* Must be normalized */
   const float ray_range[2], /* range[0] >= range[1] <=> ray is disabled */
   struct vxl_hit* hit);

/* If (ray_ranges+i*2)[0] >= (ranges+i*2)[1] with i in [0, rays_count), then
 * the i^th ray is disabled. */
VXL_API res_T
vxl_scene_trace_rays
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const size_t rays_count,
   const int rays_mask, /* Combination of vxl_ray_flag */
   const float* rays_origin, /* List of float[3] */
   const float* rays_direction, /* List of float[3] */
   const float* rays_range, /* List of float[2] */
   struct vxl_hit* hits); /* List of vxl_hit */

/* Trace a set of differential rays. If (ray_ranges+i*2)[0] >= (ranges+i*2)[1]
 * whith i in [0, rays_count), then the i^th ray is disabled. */
VXL_API res_T
vxl_scene_trace_drays
  (struct vxl_scene* scn,
   const struct vxl_rt_desc* rt_state,
   const size_t rays_count,
   const int rays_mask, /* combination of vxl_ray_flag */
   const float* rays_origin, /* list of float[3] */
   const float* rays_direction, /* list of float[3] */
   const float* rays_direction_dx, /* list of float[3] */
   const float* rays_direction_dy, /* list of float[3] */
   const float* rays_range, /* Lost of float[2] */
   struct vxl_hit* hits); /* list of vxl_hit */

END_DECLS

#endif /* VXL_H */

