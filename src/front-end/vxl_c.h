/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_C_H
#define VXL_C_H

#include "vxl.h"
#include <rsys/ref_count.h>

/* Helper macro declaring the body of the ref counting functions on Type. This
 * macro assumes that the struct vxl_##Type has an internal ref_T field named
 * `ref' and that a `void Type##_release(ref_T*)' function exists */
#define VXL_DECLARE_REF_FUNCS(Type)                                            \
  res_T                                                                        \
  vxl_ ## Type ## _ref_get(struct vxl_ ## Type* p)                             \
  {                                                                            \
    if(!p) return RES_BAD_ARG;                                                 \
    ref_get(&p->ref);                                                          \
    return RES_OK;                                                             \
  }                                                                            \
  res_T                                                                        \
  vxl_ ## Type ## _ref_put(struct vxl_ ## Type* p)                             \
  {                                                                            \
    if(!p) return RES_BAD_ARG;                                                 \
    ref_put(&p->ref, Type ## _release);                                        \
    return RES_OK;                                                             \
  }

static INLINE int
mesh_desc_check(const struct vxl_mesh_desc* desc)
{
  ASSERT(desc);
  return desc
      && desc->get_ids
      && desc->get_pos
      && desc->get_normal
      && desc->get_color
      && desc->ntris;
}

static INLINE res_T
mesh_desc_compute_aabb
  (const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   float lower[3],
   float upper[3])
{
  size_t imesh;
  ASSERT(meshes && nmeshes && lower && upper);

  f3_splat(lower, FLT_MAX);
  f3_splat(upper,-FLT_MAX);

  FOR_EACH(imesh, 0, nmeshes) {
    const struct vxl_mesh_desc* mesh = meshes + imesh;
    uint64_t itri;
    ASSERT(mesh_desc_check(mesh));

    FOR_EACH(itri, 0, mesh->ntris) {
      uint64_t ids[3];
      int i;
      mesh->get_ids(itri, ids, mesh->data);
      FOR_EACH(i, 0, 3) {
        float pos[3];
        mesh->get_pos(ids[i], pos, mesh->data);
        f3_max(upper, upper, pos);
        f3_min(lower, lower, pos);
      }
    }
  }

  if(upper[0] < lower[0] || upper[1] < lower[1] || upper[2] < lower[2])
    return RES_BAD_ARG;

  return RES_OK;
}

#endif /* VXL_C_H */
