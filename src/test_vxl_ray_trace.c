/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl.h"
#include "test_vxl_utils.h"

#include <rsys/float3.h>
#include <rsys/image.h>
#include <rsys/mem_allocator.h>

#include <string.h>

#define IMG_WIDTH 640
#define IMG_HEIGHT 480

int
main(int argc, char** argv)
{
  char dump[128];
  struct mem_allocator allocator_proxy;
  struct camera cam;
  struct image img;
  struct vxl_scene_desc desc = VXL_SCENE_DESC_NULL;
  struct vxl_scene* scn = NULL;
  struct vxl_system* sys = NULL;
  struct vxl_hit* hits;
  struct time t0 ,t1;
  unsigned char* pixels;
  float* dirs, *dirs_dx, *dirs_dy;
  float pos[3], tgt[3], up[3];
  float ray_org[3];
  float ray_dir[3];
  float ray_dir_dx[3];
  float ray_dir_dy[3];
  float pix[2];
  enum vxl_mem_location memloc;
  uint32_t ix, iy;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s <incore|oocore>\n", argv[0]);
    return 1;
  }

  if(!strcmp(argv[1], "incore")) {
    memloc = VXL_MEM_INCORE;
  } else if(!strcmp(argv[1], "oocore")) {
    memloc = VXL_MEM_OOCORE;
  } else {
    fprintf(stderr, "Unknown memory location `%s'.\n", argv[1]);
    return 1;
  }

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);
  CHK(vxl_system_create(&allocator_proxy, VXL_NTHREADS_DEFAULT, &sys) == RES_OK);

  scn = cbox_create(sys, 64, memloc);
  CHK(vxl_scene_get_desc(scn, &desc) == RES_OK);
  CHK(desc.voxels_count != 0);
  fprintf(stderr, "Scene size: %g MB\n", (double)desc.mem_size/(1024.0*1024.0));

  image_init(&allocator_proxy, &img);
  CHK(image_setup(&img, IMG_WIDTH, IMG_HEIGHT,
    sizeof_image_format(IMAGE_RGB8)*IMG_WIDTH, IMAGE_RGB8, NULL) == RES_OK);
  pixels = (uint8_t*)img.pixels;

  hits = MEM_ALLOC(&allocator_proxy, sizeof(struct vxl_hit)*IMG_WIDTH*IMG_HEIGHT);
  CHK(hits != NULL);
  dirs = MEM_ALLOC(&allocator_proxy, sizeof(float[3])*IMG_WIDTH*IMG_HEIGHT);
  CHK(dirs != NULL);
  dirs_dx = MEM_ALLOC(&allocator_proxy, sizeof(float[3])*IMG_WIDTH*IMG_HEIGHT);
  CHK(dirs_dx != NULL);
  dirs_dy = MEM_ALLOC(&allocator_proxy, sizeof(float[3])*IMG_WIDTH*IMG_HEIGHT);
  CHK(dirs_dy != NULL);

  f3(pos, 178.f,-1000.f, 273.f);
  f3(tgt, 178.f, 0.f, 273.f);
  f3(up, 0.f, 0.f, 1.f);
  camera_init(&cam, pos, tgt, up, (float)IMG_WIDTH/(float)IMG_HEIGHT);

  FOR_EACH(iy, 0, IMG_HEIGHT) {
    const float pix_dy = (float)(iy + 6)/(float)IMG_HEIGHT;
    pix[1] = (float)iy/(float)IMG_HEIGHT;
    FOR_EACH(ix, 0, IMG_WIDTH) {
      const float pix_dx = (float)(ix + 6)/(float)IMG_WIDTH;
      float tmp[2];
      pix[0] = (float)ix/(float)IMG_WIDTH;
      camera_ray(&cam, pix, ray_org, dirs + (iy*IMG_WIDTH + ix)*3);
      tmp[0] = pix_dx, tmp[1] = pix[1];
      camera_ray(&cam, tmp, ray_org, dirs_dx + (iy*IMG_WIDTH + ix)*3);
      tmp[0] = pix[0], tmp[1] = pix_dy;
      camera_ray(&cam, tmp, ray_org, dirs_dy + (iy*IMG_WIDTH + ix)*3);
    }
  }

  time_current(&t0);
  CHK(vxl_scene_trace_rays
    (scn, &VXL_RT_DESC_DEFAULT, IMG_HEIGHT*IMG_WIDTH,
     VXL_RAYS_SINGLE_ORIGIN|VXL_RAYS_SINGLE_RANGE, ray_org, dirs,
     VXL_RAY_RANGE_DEFAULT, hits) == RES_OK);
  time_current(&t1);
  time_sub(&t0, &t1, &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  fprintf(stderr, "Ray tracing time: %s\n", dump);

  FOR_EACH(iy, 0, IMG_HEIGHT) {
    pix[1] = (float)iy/(float)IMG_HEIGHT;
    FOR_EACH(ix, 0, IMG_WIDTH) {
      const float range[2] = {0.f, FLT_MAX};
      const size_t ipix = (iy*IMG_WIDTH + ix) * 3;
      struct vxl_hit hit;
      struct vxl_hit* prev_hit = hits + iy*IMG_WIDTH + ix;

      pix[0] = (float)ix/(float)IMG_WIDTH;
      camera_ray(&cam, pix, ray_org, ray_dir);
      CHK(vxl_scene_trace_ray(scn, &VXL_RT_DESC_DEFAULT, ray_org, ray_dir,
        range, &hit) == RES_OK);

      if(VXL_HIT_NONE(&hit)) {
        CHK(VXL_HIT_NONE(prev_hit) == 1);
        pixels[ipix + 0] = pixels[ipix + 1] = pixels[ipix + 2] = 0;
      } else {
        CHK(VXL_HIT_NONE(prev_hit) == 0);
        CHK(prev_hit->color[0] == hit.color[0]);
        CHK(prev_hit->color[1] == hit.color[1]);
        CHK(prev_hit->color[2] == hit.color[2]);
        CHK(eq_eps(prev_hit->distance, hit.distance, 1.e-6f) == 1);
        pixels[ipix+0] = prev_hit->color[0];
        pixels[ipix+1] = prev_hit->color[1];
        pixels[ipix+2] = prev_hit->color[2];
      }
    }
  }

  CHK(image_write_ppm_stream(&img, 0, stdout) == RES_OK);

  CHK(vxl_scene_trace_drays
    (scn, &VXL_RT_DESC_DEFAULT, IMG_HEIGHT*IMG_WIDTH,
     VXL_RAYS_SINGLE_ORIGIN|VXL_RAYS_SINGLE_RANGE, ray_org, dirs, dirs_dx,
     dirs_dy, VXL_RAY_RANGE_DEFAULT, hits) == RES_OK);

  FOR_EACH(iy, 0, IMG_HEIGHT) {
    const float pix_dy = (float)(iy + 6)/(float)IMG_HEIGHT;
    pix[1] = (float)iy/(float)IMG_HEIGHT;
    FOR_EACH(ix, 0, IMG_WIDTH) {
      const float pix_dx = (float)(ix + 6)/(float)IMG_WIDTH;
      float tmp[2];
      struct vxl_hit hit;
      struct vxl_hit* prev_hit = hits + iy*IMG_WIDTH + ix;
      const size_t ipix = (iy*IMG_WIDTH + ix) * 3;

      pix[0] = (float)ix/(float)IMG_WIDTH;
      camera_ray(&cam, pix, ray_org, ray_dir);
      tmp[0] = pix_dx, tmp[1] = pix[1];
      camera_ray(&cam, tmp, ray_org, ray_dir_dx);
      tmp[0] = pix[0], tmp[1] = pix_dy;
      camera_ray(&cam, tmp, ray_org, ray_dir_dy);
      CHK(vxl_scene_trace_dray
        (scn, &VXL_RT_DESC_DEFAULT, ray_org, ray_dir, ray_dir_dx, ray_dir_dy,
         VXL_RAY_RANGE_DEFAULT, &hit) == RES_OK);
      if(VXL_HIT_NONE(&hit)) {
        CHK(VXL_HIT_NONE(prev_hit) == 1);
        pixels[ipix + 0] = pixels[ipix + 1] = pixels[ipix + 2] = 0;
      } else {
        CHK(VXL_HIT_NONE(prev_hit) == 0);
        CHK(prev_hit->color[0] == hit.color[0]);
        CHK(prev_hit->color[1] == hit.color[1]);
        CHK(prev_hit->color[2] == hit.color[2]);
        CHK(eq_eps(prev_hit->distance, hit.distance, 1.e-6f) == 1);
        pixels[ipix+0] = prev_hit->color[0];
        pixels[ipix+1] = prev_hit->color[1];
        pixels[ipix+2] = prev_hit->color[2];
      }
    }
  }

  CHK(image_write_ppm_stream(&img, 0, stdout) == RES_OK);

  image_release(&img);
  MEM_RM(&allocator_proxy, dirs);
  MEM_RM(&allocator_proxy, dirs_dx);
  MEM_RM(&allocator_proxy, dirs_dy);
  MEM_RM(&allocator_proxy, hits);

  CHK(vxl_system_ref_put(sys) == RES_OK);
  CHK(vxl_scene_ref_put(scn) == RES_OK);
  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

