/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl.h"
#include "test_vxl_utils.h"

#include <rsys/float2.h>
#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>

#include <string.h>

static struct vxl_scene*
scene_create
  (struct vxl_system* sys,
   const enum vxl_mem_location memloc,
   const float translation[3])
{
  const float verts[] = {
    /* positions */
    -1.f, -1.f, -1.f,
    -1.f, -1.f,  1.f,
    -1.f,  1.f, -1.f,
    -1.f,  1.f,  1.f,
    1.f, -1.f, -1.f,
    1.f, -1.f,  1.f,
    1.f,  1.f, -1.f,
    1.f,  1.f,  1.f,
  };
  const size_t nverts = sizeof(verts) / (sizeof(float)*3);
  const uint32_t ids[] = {
    0, 1, 2, 2, 1, 3, /* Left */
    0, 2, 6, 6, 4, 0, /* Back */
    4, 6, 5, 5, 6, 7, /* Right */
    5, 7, 1, 1, 7, 3, /* Front */
    3, 7, 2, 2, 7, 6, /* Top */
    0, 4, 5, 5, 1, 0  /* Bottom */
  };
  const size_t nids = sizeof(ids)/sizeof(uint32_t);
  struct mesh cube = MESH_NULL;
  struct vxl_mesh_desc msh_desc = VXL_MESH_DESC_NULL;
  struct vxl_scene* scn = NULL;
  struct vxl_scene_desc scn_desc;
  struct time t0, t1;

  f3_set(cube.translation, translation);
  cube.verts = verts;
  cube.nverts = nverts;
  cube.ids = ids;
  cube.nids = nids;
  msh_desc.get_ids = mesh_get_ids;
  msh_desc.get_pos = mesh_get_pos;
  msh_desc.get_normal = VXL_MESH_GET_NORMAL_DEFAULT;
  msh_desc.get_color = VXL_MESH_GET_COLOR_DEFAULT;
  msh_desc.data = &cube;
  msh_desc.ntris = nids / 3;

  CHK(vxl_scene_create(NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_create(sys, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_create(NULL, &scn) == RES_BAD_ARG);
  CHK(vxl_scene_create(sys, &scn) == RES_OK);

  CHK(vxl_scene_setup(NULL, NULL, 0, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, NULL, 0, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, &msh_desc, 0, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, &msh_desc, 0, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, NULL, 1, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, NULL, 1, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, &msh_desc, 1, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 0, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, NULL, 0, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, NULL, 0, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, &msh_desc, 0, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, &msh_desc, 0, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, NULL, 1, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, NULL, 1, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(NULL, &msh_desc, 1, 128, memloc, NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 127, memloc, NULL, NULL) == RES_OK);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 128, memloc, &t0, NULL) == RES_OK);

  CHK(vxl_scene_get_desc(NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_get_desc(scn, NULL) == RES_BAD_ARG);
  CHK(vxl_scene_get_desc(NULL, &scn_desc) == RES_BAD_ARG);
  CHK(vxl_scene_get_desc(scn, &scn_desc) == RES_OK);
  CHK(scn_desc.definition == 128);
  CHK(scn_desc.mem_size != 0);

  CHK(t0.nsec + t0.sec*1000000000l != 0);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 128, memloc, NULL, &t1) == RES_OK);
  CHK(t1.nsec + t1.sec*1000000000l != 0);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 128, memloc, &t0, &t1) == RES_OK);
  CHK(t0.nsec + t0.sec*1000000000l != 0);
  CHK(t1.nsec + t1.sec*1000000000l != 0);
  return scn;
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator_proxy;
  struct vxl_hit hit;
  struct vxl_hit hits[4];
  struct vxl_scene* scn = NULL;
  struct vxl_system* sys = NULL;
  struct vxl_scene_desc scene_desc;
  struct vxl_rt_desc rt = VXL_RT_DESC_DEFAULT;
  float orgs[12];
  float dirs[12];
  float dirs_dx[12];
  float dirs_dy[12];
  float ranges[8];
  float f[3], o[3], d[3], ddx[3], ddy[3], r[2];
  enum vxl_mem_location memloc;
  int i, ix, iy;
  (void)argc, (void)argv;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s <incore|oocore>\n", argv[0]);
    return 1;
  }

  if(!strcmp(argv[1], "incore")) {
    memloc = VXL_MEM_INCORE;
  } else if(!strcmp(argv[1], "oocore")) {
    memloc = VXL_MEM_OOCORE;
  } else {
    fprintf(stderr, "Unknown memory location `%s'.\n", argv[1]);
    return 1;
  }

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxl_system_create(&allocator_proxy, VXL_NTHREADS_DEFAULT, &sys) == RES_OK);
  scn = scene_create(sys, memloc, f3_splat(f, 0));

  CHK(scn != NULL);

  CHK(vxl_scene_clear(NULL) == RES_BAD_ARG);
  CHK(vxl_scene_clear(scn) == RES_OK);

  f3(orgs, 0.f, 0.f, 0.f);
  f3(dirs, 0.f, 0.f, -1.f);
  CHK(vxl_scene_trace_ray(scn, &VXL_RT_DESC_DEFAULT, orgs, dirs,
    VXL_RAY_RANGE_DEFAULT, &hit) == RES_OK);
  CHK(VXL_HIT_NONE(&hit) == 1);

  rt.simd = 1;
  CHK(vxl_scene_trace_ray(scn, &VXL_RT_DESC_DEFAULT, orgs, dirs,
    VXL_RAY_RANGE_DEFAULT, &hit) == RES_OK);
  CHK(VXL_HIT_NONE(&hit) == 1);

  CHK(vxl_scene_get_desc(scn, &scene_desc) == RES_OK);
  CHK(scene_desc.voxels_count == 0);
  CHK(scene_desc.definition == 0);
  FOR_EACH(i, 0, 3) {
    CHK(scene_desc.lower[i] > scene_desc.upper[i]);
  }

  scn = cube_create(sys, f3_splat(f, 0), scn, memloc);

  CHK(vxl_scene_get_desc(scn, &scene_desc) == RES_OK);
  CHK(scene_desc.voxels_count != 0);
  CHK(scene_desc.definition != 0);
  FOR_EACH(i, 0, 3) {
    CHK(scene_desc.lower[i] <= -1);
    CHK(scene_desc.upper[i] >= 1);
  }

  CHK(vxl_scene_clear(scn) == RES_OK);
  CHK(vxl_scene_get_desc(scn, &scene_desc) == RES_OK);
  CHK(scene_desc.voxels_count == 0);
  CHK(scene_desc.definition == 0);
  FOR_EACH(i, 0, 3) {
    CHK(scene_desc.lower[i] > scene_desc.upper[i]);
  }

  scn = cube_create(sys, f3_splat(f, 32), scn, memloc);

  f3(o, 16.f, 16.f, 0.f);
  f3(d, 0.f, 0.f, 1.f);
  f2(r, 0.f, FLT_MAX);
  f3_normalize(ddx, f3_add(ddx, d, f3(ddx, 1.f, 0.f, 0.f)));
  f3_normalize(ddy, f3_add(ddy, d, f3(ddy, 0.f, 1.f, 0.f)));

  #define RAY vxl_scene_trace_ray
  CHK(RAY(NULL, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, NULL, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, NULL, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, NULL, o, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, NULL, o, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, d, NULL, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, NULL, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, d, NULL, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, NULL, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, d, r, NULL) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, NULL, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, NULL, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, NULL, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(NULL, &rt, o, d, r, &hit) == RES_BAD_ARG);
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_OK);
  f2(r, FLT_MAX, -FLT_MAX);
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_OK);
  f2(r, -1.f, FLT_MAX);
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_BAD_ARG);
  f2(r, 0, FLT_MAX);
  rt.lod_min = 1, rt.lod_max = 0;
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_BAD_ARG);
  rt.lod_min = 0, rt.lod_max = UINT32_MAX;
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_OK);
  rt.simd = 1;
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_OK);
  rt.coherency = VXL_RT_COHERENCY_HIGH;
  CHK(RAY(scn, &rt, o, d, r, &hit) == RES_OK);
  rt = VXL_RT_DESC_DEFAULT;
  #undef RAY

  #define DRAY vxl_scene_trace_dray
  CHK(DRAY(NULL, &rt, NULL, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, NULL, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, ddx, NULL, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, NULL, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, NULL) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, NULL, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, ddx, NULL, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, NULL, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, NULL, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, NULL, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, NULL, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, NULL, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, NULL, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, NULL, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(NULL, &rt, o, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_OK);
  CHK(DRAY(scn, NULL, o, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, NULL, &hit) == RES_BAD_ARG);
  f2(r, FLT_MAX, -FLT_MAX);
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_OK);
  f2(r, -1.f, FLT_MAX);
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  f2(r, 0, FLT_MAX);
  rt.lod_min = 1;
  rt.lod_max = 0;
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_BAD_ARG);
  rt.lod_min = 0;
  rt.lod_max = UINT32_MAX;
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_OK);
  rt.simd = 1;
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_OK);
  rt.coherency = VXL_RT_COHERENCY_MEDIUM;
  CHK(DRAY(scn, &rt, o, d, ddx, ddy, r, &hit) == RES_OK);
  rt = VXL_RT_DESC_DEFAULT;
  #undef DRAY

  FOR_EACH(ix, 0, 2) {
  FOR_EACH(iy, 0, 2) {
    const float vec_x[3] = { 1.f, 0.f, 0.f };
    const float vec_y[3] = { 0.f, 1.f, 0.f };
    const float vec_z[3] = { 0.f, 0.f, 1.f };
    const int iray = ix*2 + iy;
    float x[3], y[3], dx[3], dy[3];

    f3_mulf(x, vec_x, (float)ix*2.f - 1.f);
    f3_mulf(y, vec_y, (float)iy*2.f - 1.f);
    f3_mulf(dx, vec_x, (float)(ix + 1)*2.f - 1.f);
    f3_mulf(dy, vec_y, (float)(iy + 1)*2.f - 1.f);

    f3_add(dirs + iray*3, f3_add(dirs + iray*3, x, y), vec_z);
    f3_add(dirs_dx + iray*3, f3_add(dirs_dx + iray*3, dx, y), vec_z);
    f3_add(dirs_dy + iray*3, f3_add(dirs_dy + iray*3, x, dy), vec_z);
    f2(ranges + iray*2, 0, FLT_MAX);
    CHK(f3_normalize(dirs + iray*3, dirs + iray*3) != 0);
    CHK(f3_normalize(dirs_dx + iray*3, dirs_dx + iray*3) != 0);
    CHK(f3_normalize(dirs_dy + iray*3, dirs_dy + iray*3) != 0);
    f3(orgs + iray*3, (float)ix, (float)iy, 0.f);
  }}

  #define RAYS vxl_scene_trace_rays
  CHK(RAYS(NULL, &rt, 0, 0, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 0, 0, NULL, NULL, NULL, NULL) == RES_OK);
  CHK(RAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, orgs, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, NULL, dirs, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, orgs, dirs, NULL, NULL) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, orgs, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, NULL, dirs, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, orgs, dirs, NULL, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, 0, orgs, dirs, ranges, hits) == RES_OK);
  CHK(RAYS(scn, NULL, 4, 0, orgs, dirs, ranges, hits) == RES_BAD_ARG);
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, ranges, hits) == RES_OK);
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_DIRECTION, orgs, dirs, ranges, hits) == RES_OK);
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN|VXL_RAYS_SINGLE_DIRECTION,
     orgs, dirs, ranges, hits) == RES_OK);
  CHK(RAYS(scn, &rt, 4,
     VXL_RAYS_SINGLE_ORIGIN|VXL_RAYS_SINGLE_DIRECTION|VXL_RAYS_SINGLE_RANGE,
     orgs, dirs, ranges, hits) == RES_OK);
  rt.lod_min = 1;
  rt.lod_max = 0;
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs,ranges, hits) == RES_BAD_ARG);
  rt.lod_min = 0;
  rt.lod_max = UINT32_MAX;
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, ranges, hits) == RES_OK);
  rt.simd |= 1;
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, ranges, hits) == RES_OK);
  rt.coherency = VXL_RT_COHERENCY_LOW;
  CHK(RAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, ranges, hits) == RES_OK);
  rt = VXL_RT_DESC_DEFAULT;

  #define DRAYS vxl_scene_trace_drays
  CHK(DRAYS(NULL, &rt, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL) == RES_OK);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, NULL, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, dirs_dx, NULL, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, NULL, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, dirs_dx, dirs_dy, NULL, NULL) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, NULL, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, dirs_dx, NULL, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, NULL, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, NULL, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, NULL, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, NULL, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, NULL, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, NULL, dirs, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, NULL, dirs, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(NULL, &rt, 4, 0, orgs, dirs, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, dirs_dx, dirs_dy, NULL, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, 0, orgs, dirs, dirs_dx, dirs_dy, ranges, hits) == RES_OK);
  CHK(DRAYS(scn, NULL, 4, 0, orgs, dirs, dirs_dx, dirs_dy, ranges, hits) == RES_BAD_ARG);
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) == RES_OK);
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_DIRECTION, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) == RES_OK);
  CHK(DRAYS (scn, &rt, 4,
    VXL_RAYS_SINGLE_ORIGIN|VXL_RAYS_SINGLE_DIRECTION|VXL_RAYS_SINGLE_RANGE,
    orgs, dirs, dirs_dx, dirs_dy, ranges, hits) == RES_OK);
  rt.lod_min = 1;
  rt.lod_max = 0;
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) == RES_BAD_ARG);
  rt.lod_min = 0;
  rt.lod_max = UINT32_MAX;
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) == RES_OK);
  rt.simd = 1;
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) ==RES_OK);
  rt.coherency = VXL_RT_COHERENCY_MIXED;
  CHK(DRAYS(scn, &rt, 4, VXL_RAYS_SINGLE_ORIGIN, orgs, dirs, dirs_dx,
    dirs_dy, ranges, hits) == RES_OK);
  rt = VXL_RT_DESC_DEFAULT;
  #undef DRAYS

  CHK(vxl_scene_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxl_scene_ref_get(scn) == RES_OK);
  CHK(vxl_scene_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxl_scene_ref_put(scn) == RES_OK);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  CHK(vxl_system_ref_put(sys) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}

