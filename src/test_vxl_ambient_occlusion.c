/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl.h"
#include "test_vxl_utils.h"

#include <rsys/float33.h>
#include <rsys/image.h>

#include <string.h>

/* Image definition */
#define IMG_WIDTH 640
#define IMG_HEIGHT 480
/* #Samples used to estimate the per pixel Ambient occlusion */
#define NSAMPLES 8

/* Return a random number in [0, 1[ */
static FINLINE float
rand_canonical(void)
{
  return (float)rand()/(float)(RAND_MAX+1u);
}

/* Cosine weighted sampling of an unit hemisphere */
static INLINE float*
ran_hemisphere_cos(float sample[3])
{
  double phi, cos_theta, sin_theta, v;
  CHK(sample != NULL);
  phi = rand_canonical() * 2*PI;
  v = rand_canonical();
  cos_theta = sqrt(v);
  sin_theta = sqrt(1 - v);
  sample[0] = (float)(cos(phi) * sin_theta);
  sample[1] = (float)(sin(phi) * sin_theta);
  sample[2] = (float)cos_theta;
  return sample;
}

static INLINE size_t
ambient_occlusion
  (struct vxl_scene* scn,
   const float pos[3],
   const float normal[3])
{
  float frame[9];
  const float range[2] = {0.f, FLT_MAX};
  float dir[3];
  struct vxl_hit hit;

  CHK(pos != NULL);
  CHK(normal != NULL);

  f33_basis(frame, normal);
  ran_hemisphere_cos(dir);
  f33_mulf3(dir, frame, dir);
  CHK(vxl_scene_trace_ray
    (scn, &VXL_RT_DESC_DEFAULT, pos, dir, range, &hit) == RES_OK);
  return VXL_HIT_NONE(&hit) != 0;
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct vxl_system* sys = NULL;
  struct vxl_scene* scn = NULL;
  struct camera cam;
  struct image img;
  float pos[3], tgt[3], up[3];
  unsigned char* pixels = NULL;
  size_t x, y;
  enum vxl_mem_location memloc;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s <incore|oocore>\n", argv[0]);
    return 1;
  }

  if(!strcmp(argv[1], "incore")) {
    memloc = VXL_MEM_INCORE;
  } else if(!strcmp(argv[1], "oocore")) {
    memloc = VXL_MEM_OOCORE;
  } else {
    fprintf(stderr, "Unknown memory location `%s'.\n", argv[1]);
    return 1;
  }

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  CHK(vxl_system_create(&allocator, VXL_NTHREADS_DEFAULT, &sys) == RES_OK);

  scn = cbox_create(sys, 64, memloc);

  image_init(&allocator, &img);
  CHK(image_setup(&img, IMG_WIDTH, IMG_HEIGHT,
    sizeof_image_format(IMAGE_RGB8)*IMG_WIDTH, IMAGE_RGB8, NULL) == RES_OK);
  pixels = (unsigned char*)img.pixels;

  f3(pos, 178.f,-1000.f, 273.f);
  f3(tgt, 178.f, 0.f, 273.f);
  f3(up, 0.f, 0.f, 1.f);
  camera_init(&cam, pos, tgt, up, (float)IMG_WIDTH/(float)IMG_HEIGHT);

  FOR_EACH(y, 0, IMG_HEIGHT) {
    float pix[2];
    pix[1] = (float)y/(float)IMG_HEIGHT;
    FOR_EACH(x, 0, IMG_WIDTH) {
      const size_t ipix = (y*IMG_WIDTH + x)*sizeof_image_format(IMAGE_RGB8);
      float ray_org[3], ray_dir[3];
      struct vxl_hit hit = VXL_HIT_DEFAULT;
      size_t isample;
      size_t sum = 0;
      float ao;

      pix[0] = (float)x/(float)IMG_WIDTH;

      FOR_EACH(isample, 0, NSAMPLES) {
        float sample[2];
        sample[0] = pix[0] + rand_canonical()/(float)IMG_WIDTH;
        sample[1] = pix[1] + rand_canonical()/(float)IMG_HEIGHT;

        camera_ray(&cam, sample, ray_org, ray_dir);
        CHK(vxl_scene_trace_ray(scn, &VXL_RT_DESC_DEFAULT, ray_org, ray_dir,
          VXL_RAY_RANGE_DEFAULT, &hit) == RES_OK);

        if(VXL_HIT_NONE(&hit)) {
          pixels[ipix + 0] = 0;
          pixels[ipix + 1] = 0;
          pixels[ipix + 2] = 0;
        } else {
          const float normals[][3] = { /* Per face normals */
            {-1, 0, 0}, { 1, 0, 0}, { 0,-1, 0},
            { 0, 1, 0}, { 0, 0,-1}, { 0, 0, 1}
          };
          ray_org[0] += ray_dir[0] * hit.distance;
          ray_org[1] += ray_dir[1] * hit.distance;
          ray_org[2] += ray_dir[2] * hit.distance;
          ray_org[0] += normals[hit.face][0]*hit.size;
          ray_org[1] += normals[hit.face][1]*hit.size;
          ray_org[2] += normals[hit.face][2]*hit.size;
          if(f3_dot(hit.normal, ray_dir) > 0)
            f3_minus(hit.normal, hit.normal);

          sum += ambient_occlusion(scn, ray_org, hit.normal);
        }
      }
      /* Expected value of the ambient occlusion */
      ao = (float)sum / (float)NSAMPLES;
      pixels[ipix + 0] = (unsigned char)(ao*255.f + 0.5f);
      pixels[ipix + 1] = (unsigned char)(ao*255.f + 0.5f);
      pixels[ipix + 2] = (unsigned char)(ao*255.f + 0.5f);
    }
  }

  CHK(image_write_ppm_stream(&img, 0, stdout) == RES_OK);
  image_release(&img);

  CHK(vxl_system_ref_put(sys) == RES_OK);
  CHK(vxl_scene_ref_put(scn) == RES_OK);

  check_memory_leaks(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
