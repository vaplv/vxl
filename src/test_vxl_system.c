/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl.h"
#include "test_vxl_utils.h"

#include <omp.h>

#include <rsys/math.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>

int
main(int argc, char** argv)
{
  struct vxl_system* sys = NULL;
  struct mem_allocator allocator_proxy;
  unsigned nthreads;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  CHK(vxl_system_create(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(vxl_system_create(NULL, 0, &sys) == RES_BAD_ARG);
  CHK(vxl_system_create(NULL, VXL_NTHREADS_DEFAULT, NULL) == RES_BAD_ARG);
  CHK(vxl_system_create(NULL, VXL_NTHREADS_DEFAULT, &sys) == RES_OK);

  CHK(vxl_system_get_nthreads(NULL, NULL) == RES_BAD_ARG);
  CHK(vxl_system_get_nthreads(sys, NULL) == RES_BAD_ARG);
  CHK(vxl_system_get_nthreads(NULL, &nthreads) == RES_BAD_ARG);
  CHK(vxl_system_get_nthreads(sys, &nthreads) == RES_OK);
  CHK(nthreads == (unsigned)omp_get_num_procs());

  CHK(vxl_system_ref_get(NULL) == RES_BAD_ARG);
  CHK(vxl_system_ref_get(sys) == RES_OK);
  CHK(vxl_system_ref_put(NULL) == RES_BAD_ARG);
  CHK(vxl_system_ref_put(sys) == RES_OK);
  CHK(vxl_system_ref_put(sys) == RES_OK);

  CHK(vxl_system_create(&allocator_proxy, 1, &sys) == RES_OK);
  CHK(vxl_system_get_nthreads(sys, &nthreads) == RES_OK);
  CHK(nthreads == 1);
  CHK(vxl_system_ref_put(sys) == RES_OK);

  check_memory_leaks(&allocator_proxy);
  mem_shutdown_proxy_allocator(&allocator_proxy);
  CHK(mem_allocated_size() == 0);
  return 0;
}
