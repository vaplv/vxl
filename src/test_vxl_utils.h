/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_VXL_UTILS_H
#define TEST_VXL_UTILS_H

#include "vxl.h"

#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>

#include <stdio.h>

struct camera {
  float pos[3];
  float x[3], y[3], z[3]; /* Frame */
};


struct mesh {
  float translation[3];
  const float* verts;
  size_t nverts;
  const uint32_t* ids;
  size_t nids;
};
static const struct mesh MESH_NULL = {{0,0,0}, NULL, 0, NULL, 0};

static INLINE void
camera_init
  (struct camera* cam,
   const float pos[3],
   const float tgt[3],
   const float up[3],
   const float proj_ratio)
{
  const float fov_x = (float)PI * 0.25f;
  float f = 0.f;
  ASSERT(cam);

  f3_set(cam->pos, pos);
  f = f3_normalize(cam->z, f3_sub(cam->z, tgt, pos)); CHK(f != 0);
  f = f3_normalize(cam->x, f3_cross(cam->x, cam->z, up)); CHK(f != 0);
  f = f3_normalize(cam->y, f3_cross(cam->y, cam->z, cam->x)); CHK(f != 0);
  f3_divf(cam->z, cam->z, (float)tan(fov_x*0.5f));
  f3_divf(cam->y, cam->y, proj_ratio);
}

static INLINE void
camera_ray
  (const struct camera* cam,
   const float pixel[2],
   float org[3],
   float dir[3])
{
  float x[3], y[3], f;
  ASSERT(cam && pixel && org && dir);

  f3_mulf(x, cam->x, pixel[0]*2.f - 1.f);
  f3_mulf(y, cam->y, pixel[1]*2.f - 1.f);
  f3_add(dir, f3_add(dir, x, y), cam->z);
  f = f3_normalize(dir, dir); CHK(f != 0);
  f3_set(org, cam->pos);
}


static INLINE void
mesh_get_ids(const uint64_t itri, uint64_t ids[3], void* data)
{
  const uint64_t id = itri * 3;
  struct mesh* msh = data;
  CHK(itri < msh->nids/3);
  CHK(id + 2 < msh->nids);
  ids[0] = msh->ids[id + 0];
  ids[1] = msh->ids[id + 1];
  ids[2] = msh->ids[id + 2];
}

static INLINE void
mesh_get_pos(const uint64_t ivert, float verts[3], void* data)
{
  struct mesh* msh = data;
  CHK(ivert < msh->nverts);
  f3_add(verts, msh->verts + (ivert*3), msh->translation);
}

static INLINE void
cbox_triangle_get_normal
  (const uint64_t itri,
   const float uvw[3],
   float normal[3],
   void* data)
{
  struct mesh* msh = data;
  const uint64_t id = itri * 3 /* #indices per triangle */;
  uint64_t iv0, iv1, iv2;
  const float* v0, *v1, *v2;
  float e0[3], e1[3];
  CHK(msh != NULL);
  CHK(uvw != NULL);
  CHK(normal != NULL);
  CHK(id + 2 < msh->nids);
  iv0 = msh->ids[id + 0];
  iv1 = msh->ids[id + 1];
  iv2 = msh->ids[id + 2];
  CHK(uvw[0] >= 0.f && uvw[0] <= 1.f);
  CHK(uvw[1] >= 0.f && uvw[1] <= 1.f);
  CHK(uvw[2] >= 0.f && uvw[2] <= 1.f);
  CHK(eq_eps(uvw[0] + uvw[1] + uvw[2], 1.f, 1.e-6f) == 1);
  CHK(iv0 < msh->nverts);
  CHK(iv1 < msh->nverts);
  CHK(iv2 < msh->nverts);
  v0 = msh->verts + iv0 * 3;
  v1 = msh->verts + iv1 * 3;
  v2 = msh->verts + iv2 * 3;
  f3_sub(e0, v1, v0);
  f3_sub(e1, v2, v0);
  f3_normalize(normal, f3_cross(normal, e0, e1));
}

static INLINE void
cbox_triangle_get_color
  (const uint64_t itri,
   const float uvw[3],
   unsigned char color[3],
   void* data)
{
  struct mesh* msh = data;
  const uint64_t id = itri * 3 /* #indices per triangle */;
  uint64_t iv0, iv1, iv2;

  CHK(msh != NULL);
  CHK(id + 2 < msh->nids);
  iv0 = msh->ids[id + 0];
  iv1 = msh->ids[id + 1];
  iv2 = msh->ids[id + 2];
  CHK(uvw[0] >= 0.f && uvw[0] <= 1.f);
  CHK(uvw[1] >= 0.f && uvw[1] <= 1.f);
  CHK(uvw[2] >= 0.f && uvw[2] <= 1.f);
  CHK(eq_eps(uvw[0] + uvw[1] + uvw[2], 1.f, 1.e-6f) == 1);
  CHK(iv0 < msh->nverts);
  CHK(iv1 < msh->nverts);
  CHK(iv2 < msh->nverts);
  color[0] = (unsigned char)(uvw[0] * 255.f);
  color[1] = (unsigned char)(uvw[1] * 255.f);
  color[2] = (unsigned char)(uvw[2] * 255.f);
}

static INLINE struct vxl_scene*
cbox_create
  (struct vxl_system* sys,
   const size_t definition,
   const enum vxl_mem_location memloc)
{
  char buf[32];
  struct mesh cbox = MESH_NULL;
  struct vxl_mesh_desc msh_desc = VXL_MESH_DESC_NULL;
  struct vxl_scene* scn;
  struct time vxlzr;
  struct time build;
  const float verts[] = {
    /* Box */
    552.f, 0.f,   0.f,
    0.f,   0.f,   0.f,
    0.f,   559.f, 0.f,
    552.f, 559.f, 0.f,
    552.f, 0.f,   548.f,
    0.f,   0.f,   548.f,
    0.f,   559.f, 548.f,
    552.f, 559.f, 548.f,
    /* Short block */
    130.f, 65.f,  0.f,
    82.f,  225.f, 0.f,
    240.f, 272.f, 0.f,
    290.f, 114.f, 0.f,
    130.f, 65.f,  165.f,
    82.f,  225.f, 165.f,
    240.f, 272.f, 165.f,
    290.f, 114.f, 165.f,
    /* Tall block */
    423.0f, 247.0f, 0.f,
    265.0f, 296.0f, 0.f,
    314.0f, 456.0f, 0.f,
    472.0f, 406.0f, 0.f,
    423.0f, 247.0f, 330.f,
    265.0f, 296.0f, 330.f,
    314.0f, 456.0f, 330.f,
    472.0f, 406.0f, 330.f
  };
  const size_t nverts = sizeof(verts) / (sizeof(float)*3);
  const uint32_t ids[] = {
    /* Box */
    0, 1, 2, 2, 3, 0,
    4, 5, 6, 6, 7, 4,
    1, 2, 6, 6, 5, 1,
    0, 3, 7, 7, 4, 0,
    2, 3, 7, 7, 6, 2,
    /* Short block */
    12, 13, 14, 14, 15, 12,
    9, 10, 14, 14, 13, 9,
    8, 11, 15, 15, 12, 8,
    10, 11, 15, 15, 14, 10,
    8, 9, 13, 13, 12, 8,
    /* Tall block */
    20, 21, 22, 22, 23, 20,
    17, 18, 22, 22, 21, 17,
    16, 19, 23, 23, 20, 16,
    18, 19, 23, 23, 22, 18,
    16, 17, 21, 21, 20, 16
  };
  const size_t nids = sizeof(ids)/sizeof(uint32_t);

  ASSERT(sys && definition);

  cbox.translation[0] = -100;
  cbox.translation[1] = 0;
  cbox.translation[2] = -2;
  cbox.verts = verts;
  cbox.nverts = nverts;
  cbox.ids = ids;
  cbox.nids = nids;
  msh_desc.get_ids = mesh_get_ids;
  msh_desc.get_pos = mesh_get_pos;
  msh_desc.get_normal = cbox_triangle_get_normal;
  msh_desc.get_color = cbox_triangle_get_color;
  msh_desc.data = &cbox;
  msh_desc.ntris = nids / 3;
  CHK(vxl_scene_create(sys, &scn) == RES_OK);
  CHK(vxl_scene_setup
    (scn, &msh_desc, 1, definition, memloc, &vxlzr, &build) == RES_OK);

  time_dump(&vxlzr, TIME_SEC|TIME_MSEC|TIME_USEC, NULL, buf, sizeof(buf));
  fprintf(stderr, "Voxelization in %s\n", buf);
  time_dump(&build, TIME_SEC|TIME_MSEC|TIME_USEC, NULL, buf, sizeof(buf));
  fprintf(stderr, "Build SVO in %s\n", buf);

  return scn;
}

static INLINE struct vxl_scene*
cube_create
  (struct vxl_system* sys,
   const float translation[3],
   struct vxl_scene* in_scn, /* May be NULL <=> create the scene */
   const enum vxl_mem_location memloc)
{
  struct mesh cube = MESH_NULL;
  struct vxl_mesh_desc msh_desc = VXL_MESH_DESC_NULL;
  struct vxl_scene* scn = in_scn;
  const float verts[] = {
    /* positions */
    -1.f, -1.f, -1.f,
    -1.f, -1.f,  1.f,
    -1.f,  1.f, -1.f,
    -1.f,  1.f,  1.f,
    1.f, -1.f, -1.f,
    1.f, -1.f,  1.f,
    1.f,  1.f, -1.f,
    1.f,  1.f,  1.f,
  };
  const size_t nverts = sizeof(verts) / (sizeof(float)*3);
  const uint32_t ids[] = {
    0, 1, 2, 2, 1, 3, /* Left */
    0, 2, 6, 6, 4, 0, /* Back */
    4, 6, 5, 5, 6, 7, /* Right */
    5, 7, 1, 1, 7, 3, /* Front */
    3, 7, 2, 2, 7, 6, /* Top */
    0, 4, 5, 5, 1, 0  /* Bottom */
  };
  const size_t nids = sizeof(ids)/sizeof(uint32_t);

  f3_set(cube.translation, translation);
  cube.verts = verts;
  cube.nverts = nverts;
  cube.ids = ids;
  cube.nids = nids;
  msh_desc.get_ids = mesh_get_ids;
  msh_desc.get_pos = mesh_get_pos;
  msh_desc.get_normal = VXL_MESH_GET_NORMAL_DEFAULT;
  msh_desc.get_color = VXL_MESH_GET_COLOR_DEFAULT;
  msh_desc.data = &cube;
  msh_desc.ntris = nids / 3;

  if(!scn) CHK(vxl_scene_create(sys, &scn) == RES_OK);
  CHK(vxl_scene_setup(scn, &msh_desc, 1, 64, memloc, NULL, NULL) == RES_OK);
  return scn;
}

static INLINE void
check_memory_leaks(struct mem_allocator* allocator)
{
  if(MEM_ALLOCATED_SIZE(allocator)) {
    char dump[512];
    MEM_DUMP(allocator, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks\n");
  }
}

#endif /* TEST_VXL_UTILS_H */

