/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_AOS_INTERVAL_H
#define VXL_BE_AOS_INTERVAL_H

#include <rsimd/rsimd.h>
#include <float.h>

struct aos_ia3 { v4f_T lower, upper; };

static FINLINE struct aos_ia3*
aos_ia3_set(struct aos_ia3* dst, const v4f_T lower, const v4f_T upper)
{
  ASSERT(dst);
  dst->lower = lower;
  dst->upper = upper;
  return dst;
}

static FINLINE struct aos_ia3*
aos_ia3_set1(struct aos_ia3* dst, const float lower, const float upper)
{
  ASSERT(dst);
  dst->lower = v4f_set1(lower);
  dst->upper = v4f_set1(upper);
  return dst;
}

static FINLINE struct aos_ia3*
aos_ia3_add
  (struct aos_ia3* dst, const struct aos_ia3* a, const struct aos_ia3* b)
{
  ASSERT(dst && a && b);
  dst->lower = v4f_add(a->lower, b->lower);
  dst->upper = v4f_add(a->upper, b->upper);
  return dst;
}

static FINLINE struct aos_ia3*
aos_ia3_sub
  (struct aos_ia3* dst, const struct aos_ia3* a, const struct aos_ia3* b)
{
  struct aos_ia3 tmp;
  ASSERT(dst && a && b);
  tmp.lower = v4f_sub(a->lower, b->upper);
  tmp.upper = v4f_sub(a->upper, b->lower);
  *dst = tmp;
  return dst;
}

static FINLINE struct aos_ia3*
aos_ia3_mul
  (struct aos_ia3* dst, const struct aos_ia3* a, const struct aos_ia3* b)
{
  v4f_T ll, lu, ul, uu;
  v4f_T tmp0, tmp1, tmp2, tmp3;
  ASSERT(dst && a && b);

  ll = v4f_mul(a->lower, b->lower);
  lu = v4f_mul(a->lower, b->upper);
  ul = v4f_mul(a->upper, b->lower);
  uu = v4f_mul(a->upper, b->upper);

  tmp0 = v4f_min(ll, lu);
  tmp1 = v4f_min(ul, uu);
  tmp2 = v4f_max(ll, lu);
  tmp3 = v4f_max(ul, uu);
  dst->lower = v4f_min(tmp0, tmp1);
  dst->upper = v4f_max(tmp2, tmp3);
  return dst;
}

static FINLINE struct aos_ia3*
aos_ia3_rcp(struct aos_ia3* dst, const struct aos_ia3* a)
{
  v4f_T rcp_lower, rcp_upper;
  v4f_T mask, tmp0, tmp1;
  ASSERT(dst && a);

  tmp0 = v4f_lt(a->lower, v4f_zero());
  tmp1 = v4f_lt(a->upper, v4f_zero());
  mask = v4f_xor(tmp0, tmp1);
  rcp_lower = v4f_rcp(a->lower);
  rcp_upper = v4f_rcp(a->upper);
  dst->lower = v4f_sel(rcp_upper, v4f_set1(-FLT_MAX), mask);
  dst->upper = v4f_sel(rcp_lower, v4f_set1( FLT_MAX), mask);
  return dst;
}

#endif /* VXL_BE_AOS_INTERVAL_H */

