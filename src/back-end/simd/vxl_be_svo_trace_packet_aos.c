/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "simd/vxl_be_aos_packet.h"
#include "simd/vxl_be_svo_trace_simd.h"

#include "legacy/vxl_be_c.h"
#include "legacy/vxl_be_svo_c.h"
#include "legacy/vxl_be_svo_trace.h"

#include <limits.h>

#define MAX_PACKET_WIDTH 256 /* Maximum number of packetized AoS rays */

/*
 * SVO node memory layout:
 *      +...+...+      #---#---#
 *      |`#--`#--`#    |`#--`#--`#
 *      + |`#--`#--`#  # | 3 | 7 |`+
 *      '`# | 2 | 6 |  |`#---#---# .
 *   Y  + |`#---#---#  # | 1 | 5 |`+
 * Z |   `# | 0 | 4 |   `#---#---# .
 *  \|     `#---#---#     `+..`+..`+
 *   o--X
 */


/*******************************************************************************
 * AoS ray
 ******************************************************************************/
struct ALIGN(16) aos_ray {
  v4f_T org; /* Origin */
  v4f_T dir; /* Direction */
  v4f_T rcp_dir; /* Reciprocal direction */
  float range[2]; /* Near/Far ray range */
  size_t iray; /* Ray identifier */
  char is_enabled; /* Define if the ray is active or not */
};

static FINLINE void
aos_ray_setup
  (struct aos_ray* ray,
   const v4f_T org, /* World space ray origin */
   const v4f_T dir, /* World space ray direction. Should be normalized */
   const float range[2], /* Near/Far ray range */
   const size_t iray) /* Ray scale factor */
{
  ray->org = org;
  ray->dir = dir;
  ray->rcp_dir = v4f_rcp(ray->dir);
  f2_set(ray->range, range);
  ray->iray = iray;
  ray->is_enabled = 1;

  /* Ensure that the fourth dummy component of org and rcp_dir is 0 and 1
   * respectively. This make easier the ray/AABB intersection test */
  ASSERT(v4f_w(ray->org) == 0.f);
  ray->rcp_dir = v4f_and(ray->rcp_dir, v4f_mask(~0, ~0, ~0, 0));
  ray->rcp_dir = v4f_add(ray->rcp_dir, v4f_set(0.f, 0.f, 0.f, 1.f));
  ASSERT(v4f_w(ray->rcp_dir) == 1.f);
}

/* AoS implementation of the Smits intersection: "Efficiency issues for RT" */
static FINLINE char
aos_ray_intersect_svo_voxel
  (const struct aos_ray* ray,
   const v4f_T aabb_lower_minus_ray_org_min_range, /* Precomputed value */
   const v4f_T aabb_upper_minus_ray_org_max_range, /* Precomputed value */
   v4f_T* hit_t, /* Near parametric ray intersection values. May be NULL */
   enum vxl_face* face) /* May be NULL */
{
  v4f_T t0, t1, tmp, mask;
  v4f_T t0_max, t1_min;
  int imask, iaxis;
  ASSERT(v4f_w(ray->rcp_dir) == 1.f);

  /* Compute the intersection */
  t0 = v4f_mul(aabb_lower_minus_ray_org_min_range, ray->rcp_dir);
  t1 = v4f_mul(aabb_upper_minus_ray_org_max_range, ray->rcp_dir);

  /* if t0 > t1 then SWAP(t0, t1) */
  mask = v4f_gt(t0, t1);
  tmp= v4f_sel(t0, t1, mask);
  t1 = v4f_sel(t1, t0, mask);
  t0 = tmp;

  /* Check if an intersection occurs */
  t0_max = v4f_reduce_max(t0);
  t1_min = v4f_reduce_min(t1);
  if(v4f_movemask(v4f_gt(t0_max, t1_min))) return 0;

  /* Define the hit voxel face. Hit the lower or upper faces whether the ray
   * direction sign along the hit axis is positive or negative, respectively.
   * Note that the hit face must be reverted when the ray starts into the
   * voxel. Assume that the face enum is left, right, bottom, top, back and
   * front */

  if(face || hit_t) {
    /* Helper lookup table used to define on which axis a ray hit a voxel */
    const int hit_axis_lut[16] = {
      -1, 0, 1, 0, 2, 0, 1, 0, -1, -1, -1, -1, -1, -1, -1, -1
    };

    imask = v4f_movemask(v4f_eq(t0, t0_max));
    iaxis = hit_axis_lut[imask];
    if(iaxis >= 0) { /* Hit front facing voxel plane */
      ASSERT(iaxis >= 0 && iaxis < 3);
      if(face) *face = (iaxis * 2) + (ray->dir[iaxis] > 0.f);
      if(hit_t) *hit_t = t0_max;
    } else {/*Hit back facing voxel plane, i.e. ray range starts into the vxl*/
      imask = v4f_movemask(v4f_eq(t1, t1_min));
      iaxis = hit_axis_lut[imask];
      ASSERT(iaxis >= 0 && iaxis < 3);
      if(face) *face = (iaxis * 2) + (ray->dir[iaxis] > 0.f);
      if(hit_t) *hit_t = t1_min;
    }
  }
  return 1;
}

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Approximate the packet footprint at a distance of 1 from the packet origin */
static FINLINE v4f_T
aos_packet_footprint(const struct aos_ia3* dir)
{
  v4f_T main_dir;
  v4f_T vec0, vec1;
  v4f_T cos_dir;
  v4f_T sqr_hypotenuse;
  v4f_T footprint;

  main_dir = v4f_normalize3(v4f_add(dir->lower, dir->upper));
  /* The interval of directions is not normalized */
  vec0 = v4f_normalize3(dir->lower);
  vec1 = v4f_normalize3(dir->upper);

  /* Compute the packet footprint */
  cos_dir = v4f_min(v4f_dot3(main_dir, vec0), v4f_dot3(main_dir, vec1));
  sqr_hypotenuse = v4f_rcp(v4f_mul(cos_dir, cos_dir));
  footprint = v4f_sqrt(v4f_sub(sqr_hypotenuse, v4f_set1(1.f))); /* Pythagore */
  footprint = v4f_mul(footprint, v4f_set1(2.f));

  /* Handle highly incoherent rays */
  footprint = v4f_sel(footprint, v4f_set1(FLT_MAX), v4f_le(cos_dir, v4f_zero()));
  /* Handle precision issue on highly coherent rays */
  footprint = v4f_sel(footprint, v4f_zero(), v4f_ge(cos_dir, v4f_set1(1.f)));
  return footprint;
}

/* Define if a node is tiny with respect to `packet_footprint' which is the
 * footprint of the packet computed at a distance of 1 from the packet origin */
static FINLINE int
aos_is_tiny_node
  (const v4f_T node_distance, /* Distance from the packet origin */
   const v4f_T lower, /* Node lower bound */
   const v4f_T upper, /* Node upper bound */
   const v4f_T packet_footprint)
{
  v4f_T tmp, node_size;
  ASSERT(v4f_movemask(v4f_ge(packet_footprint, v4f_zero())) == 15);

  /* Assume that the node is a cube */
  node_size = v4f_sub(upper, lower);
  ASSERT(eq_epsf(v4f_x(node_size), v4f_y(node_size), v4f_x(node_size)*1.e-4f));
  ASSERT(eq_epsf(v4f_x(node_size), v4f_z(node_size), v4f_x(node_size)*1.e-4f));

  /* Empirically define a tiny node as a node smaller than 3/4 of the packet
   * footprint */
  tmp = v4f_mul(v4f_mul(packet_footprint, v4f_set1(0.75f)), node_distance);
  return v4f_movemask(v4f_lt(node_size, tmp)) & 1;
}

static FINLINE void
aos_setup_traversal_ibits(int ibits[3], v4f_T aos_dir/* AoS direction */)
{
  ALIGN(16) float dir_abs[4];
  int dir_ids[3] = {0, 1, 2};
  int i;

  /* Bubble sort the `dir' dimensions in ascending order of their absolute
   * value <=> breadth first traversal of node dimension */
  v4f_store(dir_abs, v4f_abs(aos_dir));
  if(dir_abs[dir_ids[0]]>dir_abs[dir_ids[1]]) SWAP(int, dir_ids[0], dir_ids[1]);
  if(dir_abs[dir_ids[1]]>dir_abs[dir_ids[2]]) SWAP(int, dir_ids[1], dir_ids[2]);
  if(dir_abs[dir_ids[0]]>dir_abs[dir_ids[1]]) SWAP(int, dir_ids[0], dir_ids[1]);

  /* Compute the bit indices from the dir indices, with respect to the SVO node
   * memory layout */
  FOR_EACH(i, 0, 3) ibits[i] = 2 - dir_ids[i];
}

static FINLINE void
setup_hit
  (struct vxl_hit* hit,
   const float dst,
   const int ihit_face,
   const struct svo_node_attrib* attr)
{
  int i;
  ASSERT(hit && dst >= 0.f && ihit_face<6 && ihit_face>=0);
  hit->distance = dst;
  hit->face = ihit_face;
  xnormal_get(&attr->normal, hit->normal);
  FOR_EACH(i, 0, 3) hit->color[i] = attr->color[i];
}

/* Return the number of active rays that hit the leaf */
static FINLINE size_t
trace_leaf
  (const v4f_T lower, /* Node lower bound */
   const v4f_T upper, /* Node upper bound */
   const struct svo_node_attrib* attr, /* Children attrib of the node */
   size_t* iray_1st_active, /* 1st active ray */
   struct aos_ray* rays,
   const size_t nrays,
   struct aos_packet* packet,
   struct vxl_hit* hits)
{
  v4f_T tmp0, tmp1, t_hit;
  size_t nhits = 0;
  size_t iray;
  enum vxl_face face;
  ASSERT(attr && rays && nrays && packet && hits);
  ASSERT(iray_1st_active && *iray_1st_active < nrays);

  /* Note that all rays shared the same origin. We can thus precompute some
   * values of the ray/AABB intersection test */
  tmp0 = v4f_sub(lower, rays[0].org);
  tmp1 = v4f_sub(upper, rays[0].org);

  iray = *iray_1st_active;
  if(aos_ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &t_hit, &face)) {
    /* Early hit */
    rays[iray].is_enabled = 0;
    setup_hit(hits + rays[iray].iray, v4f_x(t_hit), face, attr);
    ++nhits;
    *iray_1st_active = SIZE_MAX;
    FOR_EACH(iray, iray + 1, nrays) {
      if(rays[iray].is_enabled) {
        if(aos_ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &t_hit, &face)) {
          rays[iray].is_enabled = 0;
          setup_hit(hits + rays[iray].iray, v4f_x(t_hit), face, attr);
          ++nhits;
        } else if(*iray_1st_active == SIZE_MAX) {
          *iray_1st_active = iray;
        }
      }
    }
  } else if(aos_packet_intersect_aos_aabb(packet, lower, upper, NULL)) {
    /* !Early miss */
    FOR_EACH(iray, iray + 1, nrays) {
      if(rays[iray].is_enabled
      && aos_ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &t_hit, &face)) {
        rays[iray].is_enabled = 0;
        setup_hit(hits + rays[iray].iray, v4f_x(t_hit), face, attr);
        ++nhits;
      }
    }
  }
  return nhits;
}

/* Single trace all rays in svo_rays in [*iray_1st_active, nrays). Return the
 * number of active rays that hit a voxel and update the iray_1s_active value */
static FINLINE size_t
trace_single_ray
  (struct vxl_be_svo* svo,
   const struct svo_index* inode, /* Index of the node to traverse */
   const v4f_T node_lower, /* Node lower bound */
   const float node_size, /* Node upper bound */
   const uint32_t lod_range[2], /* Current LOD range */
   size_t* iray_1st_active, /* Index toward the 1st active ray in svo_rays */
   struct aos_ray* rays,
   struct svo_ray* svo_rays,
   const size_t nrays,
   struct vxl_hit* hits)
{
  float ALIGN(16) lower[4];
  size_t iray;
  size_t nhits = 0;
  res_T res;
  (void)res;

  ASSERT(svo && iray_1st_active && rays && svo_rays && hits);
  ASSERT(*iray_1st_active < nrays);
  ASSERT(lod_range && lod_range[0] <= lod_range[1]);

  v4f_store(lower, node_lower);

  iray = *iray_1st_active;
  *iray_1st_active = SIZE_MAX;
  FOR_EACH(iray, iray, nrays) {
    if(!rays[iray].is_enabled)
      continue;

    res = svo_node_trace_ray(svo, inode, lower, node_size, lod_range,
      svo_rays + iray, hits + rays[iray].iray);
    ASSERT(res == RES_OK);

    if(VXL_HIT_NONE(hits + rays[iray].iray)) {
      if(*iray_1st_active == SIZE_MAX)
        *iray_1st_active = iray;
    } else {
#if 0 /* For debug */
      hits[rays[iray].iray].color[0] = 255;
      hits[rays[iray].iray].color[1] = 0;
      hits[rays[iray].iray].color[2] = 0;
#endif
      rays[iray].is_enabled = 0;
      ++nhits;
    }
  }
  return nhits;
}

static INLINE void
trace
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const struct svo_index* iroot,
   struct aos_ray* rays,
   struct svo_ray* svo_rays, /* May be NULL <=> no single ray fallback */
   const size_t nrays,
   struct aos_packet* packet, /* The ray packet */
   const v4f_T footprint, /* Packet footprint */
   const int traversal_ibits[3],
   struct vxl_hit* hits)
{
  const uint8_t ichild_lut[] = { 0, 4, 2, 6, 1, 5, 3, 7 };
  #define SCALE_MAX 23 /* #mantisses of a float */
  struct stack_entry {
    struct svo_index inode; /* SVO node index */
    size_t iray; /* First active ray */
    int ichild; /* Current child */
    int nchildren; /* # traversed children */
  } stack[SCALE_MAX + 1/*Dummy entry use to avoid invalid read*/];
  v4f_T lower_min_range;
  v4f_T upper_max_range;
  v4f_T scale_exp2;
  v4f_T packet_org;
  v4f_T t[2]; /* ray parametric values of the Ray/AABB intersection */
  v4f_T tmp0, tmp1;
  struct svo_index inode;
  size_t iray;
  size_t nrays_intersected;
  float range_length;
  int scale_range[2];
  int scale;
  int ichild;
  int nchildren;
  int rt_mixed;
  ASSERT(svo && rays && nrays && packet && hits);
  ASSERT(traversal_ibits && lod_range && lod_range[0] <= lod_range[1]);
  ASSERT(packet->single_org);

  /* The SVO AABB is assumed to be [1, 2]^3 */
  lower_min_range = v4f_set(1.f, 1.f, 1.f, packet->range[0]);
  upper_max_range = v4f_set(2.f, 2.f, 2.f, packet->range[1]);

  /* Discard packet that does not intersect the SVO */
  if(!aos_packet_intersect_aos_aabb(packet, lower_min_range, upper_max_range, t))
    return;

  /* If the packet is too incoherent: switch in single ray tracing */
  rt_mixed = svo_rays != NULL;
  if(rt_mixed
  && aos_is_tiny_node(t[0], lower_min_range, upper_max_range, footprint)) {
    iray = 0;
    trace_single_ray(svo, iroot, lower_min_range, 1.f/* size */, lod_range,
      &iray, rays, svo_rays, nrays, hits);
    return; /* All packetized rays were singled ray-traced */
  }

  packet_org = packet->org.lower;
  ASSERT(v4f_w(packet_org) == 0.f);

  /* Define the id and the lower left corner of the first child. */
  ichild = 0;
  tmp0 = v4f_gt(packet_org, v4f_set1(1.5f));
  lower_min_range = v4f_sel(lower_min_range, v4f_set1(1.5f), tmp0);
  ichild = ichild_lut[v4f_movemask(tmp0)];

  /* Traversal initialisation */
  inode = *iroot;
  iray = 0;
  nchildren = 0;
  nrays_intersected = 0;
  range_length = packet->range[1]-packet->range[0];
  scale_exp2 = v4f_set(0.5f, 0.5f, 0.5f, range_length);
  scale = SCALE_MAX - 1;
  scale_range[0] = (int)(SCALE_MAX - MMIN(lod_range[0], SCALE_MAX));
  scale_range[1] = (int)(SCALE_MAX - MMIN(lod_range[1], SCALE_MAX));

  /* The dummy entry must stop the node popping, i.e. nchildren must be < 8 */
  stack[SCALE_MAX].nchildren = 0;

  /* Interative SVO traversal */
  while(scale < SCALE_MAX ) {
    const struct svo_xnode* node = svo_get_node(svo, &inode);
    const int child_flag = BIT(ichild); /* Flag of the child */
    ASSERT(eq_epsf(v4f_w(lower_min_range), packet->range[0], 1.e-6f));
    ++nchildren;

    if(child_flag & node->is_valid) { /* The child node is not empty */

      /* Define the child bounds */
      upper_max_range = v4f_add(lower_min_range, scale_exp2);

      if((child_flag & node->is_leaf) || scale < scale_range[1]) { /* leaf */
        const struct svo_node_attrib* attr;
        attr = svo_get_child_attrib(svo, &inode, (uint8_t)ichild);
        nrays_intersected += trace_leaf(lower_min_range, upper_max_range,
          attr, &iray, rays, nrays, packet, hits);
        ASSERT(nrays_intersected <= nrays);

        /* The front to back SVO traversal ensures that one can stop the
         * traversal when all rays hit something */
        if(nrays_intersected == nrays) break;

      } else { /* Sub node */
        int intersect = 0;
        size_t iray_1st = iray;

        /* Shared origin => precompute some values */
        tmp0 = v4f_sub(lower_min_range, rays[0].org);
        tmp1 = v4f_sub(upper_max_range, rays[0].org);

        if(rays[iray_1st].is_enabled) /* Early hit */
          intersect = aos_ray_intersect_svo_voxel(rays+iray_1st, tmp0, tmp1, t, NULL);

        if(!intersect) {
          iray_1st = nrays;
          intersect = aos_packet_intersect_aos_aabb
            (packet, lower_min_range, upper_max_range, t);
          if(intersect) { /* !Early fail */
            /* Look for the 1st active ray */
            FOR_EACH(iray_1st, iray + 1, nrays) {
              if(!rays[iray_1st].is_enabled) continue;
              if(aos_ray_intersect_svo_voxel(rays+iray_1st, tmp0, tmp1, t, NULL))
                break;
            }
          }
        }

        if(iray_1st < nrays) { /* The packet hit the child node */
          struct svo_index inode_next;

          /* Retriver the index of the child node */
          svo_get_child_index(svo, &inode, (int)ichild, &inode_next);

          if(rt_mixed
          && aos_is_tiny_node(t[0], lower_min_range, upper_max_range, footprint)) {
            /* Packet is too incoherent: fall-back in single ray traversal */
            nrays_intersected += trace_single_ray(svo, &inode_next,
              lower_min_range, v4f_x(scale_exp2), lod_range, &iray_1st, rays,
              svo_rays, nrays, hits);
            if(nrays_intersected == nrays) break; /* No more active ray */

          } else {
            v4f_T mid;

            /* Save the current node */
            stack[scale].iray = iray;
            stack[scale].inode = inode;
            stack[scale].ichild = ichild;
            stack[scale].nchildren = nchildren;

            inode = inode_next;
            iray = iray_1st;
            nchildren = 0;
            scale_exp2 = v4f_mul(scale_exp2, v4f_set(0.5f, 0.5f, 0.5f, 1.f));
            scale -= 1;
            nchildren = 0;

            /* Define the id and the lower left corner of the first child */
            mid = v4f_add(lower_min_range, scale_exp2);
            tmp0 = v4f_gt(packet_org, mid);
            lower_min_range = v4f_sel(lower_min_range, mid, tmp0);
            ichild = ichild_lut[v4f_movemask(tmp0)];
            continue;
          }
        }
      }
    }

    if(nchildren < 8 && iray < nrays/*#Active rays in the node*/) {
      int32_t ALIGN(16) mask[4] = { 0, 0, 0, 0 };

      /* Use the traversal bit indices to ensure a front to back traversal of
       * the children. Refer to the aos_setup_traversal_ibits for details. */
      ichild ^= BIT(traversal_ibits[0]);
      mask[2-traversal_ibits[0]] = BIT(scale);

      if(nchildren % 2 == 0) {
        ichild ^= BIT(traversal_ibits[1]);
        mask[2 - traversal_ibits[1]] = BIT(scale);
      }
      if(nchildren % 4 == 0) {
        ichild ^= BIT(traversal_ibits[2]);
        mask[2 - traversal_ibits[2]] = BIT(scale);
      }
      lower_min_range = v4f_xor(lower_min_range, v4i_rcast_v4f(v4i_load(mask)));
    } else { /* Pop node */
      v4i_T tmpi;
      float pop_exp2;
      int scale_parent;
      int32_t mask;

      /* Pop a node with children to traverse. The SCALE_MAX entry prevents
       * overflow */
      for(++scale; stack[scale].nchildren >= 8; ++scale);

      /* Pop the node data */
      inode = stack[scale].inode;
      iray = stack[scale].iray;
      ichild = stack[scale].ichild;
      nchildren = stack[scale].nchildren;

      /* Setup the next child to traverse. Use the traversal bit indices to
       * ensure a front to back traversal of the children. Refer to the
       * aos_setup_traversal_ibits for details. */
      ichild ^= BIT(traversal_ibits[0]);
      if(nchildren % 2 == 0) ichild ^= BIT(traversal_ibits[1]);
      if(nchildren % 4 == 0) ichild ^= BIT(traversal_ibits[2]);

      pop_exp2 = uitof(((uint32_t)(scale - SCALE_MAX) + 127) << 23);
      scale_exp2 = v4f_set(pop_exp2, pop_exp2, pop_exp2, range_length);
      scale_parent = scale + 1;
      mask = (1 << scale_parent) - 1;

      /* Define the lower left corner of the parent node */
      tmp0 = v4f_mask(mask, mask, mask, 0);
      tmpi = v4i_and(v4i_set1(ichild), v4i_set(4, 2, 1, 0));
      tmpi = v4i_neq(tmpi, v4i_zero());
      tmp1 = v4f_and(scale_exp2, v4i_rcast_v4f(tmpi));
      lower_min_range = v4f_andnot(tmp0, lower_min_range);
      lower_min_range = v4f_add(lower_min_range, tmp1);
    }
  }
}

/*******************************************************************************
 * Local back-end function
 ******************************************************************************/
res_T
svo_trace_packets_aos
  (struct vxl_be_svo* svo,
   const enum vxl_rt_coherency coherency,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float org[3],
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits)
{
  v4f_T vorg;
  struct aos_ray rays[MAX_PACKET_WIDTH];
  struct svo_ray svo_rays[MAX_PACKET_WIDTH];
  size_t dir_step, range_step;
  size_t idir, irange;
  size_t iray;
  float svo_size;
  float svo_scale;
  float vxl_size;
  int traversal_ibits[3];
  const char rt_mixed = coherency == VXL_RT_COHERENCY_MIXED;
  size_t packet_width;
  ASSERT(coherency != VXL_RT_COHERENCY_NONE);

  if(!svo
  || !lod_range || lod_range[0] > lod_range[1]
  || !nrays || !org || !dirs || !ranges || !hits)
    return RES_BAD_ARG;

  /* The SVO packet tracing cannot be invoked on bundle whose rays does not
   * share the same origin */
  if(!(mask & VXL_RAYS_SINGLE_ORIGIN))
    return RES_BAD_ARG;

  /* Initialize the hit array. Note that currently, the packet traversal stops
   * at the same SVO level for all rays. The size of the intersected voxel is
   * thus computed once */
  vxl_size = compute_intersected_voxel_size(svo, lod_range[1]);
  FOR_EACH(iray, 0, nrays) {
    hits[iray].distance = (float)INF;
    hits[iray].size = vxl_size;
  }

  /* Setup the packet width */
  switch(coherency) {
    case VXL_RT_COHERENCY_HIGH: packet_width = 256; break;
    case VXL_RT_COHERENCY_MEDIUM: packet_width = 64; break;
    case VXL_RT_COHERENCY_LOW: packet_width = 16; break;
    case VXL_RT_COHERENCY_MIXED: packet_width = 64; break;
    default: FATAL("Unreachable code\n"); break;
  }
  ASSERT(packet_width <= MAX_PACKET_WIDTH);

  /* Assume that the SVO is a cube */
  svo_size = svo->upper[0] - svo->lower[0];
  ASSERT(eq_epsf(svo->upper[1] - svo->lower[1], svo_size, svo_size*1.e-4f));
  ASSERT(eq_epsf(svo->upper[2] - svo->lower[2], svo_size, svo_size*1.e-4f));

  /* Transform the ray org in [1, 2]^3 space. Ensure 0 on fourth dummy
   * component (see aos_ray_setup) */
  svo_scale = 1.f / svo_size;
  vorg = v4f_set(org[0], org[1], org[2], 0.f);
  vorg = v4f_sub(vorg, v4f_set(svo->lower[0], svo->lower[1], svo->lower[2], 0));
  vorg = v4f_mul(vorg, v4f_set1(svo_scale));
  vorg = v4f_add(vorg, v4f_set(1.f, 1.f, 1.f, 0.f));

  /* Setup ray data layout */
  dir_step = mask & VXL_RAYS_SINGLE_DIRECTION ? 0 : 3;
  range_step = mask & VXL_RAYS_SINGLE_RANGE ? 0 : 2;
  idir = 0, irange = 0;

  iray = 0;
  while(iray < nrays) {
    struct aos_packet packet;
    struct aos_ia3 iadir;
    struct aos_ia3 iaorg;
    float iarange[2] = { FLT_MAX, -FLT_MAX };
    size_t iray_tmp = 0;
    size_t iray_packet = 0;
    size_t nrays_packet;
    v4f_T packet_main_dir;
    v4f_T vdir;

    aos_ia3_set1(&iadir, FLT_MAX, -FLT_MAX);
    aos_ia3_set(&iaorg, vorg, vorg);

    FOR_EACH(iray_tmp, 0, packet_width) { /* Fill the ray packet */
      if(ranges[irange + 0] < ranges[irange + 1]) {
        if(ranges[irange + 0] < 0.f) return RES_BAD_ARG;

        /* Setup  the packet ray in [0, 1] range */
        vdir = v4f_set(dirs[idir + 0], dirs[idir + 1], dirs[idir + 2], 1.f);
        aos_ray_setup(rays + iray_packet, vorg, vdir, ranges + irange, iray);
        rays[iray_packet].range[0] *= svo_scale;
        rays[iray_packet].range[1] *= svo_scale;

        /* Update the ray packet interval */
        iadir.lower = v4f_min(iadir.lower, vdir);
        iadir.upper = v4f_max(iadir.upper, vdir);
        iarange[0] = MMIN(iarange[0], rays[iray_packet].range[0]);
        iarange[1] = MMAX(iarange[1], rays[iray_packet].range[1]);

        /* Setup the single ray fallback */
        if(rt_mixed) {
          const res_T res = svo_setup_ray(svo, org, dirs + idir, NULL, NULL,
            ranges + irange, &svo_rays[iray_packet]);
          ASSERT(res == RES_OK); (void)res;
        }

        ++iray_packet;
      }
      if(++iray >= nrays) {
        break;
      } else {
        idir += dir_step;
        irange += range_step;
      }
    }
    if(!iray_packet) continue; /* No valid ray */

    /* Setup the ray packet */
    nrays_packet = iray_packet;
    aos_packet_setup(&packet, &iaorg, &iadir, iarange);

    /* Define the SVO node traversal order */
    packet_main_dir = v4f_add(iadir.lower, iadir.upper);
    packet_main_dir = v4f_mul(packet_main_dir, v4f_set1(0.5f));
    aos_setup_traversal_ibits(traversal_ibits, packet_main_dir);

    /* The packet footprint is used to switch from packet tracing to single ray
     * tracing when the bundle becomes too incoherent */
    if(!rt_mixed) {
      trace(svo, lod_range, &svo->root, rays, NULL, nrays_packet, &packet,
        v4f_zero(), traversal_ibits, hits);
    } else {
      const v4f_T footprint = aos_packet_footprint(&iadir);
      trace(svo, lod_range, &svo->root, rays, svo_rays, nrays_packet, &packet,
        footprint, traversal_ibits, hits);
    }
  }
  return RES_OK;
}

