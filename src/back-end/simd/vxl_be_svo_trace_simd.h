/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_TRACE_SIMD_H
#define VXL_BE_SVO_TRACE_SIMD_H

#include "vxl.h"

struct vxl_be_svo;

/*******************************************************************************
 * SIMD SVO trace functions
 ******************************************************************************/
extern LOCAL_SYM res_T
svo_trace_packets_aos
  (struct vxl_be_svo* svo,
   const enum vxl_rt_coherency coherency,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float org[3],
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits);

#endif /* VXL_BE_SVO_SIMD_H */

