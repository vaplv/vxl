/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_AOS_PACKET_H
#define VXL_BE_AOS_PACKET_H

#include "legacy/vxl_be_aabb.h"
#include "vxl_be_aos_interval.h"

#include <rsys/float2.h>

struct aos_packet { /* AoS ray packet */
  struct aos_ia3 org; /* Interval on ray origins */
  struct aos_ia3 rcp_dir; /* Interval of the reciprocal ray directions */
  float range[2]; /* Interval of the ray ranges */
  char single_org;
};

static FINLINE void
aos_packet_setup
  (struct aos_packet* pkt,
   const struct aos_ia3* org,
   const struct aos_ia3* dir,
   const float range[2])
{
  const v4f_T mask_w = v4f_mask(0, 0, 0, ~0);
  ASSERT(pkt && org && dir);
  pkt->org = *org;
  f2_set(pkt->range, range);
  aos_ia3_rcp(&pkt->rcp_dir, dir);

  pkt->single_org = 7 == (v4f_movemask
    (v4f_eq_eps(org->lower, org->upper, v4f_set1(1.e-6f))) & 7);

  /* Ensure that the fourth dummy component of org and rcp_dir is 0 and 1
   * respectively. This make easier the packet/AABB intersection test */
  pkt->org.lower = v4f_and(pkt->org.lower, v4f_mask(~0, ~0, ~0, 0));
  pkt->org.upper = v4f_and(pkt->org.upper, v4f_mask(~0, ~0, ~0, 0));
  pkt->rcp_dir.lower = v4f_sel(pkt->rcp_dir.lower, v4f_set1(1.f), mask_w);
  pkt->rcp_dir.upper = v4f_sel(pkt->rcp_dir.upper, v4f_set1(1.f), mask_w);
}

static FINLINE char
aos_packet_intersect_aos_aabb
  (const struct aos_packet* packet,
   const v4f_T lower_min_range, /* Packed AABB lower bound and packet min range */
   const v4f_T upper_max_range, /* Packed AABB upper bound and packet max range */
   v4f_T out_t[2]) /* May be NULL */
{
  struct aos_ia3 t, box;
  v4f_T max_lower, min_upper;
  ASSERT(packet);
  ASSERT(v4f_w(packet->org.lower) == 0.f);
  ASSERT(v4f_w(packet->org.upper) == 0.f);
  ASSERT(v4f_w(packet->rcp_dir.lower) == 1.f);
  ASSERT(v4f_w(packet->rcp_dir.upper) == 1.f);
  ASSERT(eq_eps(v4f_w(lower_min_range), packet->range[0], 1.e-6f));
  ASSERT(eq_eps(v4f_w(upper_max_range), packet->range[1], 1.e-6f));

  box.lower = lower_min_range;
  box.upper = upper_max_range;
  aos_ia3_sub(&t, &box, &packet->org);
  aos_ia3_mul(&t, &t, &packet->rcp_dir);

  ASSERT(v4f_w(t.lower) == packet->range[0]);
  ASSERT(v4f_w(t.upper) == packet->range[1]);
  max_lower = v4f_reduce_max(t.lower);
  min_upper = v4f_reduce_min(t.upper);
  if(out_t) {
    out_t[0] = max_lower;
    out_t[1] = min_upper;
  }
  return v4f_movemask(v4f_le(max_lower, min_upper)) != 0;
}

static FINLINE char
aos_packet_intersect_aabb
  (const struct aos_packet* packet, const struct aabb* aabb)
{
  const v4f_T lower_min_range = v4f_set
    (aabb->lower[0], aabb->lower[1], aabb->lower[2], packet->range[0]);
  const v4f_T upper_max_range = v4f_set
    (aabb->upper[0], aabb->upper[1], aabb->upper[2], packet->range[1]);
  return aos_packet_intersect_aos_aabb
    (packet, lower_min_range, upper_max_range, NULL);
}

#endif /* VXL_BE_AOS_PACKET_H */

