/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_H
#define VXL_BE_H

#include "vxl.h"

struct mem_allocator;

struct vxl_be_svo_desc {
  float lower[3], upper[3]; /* World space AABB */
  size_t voxels_count; /* Total number of voxels partitioned in the SVO */
  uint16_t definition; /* SVO discretization along the 3 axis */
  size_t mem_size; /* Memory size in bytes of the SVO */
};

/* Check the values of the vxl_face enum. Several pieces of code rely on this
 * specific order to setup the voxel face that is hit by a ray. */
STATIC_ASSERT
  (  VXL_FACE_LEFT == 0 && VXL_FACE_BOTTOM == 2 && VXL_FACE_BACK == 4
  && VXL_FACE_RIGHT == 1 && VXL_FACE_TOP == 3 && VXL_FACE_FRONT == 5,
  Unexpected_values_for_the_vxl_face_enum);

/* Opaque back-end API types */
struct vxl_be_device;
struct vxl_be_svo;

BEGIN_DECLS

/*******************************************************************************
 * Device - Back-end entry point
 ******************************************************************************/
VXL_API res_T
vxl_be_device_create
  (struct mem_allocator* allocator,
   const unsigned nthreads_hint, /* Hint on the number of threads to use */
   struct vxl_be_device** dev);

VXL_API res_T
vxl_be_device_ref_get
  (struct vxl_be_device* dev);

VXL_API res_T
vxl_be_device_ref_put
  (struct vxl_be_device* dev);

/* Return the maximum number of threads supported by the device */
VXL_API res_T
vxl_be_device_get_nthreads
  (const struct vxl_be_device* dev,
   unsigned* nthreads);

/*******************************************************************************
 * Sparse Voxel Octree - hierarchical space partitioning data structure
 ******************************************************************************/
VXL_API res_T
vxl_be_svo_create
  (struct vxl_be_device* dev,
   struct vxl_be_svo** svo);

VXL_API res_T
vxl_be_svo_ref_get
  (struct vxl_be_svo* svo);

VXL_API res_T
vxl_be_svo_ref_put
  (struct vxl_be_svo* svo);

/* Voxelize the submitted meshes and build a SVO from the resulting voxels. */
VXL_API res_T
vxl_be_svo_build
  (struct vxl_be_svo* svo,
   const size_t definition, /* Overall definition of the SVO */
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const enum vxl_mem_location mem_location,
   struct time* vxlzr_timer, /* Voxelization timer. May be NULL */
   struct time* build_timer); /* SVO Build timer. May be NULL */

VXL_API res_T
vxl_be_svo_get_desc
  (const struct vxl_be_svo* svo,
   struct vxl_be_svo_desc* desc);

VXL_API res_T
vxl_be_svo_clear
  (struct vxl_be_svo* svo);

VXL_API res_T
vxl_be_svo_trace_ray
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const float ray_origin[3],
   const float ray_direction[3], /* Must be normalized */
   const float ray_range[2], /* range[0] >= range[1] <=> ray is disabled */
   struct vxl_hit* hit);

/* Trace a differential ray */
VXL_API res_T
vxl_be_svo_trace_dray
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const float ray_origin[3],
   const float ray_dir[3], /* Must be normalized */
   const float ray_dir_dx[3], /* Differential dir in x. Must be normalized */
   const float ray_dir_dy[3], /* Differential dir in y. Must be normalized */
   const float ray_range[2], /* range[0] >= range[1] <=> ray is disabled */
   struct vxl_hit* hit);

VXL_API res_T
vxl_be_svo_trace_rays
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask, /* Combination of vxl_ray_flag */
   const float* orgs,
   const float* dirs,
   const float* ranges, /* (ranges+i)[0] >= (ranges+i)[1] <=> ray is disabled */
   struct vxl_hit* hits);

/* Trace a set of differential rays */
VXL_API res_T
vxl_be_svo_trace_drays
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask, /* Combination of vxl_ray_flag */
   const float* orgs,
   const float* dirs,
   const float* dirs_dx,
   const float* dirs_dy,
   const float* ranges, /* (ranges+i)[0] >= (ranges+i)[1] <=> ray is disabled */
   struct vxl_hit* hits);

END_DECLS

#endif /* VXL_BE_H */

