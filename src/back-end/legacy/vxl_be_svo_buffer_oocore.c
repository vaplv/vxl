/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */

#include "vxl_be_svo_buffer_oocore.h"

#include <errno.h>
#include <sys/mman.h>
#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
flush_nodes(struct svo_buffer_oocore* buf)
{
  long fp_nodes;
  long fp_attrs;
  size_t n;
  res_T res = RES_OK;
  ASSERT(buf);

  /* Retrieve the current file pointers */
  fp_nodes = ftell(buf->stream_nodes);
  fp_attrs = ftell(buf->stream_attrs);

  /* Flush the page of nodes */
  n = fwrite(buf->nodes, 1, buf->pagesize, buf->stream_nodes);
  if(n != buf->pagesize) { res = RES_IO_ERR; goto error; }

  /* Flush the page of attribs */
  n = fwrite(buf->attrs, 1, buf->pagesize, buf->stream_attrs);
  if(n != buf->pagesize) { res = RES_IO_ERR; goto error; }

  /* Update the index toward the next valid node */
  ASSERT(buf->node_head.ipage < SVO_INDEX_IPAGE_MAX);
  ASSERT(SVO_INDEX_EQ(&buf->node_head, &buf->attr_head));

  buf->node_head.inode = 0;
  buf->attr_head.inode = 0;
  buf->node_head.ipage++;
  buf->attr_head.ipage++;

exit:
  return res;
error:
  CHK(fseek(buf->stream_nodes, fp_nodes, SEEK_SET) >= 0);
  CHK(fseek(buf->stream_attrs, fp_attrs, SEEK_SET) >= 0);
  goto exit;
}

static res_T
flush_leafs(struct svo_buffer_oocore* buf)
{
  long fp_leafs;
  size_t n;
  res_T res = RES_OK;

  /* Retrieve the current file pointer */
  fp_leafs = ftell(buf->stream_leafs);

  /* Flush the page of leafs */
  n = fwrite(buf->leafs, 1, buf->pagesize, buf->stream_leafs);
  if(n != buf->pagesize) { res = RES_IO_ERR; goto error; }

  ASSERT(buf->leaf_head.ipage < SVO_INDEX_IPAGE_MAX);
  buf->leaf_head.inode = 0;
  buf->leaf_head.ipage++;

exit:
  return res;
error:
  CHK(fseek(buf->stream_leafs, fp_leafs, SEEK_SET) >= 0);
  goto exit;
}

static FINLINE res_T
ensure_allocated_nodes(struct svo_buffer_oocore* buf, const size_t nnodes)
{
  ASSERT(buf && !svo_buffer_oocore_is_finalized(buf));
  ASSERT(SVO_INDEX_EQ(&buf->node_head, &buf->attr_head));

  /* Sufficient memory space in the current page */
  if(buf->node_head.inode + nnodes <= buf->page_nnodes) {
    return RES_OK;
  } else {
    return flush_nodes(buf);
  }
}

static res_T
ensure_allocated_leafs(struct svo_buffer_oocore* buf, const size_t nleafs)
{
  ASSERT(buf && !svo_buffer_oocore_is_finalized(buf));

  /* Sufficient memory space in the current page of leafs */
  if(buf->leaf_head.inode + nleafs <= buf->page_nnodes) {
    return RES_OK;
  } else {
    return flush_leafs(buf);
  }
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
svo_buffer_oocore_init
  (struct mem_allocator* allocator, struct svo_buffer_oocore* buf)
{
  ASSERT(buf);
  memset(buf, 0, sizeof(*buf));

  buf->allocator = allocator;
  buf->node_head = SVO_INDEX_NULL;
  buf->attr_head = SVO_INDEX_NULL;
  buf->leaf_head = SVO_INDEX_NULL;
  buf->node_tail = SVO_INDEX_NULL;
  buf->attr_tail = SVO_INDEX_NULL;
  buf->leaf_tail = SVO_INDEX_NULL;
}

void
svo_buffer_oocore_release(struct svo_buffer_oocore* buf)
{
  char* mem;
  ASSERT(buf);
  if(buf->stream_nodes) fclose(buf->stream_nodes);
  if(buf->stream_attrs) fclose(buf->stream_attrs);
  if(buf->stream_leafs) fclose(buf->stream_leafs);
  if((mem = buf->nodes)) MEM_RM(buf->allocator, mem);
  if(buf->nodes_mmap_len) CHK(!munmap(buf->nodes_mmap, buf->nodes_mmap_len));
  if(buf->attrs_mmap_len) CHK(!munmap(buf->attrs_mmap, buf->attrs_mmap_len));
  if(buf->leafs_mmap_len) CHK(!munmap(buf->leafs_mmap, buf->leafs_mmap_len));
}

res_T
svo_buffer_oocore_setup
  (struct svo_buffer_oocore* buf,
   const uint16_t id,
   const size_t pagesize)
{
  char* mem = NULL;
  size_t memsz;
  res_T res = RES_OK;

  ASSERT(buf && pagesize);
  ASSERT(buf->node_head.ibuffer == SVO_INDEX_NULL.ibuffer);
  ASSERT(buf->leaf_head.ibuffer == SVO_INDEX_NULL.ibuffer);

  buf->pagesize = pagesize;
  buf->page_nnodes = pagesize/sizeof(struct svo_xnode);
  buf->page_nnodes = MMIN(buf->page_nnodes, SVO_INDEX_INODE_MAX);

  memsz = 3/*#pages*/ * buf->pagesize;
  mem = MEM_ALLOC_ALIGNED(buf->allocator, memsz, 16);
  if(!mem) {
    res = RES_MEM_ERR;
    goto error;
  }
  buf->nodes = mem + 0*buf->pagesize;
  buf->attrs = mem + 1*buf->pagesize;
  buf->leafs = mem + 2*buf->pagesize;

  if(!(buf->stream_nodes = tmpfile())) { res = RES_IO_ERR; goto error; }
  if(!(buf->stream_attrs = tmpfile())) { res = RES_IO_ERR; goto error; }
  if(!(buf->stream_leafs = tmpfile())) { res = RES_IO_ERR; goto error; }

  buf->node_head.ibuffer = id;
  buf->attr_head.ibuffer = id;
  buf->leaf_head.ibuffer = id;
  buf->node_head.ipage = 0;
  buf->attr_head.ipage = 0;
  buf->leaf_head.ipage = 0;

exit:
  return res;
error:
  goto exit;
}

res_T
svo_buffer_oocore_alloc_nodes
  (struct svo_buffer_oocore* buf,
   const size_t nnodes,
   struct svo_index* first_node,
   struct svo_xnode** nodes,
   struct svo_node_attrib** attribs)
{
  res_T res = RES_OK;
  ASSERT(buf && nnodes && first_node && nodes && attribs);
  ASSERT(!SVO_INDEX_EQ(&buf->node_head, &SVO_INDEX_NULL));
  ASSERT(SVO_INDEX_EQ(&buf->node_head, &buf->attr_head));

  if(nnodes > buf->page_nnodes) return RES_MEM_ERR;

  res = ensure_allocated_nodes(buf, nnodes);
  if(res != RES_OK) return res;

  *first_node = buf->node_head; /* Setup the index toward the nodes */
  buf->node_head.inode = (node_id_T)(buf->node_head.inode + nnodes);
  buf->attr_head.inode = (node_id_T)(buf->attr_head.inode + nnodes);

  *nodes = ((struct svo_xnode*)buf->nodes) + first_node->inode;
  *attribs = ((struct svo_node_attrib*)buf->attrs) + first_node->inode;

  return RES_OK;
}

res_T
svo_buffer_oocore_alloc_far_index
  (struct svo_buffer_oocore* buf,
   struct svo_index* id,
   struct svo_index** far_index)
{
  size_t remaining_size;
  size_t skipped_nnodes;
  ASSERT(buf && id && far_index);
  ASSERT(!SVO_INDEX_EQ(&buf->node_head, &SVO_INDEX_NULL));

  /* Compute the remaining memory size in the current node page */
  remaining_size = buf->page_nnodes - buf->node_head.inode;
  remaining_size = remaining_size * sizeof(struct svo_xnode);

  /* Not enough memory in the current page */
  if(sizeof(struct svo_index) > remaining_size) return RES_MEM_ERR;

  *id = buf->node_head; /* Setup the index toward the far index */
  *far_index = (struct svo_index*)(((struct svo_xnode*)buf->nodes) + id->inode);

  /* Compute the number of skipped nodes */
  skipped_nnodes = sizeof(struct svo_index) / sizeof(struct svo_xnode);
  buf->node_head.inode = (node_id_T)(buf->node_head.inode + skipped_nnodes);

  /* Skip as many attribs as skipped nodes since the attrib are indexed by the
   * node identifier */
  buf->attr_head.inode = (node_id_T)(buf->attr_head.inode + skipped_nnodes);
  return RES_OK;
}

res_T
svo_buffer_oocore_alloc_leaf_attribs
  (struct svo_buffer_oocore* buf,
   const size_t nleafs,
   struct svo_index* first_attrib,
   struct svo_node_attrib** attribs)
{
  res_T res = RES_OK;
  ASSERT(buf && nleafs && first_attrib);

  if(nleafs > buf->page_nnodes) return RES_MEM_ERR;

  res = ensure_allocated_leafs(buf, nleafs);
  if(res != RES_OK) return res;

  *first_attrib = buf->leaf_head;
  *attribs = ((struct svo_node_attrib*)buf->leafs) + first_attrib->inode;

  buf->leaf_head.inode = (node_id_T)(buf->leaf_head.inode + nleafs);
  return RES_OK;
}

res_T
svo_buffer_oocore_finalize(struct svo_buffer_oocore* buf)
{
  res_T res = RES_OK;
  ASSERT(buf && !svo_buffer_oocore_is_finalized(buf));

  if(RES_OK != (res = flush_nodes(buf))) return res;
  if(RES_OK != (res = flush_leafs(buf))) return res;

  /* Save the index toward the end of the buffer */
  buf->node_tail = buf->node_head;
  buf->attr_tail = buf->attr_head;
  buf->leaf_tail = buf->leaf_head;

  #define MMAP(Name) {                                                         \
    fseek(buf->stream_##Name, 0, SEEK_END);                                    \
    buf->Name##_mmap_len = (size_t)ftell(buf->stream_##Name);                  \
    buf->Name##_mmap = mmap(NULL, buf->Name##_mmap_len, PROT_READ,             \
      MAP_PRIVATE, fileno(buf->stream_##Name), 0);                             \
    if(buf->Name##_mmap == MAP_FAILED) {                                       \
      buf->Name##_mmap_len = 0;                                                \
      return RES_IO_ERR;                                                       \
    }                                                                          \
  } (void)0
  MMAP(nodes);
  MMAP(attrs);
  MMAP(leafs);
  #undef MMAP

  return RES_OK;
}

int
svo_buffer_oocore_is_finalized(const struct svo_buffer_oocore* buf)
{
  ASSERT(buf);
  return !SVO_INDEX_EQ(&buf->node_tail, &SVO_INDEX_NULL)
      && !SVO_INDEX_EQ(&buf->attr_tail, &SVO_INDEX_NULL)
      && !SVO_INDEX_EQ(&buf->leaf_tail, &SVO_INDEX_NULL);
}

res_T
svo_buffer_oocore_copy_incore
  (struct svo_buffer_oocore* src, struct svo_buffer_incore* dst)
{
  struct svo_xnode* nodes_src;
  struct svo_xnode* nodes_dst;
  struct svo_node_attrib* attribs_src;
  struct svo_node_attrib* attribs_dst;
  struct svo_index id_src;
  struct svo_index id_dst;
  res_T res = RES_OK;
  ASSERT(src && dst && svo_buffer_oocore_is_finalized(src));
  ASSERT(src->pagesize == dst->pagesize);

  svo_buffer_incore_clear(dst);

  /* Copy the nodes */
  id_src.ibuffer = src->node_head.ibuffer;
  id_src.ipage = 0;
  id_src.inode = 0;
  while(!SVO_INDEX_EQ(&id_src, &src->node_tail)) {
    res = svo_buffer_incore_alloc_nodes
      (dst, src->page_nnodes, &id_dst, &nodes_dst, &attribs_dst);
    if(res != RES_OK) goto error;

    /* Fetch the first node of the source page and copy the whole page toward
     * the destination */
    nodes_src = svo_buffer_oocore_get_node(src, &id_src);
    memcpy(nodes_dst, nodes_src, src->pagesize);
    ++id_src.ipage;
  }

  /* Copy the attribs */
  id_dst.ipage = 0;
  id_src.ipage = 0;
  id_dst.inode = 0;
  id_src.inode = 0;
  while(!SVO_INDEX_EQ(&id_src, &src->attr_tail)) {

    /* Fetch the first attrib of the src and dst attributes and copy the whole
     * page. Note that the dst attribs were allocated during the copy of the
     * nodes */
    attribs_src = svo_buffer_oocore_get_node_attrib(src, &id_src);
    attribs_dst = svo_buffer_incore_get_node_attrib(dst, &id_dst);
    memcpy(attribs_dst, attribs_src, src->pagesize);

    ++id_src.ipage;
    ++id_dst.ipage;
  }

  /* Copy the leafs */
  id_src.ipage = 0;
  id_src.inode = 0;
  while(!SVO_INDEX_EQ(&id_src, &src->leaf_tail)) {

    res = svo_buffer_incore_alloc_leaf_attribs
      (dst, src->page_nnodes, &id_dst, &attribs_dst);
    if(res != RES_OK) goto error;

    /* Fetch the first attrib of the source page and copy the whole page
     * toward the destination */
    attribs_src = svo_buffer_oocore_get_leaf_attrib(src, &id_src);
    memcpy(attribs_dst, attribs_src, src->pagesize);
    ++id_src.ipage;
  }

exit:
  return res;
error:
  svo_buffer_incore_clear(dst);
  goto exit;
}

