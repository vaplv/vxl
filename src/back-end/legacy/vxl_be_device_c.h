/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_DEVICE_C_H
#define VXL_BE_DEVICE_C_H

#include "vxl_be.h"
#include <rsys/ref_count.h>

struct vxl_be_device {
  unsigned nthreads; /* Max #threads internally used by the back-end */
  size_t pagesize; /* Memory page size */
  ref_T ref;
  struct mem_allocator* allocator;
};

#endif /* VXL_BE_DEVICE_C_H */

