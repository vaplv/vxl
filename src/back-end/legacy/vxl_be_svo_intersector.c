/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "legacy/vxl_be_device_c.h"
#include "legacy/vxl_be_svo_trace.h"
#include "simd/vxl_be_svo_trace_simd.h"
#include "vxl_be_svo_c.h"

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static FINLINE int
check_rt_desc(const struct vxl_rt_desc* rt_state)
{
  return rt_state && rt_state->lod_min <= rt_state->lod_max;
}

/*******************************************************************************
 * Exported back-end functions
 ******************************************************************************/
res_T
vxl_be_svo_trace_ray
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const float org[3],
   const float dir[3],
   const float range[2],
   struct vxl_hit* hit)
{
  uint32_t lod_range[2];

  if(!check_rt_desc(rt_state))
    return RES_BAD_ARG;

  if(svo_is_empty(svo)) {
    hit->distance = (float)INF;
    return RES_OK;
  }

  lod_range[0] = rt_state->lod_min;
  lod_range[1] = rt_state->lod_max;
  return svo_trace_ray
    (svo, lod_range, org, dir, NULL, NULL, range, hit);
}

res_T
vxl_be_svo_trace_dray
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const float org[3],
   const float dir[3],
   const float dir_dx[3],
   const float dir_dy[3],
   const float range[2],
   struct vxl_hit* hit)
{
  uint32_t lod_range[2];

  if(!check_rt_desc(rt_state) || !dir_dx || !dir_dy)
    return RES_BAD_ARG;

  if(svo_is_empty(svo)) {
    hit->distance = (float)INF;
    return RES_OK;
  }

  lod_range[0] = rt_state->lod_min;
  lod_range[1] = rt_state->lod_max;
  return svo_trace_ray
    (svo, lod_range, org, dir, dir_dx, dir_dy, range, hit);
}

res_T
vxl_be_svo_trace_rays
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits)
{
  uint32_t lod_range[2];
  res_T res = RES_OK;

  if(!check_rt_desc(rt_state))
    return RES_BAD_ARG;

  if(svo_is_empty(svo)) {
    size_t i;
    FOR_EACH(i, 0, nrays) hits[i].distance = (float)INF;
    return RES_OK;
  }

  lod_range[0] = rt_state->lod_min;
  lod_range[1] = rt_state->lod_max;
  if(rt_state->coherency == VXL_RT_COHERENCY_NONE
  || (mask & VXL_RAYS_SINGLE_ORIGIN) == 0) { /*Packet tracing is not supported*/
    res = svo_trace_rays(svo, lod_range, nrays, mask, orgs, dirs,
      NULL, NULL, ranges, hits);
  } else if(rt_state->simd) {
    res = svo_trace_packets_aos(svo, rt_state->coherency, lod_range,
      nrays, mask, orgs, dirs, ranges, hits);
  } else  {
    res = svo_trace_packets(svo, rt_state->coherency, lod_range,
      nrays, mask, orgs, dirs, ranges, hits);
  }
  return res;
}

res_T
vxl_be_svo_trace_drays
  (struct vxl_be_svo* svo,
   const struct vxl_rt_desc* rt_state,
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* dirs_dx,
   const float* dirs_dy,
   const float* ranges,
   struct vxl_hit* hits)
{
  uint32_t lod_range[2];

  if(!check_rt_desc(rt_state) || !dirs_dx || !dirs_dy)
    return RES_BAD_ARG;

  if(svo_is_empty(svo)) {
    size_t i;
    FOR_EACH(i, 0, nrays) hits[i].distance = (float)INF;
    return RES_OK;
  }

  lod_range[0] = rt_state->lod_min;
  lod_range[1] = rt_state->lod_max;

  return svo_trace_rays(svo, lod_range, nrays, mask, orgs, dirs,
    dirs_dx, dirs_dy, ranges, hits);
}

