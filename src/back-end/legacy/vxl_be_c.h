/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_C_H
#define VXL_BE_C_H

#include <rsys/rsys.h>

/* Empirical scale used to extend the AABB of the meshes to handle numerical
 * imprecision when a mesh primitive lies onto the AABB */
#define MESHES_AABB_SCALE 1.01f

enum snap_type {
  SNAP_CEIL,
  SNAP_FLOOR,
  SNAP_ROUND
};

/* Snap a floating point value to a discret integer */
static FINLINE int32_t
snap_float(const float fp, const float unit, enum snap_type type)
{
  int32_t res;
  ASSERT(unit > 0.f);
  switch(type) {
    case SNAP_CEIL:
      res = (int32_t)(fp / unit + (fp >= 0.f ?  1.f : 0.f));
      break;
    case SNAP_FLOOR:
      res = (int32_t)(fp / unit + (fp < 0.f ? -1.f : 0.f));
      break;
    case SNAP_ROUND:
      res = (int32_t)(fp / unit + (fp < 0.f ? -0.5f : 0.5f));
      break;
    default: FATAL("Unreachable code\n"); break;
  }
  return res;
}

static INLINE uint64_t
morton3D_encode_u21(const uint32_t u21)
{
  uint64_t u64 = u21 & ((1<<21) - 1);
  ASSERT(u21 <= ((1 << 21) - 1));
  u64 = (u64 | (u64 << 32)) & 0xFFFF00000000FFFF;
  u64 = (u64 | (u64 << 16)) & 0x00FF0000FF0000FF;
  u64 = (u64 | (u64 << 8))  & 0xF00F00F00F00F00F;
  u64 = (u64 | (u64 << 4))  & 0x30C30C30C30C30C3;
  u64 = (u64 | (u64 << 2))  & 0x9249249249249249;
  return u64;
}

static INLINE uint32_t
morton3D_decode_u21(const uint64_t u64)
{
  uint64_t tmp = (u64 & 0x9249249249249249);
  tmp = (tmp | (tmp >> 2))  & 0x30C30C30C30C30C3;
  tmp = (tmp | (tmp >> 4))  & 0xF00F00F00F00F00F;
  tmp = (tmp | (tmp >> 8))  & 0x00FF0000FF0000FF;
  tmp = (tmp | (tmp >> 16)) & 0xFFFF00000000FFFF;
  tmp = (tmp | (tmp >> 32)) & 0x00000000FFFFFFFF;
  ASSERT(tmp <= ((1<<21)-1));
  return (uint32_t)tmp;
}

static INLINE uint64_t
morton_xyz_encode_u21(const uint32_t xyz[3])
{
  return (morton3D_encode_u21(xyz[0]) << 2)
       | (morton3D_encode_u21(xyz[1]) << 1)
       | (morton3D_encode_u21(xyz[2]) << 0);
}

static INLINE void
morton_xyz_decode_u21(const uint64_t code, uint32_t xyz[3])
{
  ASSERT(xyz && code < ((1ul << 63)-1));
  xyz[0] = (uint32_t)morton3D_decode_u21(code >> 2);
  xyz[1] = (uint32_t)morton3D_decode_u21(code >> 1);
  xyz[2] = (uint32_t)morton3D_decode_u21(code >> 0);
}

/* Count the number of bits set to 1 */
static FINLINE int
popcount(const uint8_t x)
{
  int n = x - ((x >> 1) & 0x55);
  n = (n & 0x33) + ((n >> 2) & 0x33);
  n = (n + (n >> 4)) & 0x0f;
  return (n * 0x0101) >> 8;
}

static FINLINE uint32_t
ftoui(const float f)
{
  union { uint32_t ui; float f; } ucast;
  ucast.f = f;
  return ucast.ui;
}

static FINLINE float
uitof(const uint32_t ui)
{
  union { uint32_t ui; float f; } ucast;
  ucast.ui = ui;
  return ucast.f;
}

#endif /* VXL_BE_C_H */

