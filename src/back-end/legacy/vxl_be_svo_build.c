/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"

#include "vxl_be.h"
#include "vxl_be_device_c.h"
#include "vxl_be_svo_c.h"
#include "vxl_be_svo_builder.h"
#include "vxl_be_triangle.h"
#include "vxl_be_voxel.h"

#include <rsys/clock_time.h>
#include <omp.h>

struct sub_svo {
  struct svo_node node; /* Root node */
  size_t nvxls; /* #voxels */
};

struct timer {
  struct time vxlzr;
  struct time build;
};

struct mshtri {
  size_t imesh; /* Mesh identifier to which the triangle belongs to */
  size_t itri; /* Triangle identifier */
};

/* Declare the list of mesh triangles */
#define DARRAY_NAME mshtri
#define DARRAY_DATA struct mshtri
#include <rsys/dynamic_array.h>

/* Declare the list of a set of mesh triangles */
#define DARRAY_NAME mshtris_list
#define DARRAY_DATA struct darray_mshtri
#define DARRAY_FUNCTOR_INIT darray_mshtri_init
#define DARRAY_FUNCTOR_COPY darray_mshtri_copy
#define DARRAY_FUNCTOR_RELEASE darray_mshtri_release
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_mshtri_copy_and_release
#include <rsys/dynamic_array.h>

struct voxelizer {
  /* Per thread data structures */
  struct darray_voxaccs_list voxaccs_lists;
  struct darray_voxaccs_htbl voxaccs_htbls;
  struct darray_voxels_list scratches; /* Use to sort voxels */
};

#define DARRAY_NAME sub_svo
#define DARRAY_DATA struct sub_svo
#include <rsys/dynamic_array.h>

#define DARRAY_NAME timer
#define DARRAY_DATA struct timer
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
voxelizer_release(struct voxelizer* vxlzr)
{
  ASSERT(vxlzr);
  darray_voxaccs_list_release(&vxlzr->voxaccs_lists);
  darray_voxaccs_htbl_release(&vxlzr->voxaccs_htbls);
  darray_voxels_list_release(&vxlzr->scratches);
}

static res_T
voxelizer_init(struct vxl_be_device* dev, struct voxelizer* vxlzr)
{
  res_T res = RES_OK;
  ASSERT(dev && vxlzr);

  darray_voxaccs_list_init(dev->allocator, &vxlzr->voxaccs_lists);
  darray_voxaccs_htbl_init(dev->allocator, &vxlzr->voxaccs_htbls);
  darray_voxels_list_init(dev->allocator, &vxlzr->scratches);

  res = darray_voxaccs_list_resize(&vxlzr->voxaccs_lists, dev->nthreads);
  if(res != RES_OK) goto error;
  res = darray_voxaccs_htbl_resize(&vxlzr->voxaccs_htbls, dev->nthreads);
  if(res != RES_OK) goto error;
  res = darray_voxels_list_resize(&vxlzr->scratches, dev->nthreads);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  voxelizer_release(vxlzr);
  goto exit;
}

static FINLINE enum svo_buffer_type
vxl_mem_location_to_svo_buffer_type(const enum vxl_mem_location memloc)
{
  enum svo_buffer_type type;
  switch(memloc) {
    case VXL_MEM_INCORE: type = SVO_BUFFER_INCORE; break;
    case VXL_MEM_OOCORE: type = SVO_BUFFER_OOCORE; break;
    default: FATAL("Unreachable code\n"); break;
  }
  return type;
}

static void
estimate_user_timers
  (const struct time* elapsed_time, /* Elapsed time from the user pov */
   const struct darray_timer* thread_timers, /* Per thread timers */
   struct timer* timer) /* Timers from the user pov */
{
  struct time time;
  double vxlzr_normalized, build_normalized;
  int64_t overall, vxlzr, build;
  size_t ithread;
  ASSERT(elapsed_time && thread_timers && timer);

  /* Accumulate the per thread timers */
  time_zero(&timer->vxlzr);
  time_zero(&timer->build);
  FOR_EACH(ithread, 0, darray_timer_size_get(thread_timers)) {
    const struct timer* t = darray_timer_cdata_get(thread_timers)+ithread;
    time_add(&timer->vxlzr, &timer->vxlzr, &t->vxlzr);
    time_add(&timer->build, &timer->build, &t->build);
  }

  /* Normalized the accumulated timers */
  time_add(&time, &timer->vxlzr, &timer->build);
  overall = time_val(&time, TIME_NSEC);
  vxlzr = time_val(&timer->vxlzr, TIME_NSEC);
  build = time_val(&timer->build, TIME_NSEC);
  vxlzr_normalized = (double)vxlzr / (double)overall;
  build_normalized = (double)build / (double)overall;

  /* Estimate the user timers */
  overall = time_val(elapsed_time, TIME_NSEC);
  vxlzr = (int64_t)((double)overall * vxlzr_normalized);
  build = (int64_t)((double)overall * build_normalized);
  timer->vxlzr.sec = vxlzr / 1000000000l;
  timer->build.sec = build / 1000000000l;
  timer->vxlzr.nsec = vxlzr - timer->vxlzr.sec * 1000000000l;
  timer->build.nsec = build - timer->build.sec * 1000000000l;
}

static res_T
project_mesh_triangle_to_partitions
  (const struct mshtri* mshtri,
   const float origin[3], /* World space origin of the voxel space */
   const uint32_t clamp_lower[3],
   const uint32_t clamp_upper[3],
   const float vxl_sz, /* World space size of a voxel */
   const size_t part_def, /* Definition of a partition along the 3 axis */
   const struct vxl_mesh_desc* meshes,
   struct darray_mshtri parts[]) /* List of morton ordered parts */
{
  float vertices[3][3];
  uint64_t indices[3];
  uint64_t mcode[3]; /* Morton code cache */
  uint32_t ipart[3];
  uint32_t ipart_low[3]; /* Inclusive Lower bound id of the intersected parts */
  uint32_t ipart_upp[3]; /* Exclusive upper bound id if the intersected parts */
  float lower[3]; /* Triangle lower bound */
  float upper[3]; /* Triangle upper bound */
  uint32_t vlower[3]; /* Triangle lower bound in voxel space */
  uint32_t vupper[3]; /* Triangle upper bound in voxel space */
  size_t imesh;
  size_t itri;
  res_T res = RES_OK;
  ASSERT(mshtri && origin && vxl_sz && IS_POW2(part_def) && meshes && parts);
  ASSERT(clamp_lower && clamp_upper);
  ASSERT(clamp_lower[0] < clamp_upper[0]);
  ASSERT(clamp_lower[1] < clamp_upper[1]);
  ASSERT(clamp_lower[2] < clamp_upper[2]);

  imesh = mshtri->imesh;
  itri = mshtri->itri;

  /* Fetch the triangle vertices */
  meshes[imesh].get_ids(itri, indices, meshes[imesh].data);
  meshes[imesh].get_pos(indices[0], vertices[0], meshes[imesh].data);
  meshes[imesh].get_pos(indices[1], vertices[1], meshes[imesh].data);
  meshes[imesh].get_pos(indices[2], vertices[2], meshes[imesh].data);

  /* Compute the world space triangle AABB */
  lower[0] = MMIN(MMIN(vertices[0][0], vertices[1][0]), vertices[2][0]);
  lower[1] = MMIN(MMIN(vertices[0][1], vertices[1][1]), vertices[2][1]);
  lower[2] = MMIN(MMIN(vertices[0][2], vertices[1][2]), vertices[2][2]);
  upper[0] = MMAX(MMAX(vertices[0][0], vertices[1][0]), vertices[2][0]);
  upper[1] = MMAX(MMAX(vertices[0][1], vertices[1][1]), vertices[2][1]);
  upper[2] = MMAX(MMAX(vertices[0][2], vertices[1][2]), vertices[2][2]);
  ASSERT(lower[0]>=origin[0] && lower[1]>=origin[1] && lower[2]>=origin[2]);
  ASSERT(upper[0]>=origin[0] && upper[1]>=origin[1] && upper[2]>=origin[2]);

  /* Transform the triangle AABB to voxel space */
  vlower[0] = (uint32_t)((lower[0]-origin[0])/vxl_sz);
  vlower[1] = (uint32_t)((lower[1]-origin[1])/vxl_sz);
  vlower[2] = (uint32_t)((lower[2]-origin[2])/vxl_sz);
  vupper[0] = (uint32_t)snap_float(upper[0]-origin[0], vxl_sz, SNAP_CEIL);
  vupper[1] = (uint32_t)snap_float(upper[1]-origin[1], vxl_sz, SNAP_CEIL);
  vupper[2] = (uint32_t)snap_float(upper[2]-origin[2], vxl_sz, SNAP_CEIL);

  /* Is triangle clipped by clamp domain */
  if(vlower[0] >= clamp_upper[0]
  || vlower[1] >= clamp_upper[1]
  || vlower[2] >= clamp_upper[2]
  || vupper[0] <= clamp_lower[0]
  || vupper[1] <= clamp_lower[1]
  || vupper[2] <= clamp_lower[2])
    goto exit;

  /* Shrink the triangle AABB to the voxelization domain */
  vlower[0] = MMAX(vlower[0], clamp_lower[0]);
  vlower[1] = MMAX(vlower[1], clamp_lower[1]);
  vlower[2] = MMAX(vlower[2], clamp_lower[2]);
  vupper[0] = MMIN(vupper[0], clamp_upper[0]);
  vupper[1] = MMIN(vupper[1], clamp_upper[1]);
  vupper[2] = MMIN(vupper[2], clamp_upper[2]);

  /* Compute the intersected lower and upper bounds of the partition
   * intersected by the "clamped" triangle. */
  ipart_low[0] = (uint32_t)((vlower[0]-clamp_lower[0]) / part_def);
  ipart_low[1] = (uint32_t)((vlower[1]-clamp_lower[1]) / part_def);
  ipart_low[2] = (uint32_t)((vlower[2]-clamp_lower[2]) / part_def);
  ipart_upp[0] = (uint32_t)((vupper[0]-clamp_lower[0]+(part_def-1)) / part_def);
  ipart_upp[1] = (uint32_t)((vupper[1]-clamp_lower[1]+(part_def-1)) / part_def);
  ipart_upp[2] = (uint32_t)((vupper[2]-clamp_lower[2]+(part_def-1)) / part_def);

  /* Register the triangle into the intersected partitions */
  FOR_EACH(ipart[0], ipart_low[0], ipart_upp[0]) {
    mcode[0] = morton3D_encode_u21(ipart[0]) << 2;

    FOR_EACH(ipart[1], ipart_low[1], ipart_upp[1]) {
      mcode[1] = (morton3D_encode_u21(ipart[1]) << 1) | mcode[0];

      FOR_EACH(ipart[2], ipart_low[2], ipart_upp[2]) {
        mcode[2] = (morton3D_encode_u21(ipart[2]) << 0) | mcode[1];

        res = darray_mshtri_push_back(&parts[mcode[2]], mshtri);
        if(res != RES_OK) goto error;
      }
    }
  }
exit:
  return res;
error:
  goto exit;
}

static res_T
partition_voxelize
  (struct voxelizer* vxlzr,
   struct darray_voxel* voxels, /* List where emitted voxels are stored */
   const struct darray_mshtri* part, /* Partition to voxelize */
   const uint32_t part_lower[3], /* Voxel space lower bound of the partition */
   const uint32_t part_upper[3], /* Voxel space upper bound of the partition */
   const size_t part_def, /* Definition of the partition along the 3 axis */
   const float origin[3], /* World space origin of the voxels */
   const uint32_t definition[3], /* Definition of the set of meshes */
   const float vxl_sz, /* World space size of a voxel */
   const struct vxl_mesh_desc* meshes)
{
  struct darray_voxacc* voxaccs;
  struct htable_voxacc* mcode2voxacc;
  struct darray_voxel* scratch;
  size_t part_ntris; /* # triangles into a partition */
  size_t imshtri;
  unsigned ithread = (unsigned)omp_get_thread_num();
  res_T res = RES_OK;
  ASSERT(vxlzr && voxels && part && part_lower && part_upper && IS_POW2(part_def));
  ASSERT(origin && definition && meshes);
  ASSERT(part_lower[0] < part_upper[0]);
  ASSERT(part_lower[1] < part_upper[1]);
  ASSERT(part_lower[2] < part_upper[2]);

  part_ntris = darray_mshtri_size_get(part);
  if(!part_ntris) goto exit;

  /* Fetch and clear the per thread data */
  voxaccs = darray_voxaccs_list_data_get(&vxlzr->voxaccs_lists) + ithread;
  mcode2voxacc = darray_voxaccs_htbl_data_get(&vxlzr->voxaccs_htbls) + ithread;
  scratch = darray_voxels_list_data_get(&vxlzr->scratches) + ithread;
  darray_voxacc_clear(voxaccs);
  htable_voxacc_clear(mcode2voxacc);

  /* Voxelize the triangles of the partition */
  FOR_EACH(imshtri, 0, part_ntris) {
    const struct mshtri* mshtri = darray_mshtri_cdata_get(part)+imshtri;
    float vertices[3][3];
    uint64_t indices[3];
    struct triangle tri;
    const size_t itri = mshtri->itri;
    const size_t imesh = mshtri->imesh;

    /* Fetch the triangle vertices */
    meshes[imesh].get_ids(itri, indices, meshes[imesh].data);
    meshes[imesh].get_pos(indices[0], vertices[0], meshes[imesh].data);
    meshes[imesh].get_pos(indices[1], vertices[1], meshes[imesh].data);
    meshes[imesh].get_pos(indices[2], vertices[2], meshes[imesh].data);

    /* Setup the triangle context */
    triangle_setup(&tri, imesh, origin, definition, vxl_sz, vertices);
    if(!tri.is_valid) continue;

    /* Voxelize the triangle */
    res = triangle_voxelize(&tri, voxaccs, mcode2voxacc, part_lower, part_upper,
      vxl_sz, itri, meshes+imesh, origin);
    if(res != RES_OK) goto error;
  }

  /* Flush the accumulated voxel attribs into a morton ordered list */
  res = flush_accumulated_voxels(darray_voxacc_cdata_get(voxaccs),
    darray_voxacc_size_get(voxaccs), part_def, scratch, voxels);
  if(res != RES_OK) goto error;

exit:
  return (res_T)res;
error:
  goto exit;
}

/* Build the SVO for a range of partitions */
static res_T
svo_build_range
  (struct vxl_be_svo* svo,
   struct voxelizer* vxlzr,
   struct darray_mshtris_list* mshtris_parts, /* Per partition list of triangles */
   struct darray_voxels_list* voxels_parts, /* Per thread list of partition voxels */
   const size_t part_begin,
   const size_t part_end,
   const size_t part_def,
   const size_t svo_definition, /* Definition of the SVO to build */
   const uint32_t definition[3], /* Definition of the set of meshes */
   const size_t ibuffer,
   const float vxl_sz,
   const struct vxl_mesh_desc* meshes,
   struct timer* timer,
   struct sub_svo* sub_svo)
{
  struct svo_builder bldr;
  struct time t0, t1;
  uint64_t mcode;
  size_t ipart;
  const unsigned ithread = (unsigned)omp_get_thread_num();
  res_T res = RES_OK;
  ASSERT(svo && vxlzr && mshtris_parts && voxels_parts);
  ASSERT(part_begin < part_end && definition && meshes);
  ASSERT(IS_POW2(part_def) && IS_POW2(svo_definition));
  ASSERT(ibuffer < darray_buffer_size_get(&svo->buffers));
  ASSERT(timer && sub_svo);

  /* 1st morton code of the SVO */
  mcode = part_begin * part_def * part_def * part_def;

  svo_builder_init(&bldr, svo, ibuffer, svo_definition, mcode);

  sub_svo->nvxls = 0;
  FOR_EACH(ipart, part_begin, part_end) {
    uint32_t part_coords[3]; /* Voxel space coordinates of the partition */
    uint32_t part_lower[3], part_upper[3]; /* Voxel space partition AABB */
    struct darray_voxel* vxls;
    struct darray_mshtri* part; /* List of triangles of the partition */
    size_t part_ntris; /* # triangles into a partition */
    size_t ivxl;
    size_t nvxls;

    part = darray_mshtris_list_data_get(mshtris_parts) + ipart;
    part_ntris = darray_mshtri_size_get(part);
    if(!part_ntris) continue;

    morton_xyz_decode_u21(ipart, part_coords);

    /* Fetch and clear the per thread pool of voxels */
    vxls = darray_voxels_list_data_get(voxels_parts) + ithread;
    darray_voxel_clear(vxls);

    /* Define the voxel space AABB of the partition*/
    part_lower[0] = (uint32_t)(part_coords[0] * part_def);
    part_lower[1] = (uint32_t)(part_coords[1] * part_def);
    part_lower[2] = (uint32_t)(part_coords[2] * part_def);
    part_upper[0] = MMIN(part_lower[0] + (uint32_t)part_def, definition[0]);
    part_upper[1] = MMIN(part_lower[1] + (uint32_t)part_def, definition[1]);
    part_upper[2] = MMIN(part_lower[2] + (uint32_t)part_def, definition[2]);

    /* Voxelize the partition */
    time_current(&t0);
    res = partition_voxelize(vxlzr, vxls, part, part_lower, part_upper,
      part_def, svo->lower, definition, vxl_sz, meshes);
    if(res != RES_OK) goto error;
    time_current(&t1),
    time_add(&timer->vxlzr, time_sub(&t0, &t1, &t0), &timer->vxlzr);

    /* Add the emitted voxels to the SVO */
    nvxls = darray_voxel_size_get(vxls);
    time_current(&t0);
    FOR_EACH(ivxl, 0, nvxls) {
      res = svo_builder_add_voxel(&bldr, darray_voxel_cdata_get(vxls)+ivxl);
      if(res != RES_OK) goto error;
    }
    time_current(&t1);
    time_add(&timer->build, time_sub(&t0, &t1, &t0), &timer->build);
    sub_svo->nvxls += nvxls;

    /* Purge the partition */
    darray_mshtri_purge(part);
  }

  time_current(&t0);

  res = svo_builder_finalize(&bldr, 0, NULL, &sub_svo->node);
  if(res != RES_OK) goto error;

  time_current(&t1);
  time_add(&timer->build, time_sub(&t0, &t1, &t0), &timer->build);

exit:
  svo_builder_release(&bldr);
  return res;
error:
  goto exit;
}

/* Both voxelization and SVO build are parallelized. This routine should be
 * invoked on SVO whose number of partitions is greater or equal to the number
 * of threads. */
static res_T
svo_build
  (struct vxl_be_svo* svo,
   const size_t part_def, /* Partition definition along the 3 axis */
   const size_t nparts, /* Number of partitions */
   const uint32_t definition[3], /* Definition of the set of meshes */
   const float vxl_sz, /* World space size of a voxel */
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const enum vxl_mem_location memloc,
   struct timer* gtimer)
{
  struct hl_svo_builder bldr;
  struct darray_mshtris_list mshtris_lists;
  struct darray_voxels_list voxels_lists;
  struct darray_sub_svo sub_svos;
  struct darray_timer timers;
  struct time t0, t1;
  struct voxelizer vxlzr;
  uint64_t itri;
  size_t imesh;
  size_t ntris;
  size_t hl_svo_def;
  size_t sub_svo_def;
  size_t isub_svo;
  size_t nsub_svos;
  size_t nsub_svo_parts;
  size_t ithread;
  size_t ibuf;
  enum svo_buffer_type buf_type;
  int ibase_lvl;
  int bldr_is_init = 0;
  ATOMIC res = RES_OK;
  ASSERT(svo && vxl_sz && part_def && nparts && vxl_sz);
  ASSERT(definition && meshes && nmeshes && gtimer);

  darray_mshtris_list_init(svo->dev->allocator, &mshtris_lists);
  darray_voxels_list_init(svo->dev->allocator, &voxels_lists);
  darray_sub_svo_init(svo->dev->allocator, &sub_svos);
  darray_timer_init(svo->dev->allocator, &timers);
  res = voxelizer_init(svo->dev, &vxlzr);
  if(res != RES_OK) goto error;

  res = darray_timer_resize(&timers, svo->dev->nthreads);
  if(res != RES_OK) goto error;

  FOR_EACH(ithread, 0, svo->dev->nthreads) {
    time_zero(&darray_timer_data_get(&timers)[ithread].vxlzr);
    time_zero(&darray_timer_data_get(&timers)[ithread].build);
  }
  time_zero(&gtimer->vxlzr);
  time_zero(&gtimer->build);

  /* Reserve memory space for the partitions */
  res = darray_mshtris_list_resize(&mshtris_lists, nparts);
  if(res != RES_OK) goto error;

  /* Note that there is a list of voxels per thread and not per partition since
   * once voxelize, a partition is directly registered into the SVO. */
  res = darray_voxels_list_resize(&voxels_lists, svo->dev->nthreads);
  if(res != RES_OK) goto error;

  /* Project the triangles into the partitions that they intersect */
  ntris = 0;
  FOR_EACH(imesh, 0, nmeshes) {
    FOR_EACH(itri, 0, meshes[imesh].ntris) {
      const uint32_t zero[3] = {0, 0, 0};
      struct mshtri mshtri;
      mshtri.itri = itri;
      mshtri.imesh = imesh;
      res = project_mesh_triangle_to_partitions(&mshtri, svo->lower, zero,
        definition, vxl_sz, part_def, meshes,
        darray_mshtris_list_data_get(&mshtris_lists));
      if(res != RES_OK) goto error;
    }
    ntris += meshes[imesh].ntris;
  }
  if(!ntris) goto exit; /* Nothing to voxelize */

  /* Define the high level SVO definition (i.e. #sub-SVOs) */
  hl_svo_def = svo->definition/part_def;
  sub_svo_def = svo->definition / hl_svo_def;
  nsub_svos = hl_svo_def*hl_svo_def*hl_svo_def;
  ASSERT(IS_POW2(sub_svo_def));

  /* Compute the SVO level from which the high-level SVO is built */
  ibase_lvl = log2i((int)sub_svo_def) - 1;

  /* Allocate the sub_svo */
  res = darray_sub_svo_resize(&sub_svos, nsub_svos);
  if(res != RES_OK) goto error;

  /* Allocate one SVO buffer per thread + one buffer for the high level SVO */
  res = darray_buffer_resize(&svo->buffers, svo->dev->nthreads + 1);
  if(res != RES_OK) goto error;

  /* Initialize the SVO buffers */
  buf_type = vxl_mem_location_to_svo_buffer_type(memloc);
  FOR_EACH(ibuf, 0, darray_buffer_size_get(&svo->buffers)) {
    struct svo_buffer* buf = darray_buffer_data_get(&svo->buffers)+ibuf;
    res = svo_buffer_setup(buf, buf_type, (uint16_t)ibuf, svo->dev->pagesize);
    if(res != RES_OK) goto error;
  }

  /* Compute the number of partitions per sub svo */
  nsub_svo_parts = sub_svo_def / part_def;
  nsub_svo_parts = nsub_svo_parts*nsub_svo_parts*nsub_svo_parts;

  /* Launch sub SVO build */
  time_current(&t0);
  omp_set_num_threads((int)svo->dev->nthreads);
  #pragma omp parallel for schedule(dynamic, 1)
  for(isub_svo=0; isub_svo < nsub_svos; ++isub_svo) {
    const size_t part_begin = isub_svo * nsub_svo_parts;
    const size_t part_end = part_begin + nsub_svo_parts;
    struct sub_svo* sub_svo = darray_sub_svo_data_get(&sub_svos) + isub_svo;
    struct timer* timer = darray_timer_data_get(&timers)+omp_get_thread_num();
    res_T res_local = RES_OK;

    if(ATOMIC_GET(&res) != RES_OK) continue;

    ibuf = (size_t)omp_get_thread_num();

    res_local = svo_build_range(svo, &vxlzr, &mshtris_lists, &voxels_lists,
      part_begin, part_end, part_def, sub_svo_def, definition, ibuf, vxl_sz,
      meshes, timer, sub_svo);
    if(res_local != RES_OK) {
      ATOMIC_SET(&res, res_local);
      continue;
    }
  }
  if(ATOMIC_GET(&res) != RES_OK) goto error;

  /* Finalize the per thread buffer */
  FOR_EACH(ithread, 0, svo->dev->nthreads) {
    res = svo_buffer_finalize(darray_buffer_data_get(&svo->buffers)+ithread);
    if(res != RES_OK) goto error;
  }

  time_sub(&t0, time_current(&t1), &t0);
  estimate_user_timers(&t0, &timers, gtimer);

  /* Define the buffer index of the High Level SVO */
  ibuf = svo->dev->nthreads;
  hl_svo_builder_init(&bldr, svo, ibuf, hl_svo_def, ibase_lvl);
  bldr_is_init = 1;

  /* Build the high level SVO */
  time_current(&t0);
  svo->nvoxels = 0;
  FOR_EACH(isub_svo, 0, nsub_svos) {
    struct sub_svo* sub_svo = darray_sub_svo_data_get(&sub_svos) + isub_svo;

    if(!sub_svo->nvxls) continue;
    svo->nvoxels += sub_svo->nvxls;

    res = hl_svo_builder_add_node(&bldr, &sub_svo->node, isub_svo);
    if(res != RES_OK)  goto error;
  }
  res = hl_svo_builder_finalize(&bldr);
  if(res != RES_OK) goto error;

  res = svo_buffer_finalize(bldr.buffer);
  if(res != RES_OK) goto error;

  time_current(&t1);
  time_add(&gtimer->build, time_sub(&t0, &t1, &t0), &gtimer->build);

exit:
  if(bldr_is_init) hl_svo_builder_release(&bldr);
  darray_mshtris_list_release(&mshtris_lists);
  darray_voxels_list_release(&voxels_lists);
  darray_sub_svo_release(&sub_svos);
  darray_timer_release(&timers);
  voxelizer_release(&vxlzr);
  return (res_T)res;
error:
  goto exit;
}

/* Parallelize the voxelization for the whole grid and sequentially build the
 * SVO from the emitted voxels. This routine should be used on SVO whose
 * definition will not take benefit of a parallel build, i.e. "small" SVOs */
static res_T
svo_build2
  (struct vxl_be_svo* svo,
   const uint32_t definition[3], /* Definition of the set of meshes */
   const float vxl_sz,
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const enum vxl_mem_location memloc,
   struct timer* timer)
{
  struct time t0, t1;
  struct voxelizer vxlzr;
  struct darray_mshtris_list mshtris_lists;
  struct darray_voxels_list voxels_lists;
  static const size_t part_def = 32;
  struct svo_builder bldr;
  size_t imesh;
  size_t ipart;
  size_t nparts;
  size_t ntris;
  enum svo_buffer_type buf_type;
  ATOMIC res = RES_OK;
  ASSERT(svo && definition && meshes && nmeshes && timer);
  ASSERT(svo->definition >= definition[0]);
  ASSERT(svo->definition >= definition[1]);
  ASSERT(svo->definition >= definition[2]);

  /* Allocate the SVO buffer */
  res = darray_buffer_resize(&svo->buffers, 1);
  if(res != RES_OK) return (res_T)res;

  darray_mshtris_list_init(svo->dev->allocator, &mshtris_lists);
  darray_voxels_list_init(svo->dev->allocator, &voxels_lists);
  svo_builder_init(&bldr, svo, 0, svo->definition, 0);
  res = voxelizer_init(svo->dev, &vxlzr);
  if(res != RES_OK) goto error;

  buf_type = vxl_mem_location_to_svo_buffer_type(memloc);
  res = svo_buffer_setup(darray_buffer_data_get(&svo->buffers), buf_type, 0,
    svo->dev->pagesize);
  if(res != RES_OK) goto error;

  /* Compute the overall #partitions that allows morton indexing */
  nparts = (size_t)((svo->definition + (part_def+1)/*ceil*/)/part_def);
  nparts = round_up_pow2(nparts);
  nparts = nparts*nparts*nparts;

  /* Reserve memory space for the partitions */
  res = darray_mshtris_list_resize(&mshtris_lists, nparts);
  if(res != RES_OK) goto error;
  res = darray_voxels_list_resize(&voxels_lists, nparts);
  if(res != RES_OK) goto error;

  /* Project the triangle into the partitions */
  ntris = 0;
  FOR_EACH(imesh, 0, nmeshes) {
    size_t itri;
    FOR_EACH(itri, 0, meshes[imesh].ntris) {
      struct mshtri mshtri;
      const uint32_t zero[3] = {0, 0, 0};
      mshtri.itri = itri;
      mshtri.imesh = imesh;
      res = project_mesh_triangle_to_partitions(&mshtri, svo->lower,
        zero, definition,  vxl_sz, part_def, meshes,
        darray_mshtris_list_data_get(&mshtris_lists));
      if(res != RES_OK) goto error;
    }
    ntris += meshes[imesh].ntris;
  }
  if(!ntris) goto exit; /* Nothing to voxelize */

  /* Parallel triangle voxelization */
  time_current(&t0);
  omp_set_num_threads((int)svo->dev->nthreads);
  #pragma omp parallel for schedule(dynamic, 1)
  for(ipart=0; ipart < nparts; ++ipart) {
    uint32_t part_lower[3], part_upper[3];
    uint32_t part_coords[3];
    struct darray_mshtri* tris;
    struct darray_voxel* vxls;
    size_t part_ntris;
    res_T res_local = RES_OK;

    if(ATOMIC_GET(&res) != RES_OK) continue;

    tris = darray_mshtris_list_data_get(&mshtris_lists) + ipart;
    vxls = darray_voxels_list_data_get(&voxels_lists) + ipart;

    part_ntris = darray_mshtri_size_get(tris);
    if(!part_ntris) continue;

    morton_xyz_decode_u21(ipart, part_coords);

    /* Define the voxel space AABB of the partition */
    part_lower[0] = (uint32_t)(part_coords[0] * part_def);
    part_lower[1] = (uint32_t)(part_coords[1] * part_def);
    part_lower[2] = (uint32_t)(part_coords[2] * part_def);
    part_upper[0] = MMIN(part_lower[0] + (uint32_t)part_def, definition[0]);
    part_upper[1] = MMIN(part_lower[1] + (uint32_t)part_def, definition[1]);
    part_upper[2] = MMIN(part_lower[2] + (uint32_t)part_def, definition[2]);

    /* Voxelize the partition */
    res_local = partition_voxelize(&vxlzr, vxls, tris, part_lower, part_upper,
      part_def, svo->lower, definition, vxl_sz, meshes);
    if(res_local != RES_OK) {
      ATOMIC_SET(&res, res_local);
      continue;
    }
  }
  if(ATOMIC_GET(&res) != RES_OK) goto error;
  time_current(&t1);
  time_sub(&timer->vxlzr, &t1, &t0);

  /* Register the emitted voxels into the SVO */
  time_current(&t0);
  svo->nvoxels = 0;
  FOR_EACH(ipart, 0, nparts) {
    const struct darray_voxel* vxls;
    size_t ivxl;
    size_t nvxls;

    vxls = darray_voxels_list_cdata_get(&voxels_lists) + ipart;
    nvxls = darray_voxel_size_get(vxls);
    if(!nvxls) continue;

    FOR_EACH(ivxl, 0, nvxls) {
      res = svo_builder_add_voxel(&bldr, darray_voxel_cdata_get(vxls)+ivxl);
      if(res != RES_OK) goto error;
    }
    svo->nvoxels += nvxls;
  }
  res = svo_builder_finalize(&bldr, 1, &svo->root, NULL);
  if(res != RES_OK) goto error;

  res = svo_buffer_finalize(bldr.buffer);
  if(res != RES_OK) goto error;

  time_current(&t1);
  time_sub(&timer->build, &t1, &t0);

exit:
  darray_mshtris_list_release(&mshtris_lists);
  darray_voxels_list_release(&voxels_lists);
  svo_builder_release(&bldr);
  voxelizer_release(&vxlzr);
  return (res_T)res;
error:
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
vxl_be_svo_build
  (struct vxl_be_svo* svo,
   const size_t definition,
   const struct vxl_mesh_desc* meshes,
   const size_t nmeshes,
   const enum vxl_mem_location memloc,
   struct time* vxlzr_timer, /* May be NULL */
   struct time* build_timer) /* May be NULL */
{
  struct timer gtimer; /* Global timer */
  size_t part_def = 128; /* Definition of a partition along the 3 axis */
  size_t imesh;
  size_t nparts;
  size_t ntris;
  float lower[3], upper[3]; /* World space scene AABB */
  float sz[3]; /* Size of the scene AABB */
  float max_sz; /* Max size of the scene AABB */
  float vxl_sz; /* World space size of a voxel along the 3 axis */
  uint32_t def_adjusted[3]; /* Adjusted definition */
  res_T res = RES_OK;
  ASSERT(IS_POW2(part_def));

  if(!svo || !definition || !meshes || !nmeshes
  || (memloc != VXL_MEM_INCORE && memloc != VXL_MEM_OOCORE)) {
    return RES_BAD_ARG;
  }

  /* Compute the overall number of triangles */
  ntris = 0;
  FOR_EACH(imesh, 0, nmeshes) {
    if(!mesh_desc_check(meshes+imesh)) {
      res = RES_BAD_ARG;
      goto error;
    }
    ntris += meshes[imesh].ntris;
  }
  if(!ntris) goto exit; /* Nothing to voxelize */

  /* Compute the mesh AABB */
  res = mesh_desc_compute_aabb(meshes, nmeshes, lower, upper);
  if(res != RES_OK) goto error;
  if(lower[0]>upper[0] || lower[1]>upper[1] || lower[2]>upper[2]) goto error;

  /* Compute the voxel size with respect to the submitted definition. */
  f3_sub(sz, upper, lower);
  f3_mulf(sz, sz, MESHES_AABB_SCALE); /* Handle numerical imprecision */
  max_sz = MMAX(MMAX(sz[0], sz[1]), sz[2]);
  vxl_sz = max_sz / (float)definition;

  /* Compute the definition of the set of meshes */
  #define SNAP (uint32_t)snap_float
  def_adjusted[0] = (uint32_t)definition;
  def_adjusted[1] = (uint32_t)definition;
  def_adjusted[2] = (uint32_t)definition;
  if(sz[0] != max_sz) def_adjusted[0] = SNAP(sz[0], vxl_sz, SNAP_CEIL);
  if(sz[1] != max_sz) def_adjusted[1] = SNAP(sz[1], vxl_sz, SNAP_CEIL);
  if(sz[2] != max_sz) def_adjusted[2] = SNAP(sz[2], vxl_sz, SNAP_CEIL);
  #undef SNAP

  /* Setup the SVO definition  */
  svo->definition = (uint16_t)MMAX(def_adjusted[0], 2);
  svo->definition = (uint16_t)MMAX(def_adjusted[1], svo->definition);
  svo->definition = (uint16_t)MMAX(def_adjusted[2], svo->definition);
  svo->definition = (uint16_t)round_up_pow2(svo->definition);

  /* Shrink the partition definition to the SVO definition */
  part_def = MMIN(svo->definition, part_def);

  /* Setup the SVO AABB */
  f3_set(svo->lower, lower);
  f3_addf(svo->upper, svo->lower, svo->definition * vxl_sz);

  /* Compute the overall #partitions that allows morton indexing */
  nparts = (size_t)((definition + (part_def - 1)/*ceil*/)/part_def);
  nparts = round_up_pow2(nparts);
  nparts = nparts*nparts*nparts;

  if(nparts >= svo->dev->nthreads) {
    res = svo_build(svo, part_def, nparts, def_adjusted, vxl_sz, meshes,
      nmeshes, memloc, &gtimer);
  } else {
    /* If the number of partitions is less than the number of threads;
     * parallelize the voxelization but do not parallelize the build step */
    res = svo_build2
      (svo, def_adjusted, vxl_sz, meshes, nmeshes, memloc, &gtimer);
  }
  if(res != RES_OK) goto error;

exit:
  if(vxlzr_timer) *vxlzr_timer = gtimer.vxlzr;
  if(build_timer) *build_timer = gtimer.build;
  return res;
error:
  goto exit;
}

