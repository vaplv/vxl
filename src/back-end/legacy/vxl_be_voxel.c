/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_be_voxel.h"

#include <rsys/math.h>
#include <rsys/float3.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Morton sort the voxels. Note that the submitted voxels are the voxels of
 * *one* bin. Consequently their Morton codes differ only on the LSB in
 * [0..log2(bin_def^3)]. It is thus sufficient to sort wrt this subset of
 * bits */
static FINLINE void
radix_sort
  (const size_t bin_def,
   struct darray_voxel* voxels, /* Voxels to sort */
   /* Temp array. Should have the same size than `voxels' */
   struct darray_voxel* scratch)
{
  const uint64_t mask = ((bin_def*bin_def*bin_def)-1);
  uint64_t max = 0;
  struct voxel* a = darray_voxel_data_get(voxels);
  struct voxel* b = darray_voxel_data_get(scratch);
  size_t n = darray_voxel_size_get(voxels);
  int shift = 0;
  size_t i;
  ASSERT(IS_POW2(bin_def));
  ASSERT(n && n <= bin_def*bin_def*bin_def);
  ASSERT(n == darray_voxel_size_get(scratch));

  /* Find the maximum morton code to sort. Discard MSB that are constants for
   * all voxels.  */
  FOR_EACH(i, 0, n) {
    const uint64_t mcode = a[i].mcode & mask;
    ASSERT(i == 0 || (a[i].mcode & ~mask) == (a[i-1].mcode & ~mask));
    if(mcode > max) max = mcode;
  }

  do {
    int bucket[256] = {0};
    FOR_EACH(i, 0, n) { /* Find #collisions */
      ++bucket[(a[i].mcode >> shift) & 0xFF];
    }
    FOR_EACH(i, 1, 256) {
      bucket[i] += bucket[i-1]; /* Compute offsets */
    }
    for(i=n; i-- > 0; ) { /* Radix sort */
      b[--bucket[(a[i].mcode >> shift) & 0xFF]] = a[i];
    }

    shift += 8;
    SWAP(struct voxel*, a, b);

  } while(max >> shift);

#ifndef NDEBUG
  /* Check that the voxels are correctly Morton sorted */
  FOR_EACH(i, 1, n) {
    ASSERT(a[i].mcode >= a[i-1].mcode);
  }
#endif

  if(a != darray_voxel_data_get(voxels)) {
    darray_voxel_swap(voxels, scratch);
  }
}


/*******************************************************************************
 * Local functions
 ******************************************************************************/
/* Normalize and sort the voxel attributes of `accums' into `voxels' */
res_T
flush_accumulated_voxels
  (const struct voxacc* voxaccs, /* Per voxel accumulated attribs */
   const size_t nvxls, /* #voxels to flush */
   const size_t bin_def, /* Definition of a bin along the 3 axis */
   struct darray_voxel* scratch, /* Temp array to radix sort the voxels */
   struct darray_voxel* voxels) /* Output list of Morton-sorted voxels */
{
  size_t ivxl;
  res_T res = RES_OK;
  ASSERT((voxaccs || !nvxls) && scratch && voxels && scratch != voxels);

  if(!nvxls) return RES_OK;

  /* Reserve the space to store the normalized voxels */
  res = darray_voxel_resize(voxels, nvxls);
  if(res != RES_OK) return res;

  /* Reserve the temporary voxel array used by the radix "Morton" sort */
  res = darray_voxel_resize(scratch, nvxls);
  if(res != RES_OK) return res;

  /* Normalize the accumulated voxels into the pool of voxels */
  FOR_EACH(ivxl, 0, nvxls) {
    const struct voxacc* voxacc = voxaccs + ivxl;
    struct voxel* vxl = darray_voxel_data_get(voxels) + ivxl;
    vxl->mcode = voxacc->mcode;
    f3_splat(vxl->normal, 0);
    if(voxacc->naccums == 1) {
      vxl->col[0] = (unsigned char)(voxacc->col[0]);
      vxl->col[1] = (unsigned char)(voxacc->col[1]);
      vxl->col[2] = (unsigned char)(voxacc->col[2]);
      f3_set(vxl->normal, voxacc->normal);
    } else {
      vxl->col[0] = (unsigned char)(voxacc->col[0] / voxacc->naccums);
      vxl->col[1] = (unsigned char)(voxacc->col[1] / voxacc->naccums);
      vxl->col[2] = (unsigned char)(voxacc->col[2] / voxacc->naccums);
      f3_normalize(vxl->normal, voxacc->normal);
    }
  }

  /* Morton sort the normalized voxels */
  radix_sort(bin_def, voxels, scratch);
  return RES_OK;
}
