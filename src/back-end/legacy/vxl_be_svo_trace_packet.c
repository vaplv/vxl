/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl.h"
#include "vxl_be.h"
#include "vxl_be_c.h"
#include "vxl_be_device_c.h"
#include "vxl_be_ray.h"
#include "vxl_be_packet.h"
#include "vxl_be_svo_c.h"
#include "vxl_be_svo_trace.h"

#define MAX_PACKET_WIDTH 256 /* Maximum number of per packet rays */

/*
 * SVO node memory layout:
 *      +...+...+      #---#---#
 *      |`#--`#--`#    |`#--`#--`#
 *      + |`#--`#--`#  # | 3 | 7 |`+
 *      '`# | 2 | 6 |  |`#---#---# .
 *   Y  + |`#---#---#  # | 1 | 5 |`+
 * Z |   `# | 0 | 4 |   `#---#---# .
 *  \|     `#---#---#     `+..`+..`+
 *   o--X
 */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Compute the ibits to XOR^ed to the child node index to obtain the index of
 * the next child to traverse with respect to a front to back order. (1 <<
 * ibits[0]) must be XOR^ed after each traversal step. (1 << ibits[1]) should
 * be XOR^ed on the second, forth and sixth iterations while (2 << ibits[2])
 * should be only XOR^ed at the fourth step */
static FINLINE void
setup_traversal_ibits(int ibits[3], const float dir[3])
{
  int dir_ids[3] = {0, 1, 2};
  float dir_abs[3];
  int i;
  ASSERT(ibits && dir);

  /* Bubble sort the `dir' dimensions in ascending order of their absolute
   * value <=> breadth first traversal of node dimension */
  FOR_EACH(i, 0, 3) dir_abs[i] = absf(dir[i]);
  if(dir_abs[dir_ids[0]]>dir_abs[dir_ids[1]]) SWAP(int, dir_ids[0], dir_ids[1]);
  if(dir_abs[dir_ids[1]]>dir_abs[dir_ids[2]]) SWAP(int, dir_ids[1], dir_ids[2]);
  if(dir_abs[dir_ids[0]]>dir_abs[dir_ids[1]]) SWAP(int, dir_ids[0], dir_ids[1]);

  /* Compute the bit indices from the dir indices, with respect to the SVO node
   * memory layout */
  FOR_EACH(i, 0, 3) ibits[i] = 2 - dir_ids[i];
}

/* Approximate the packet footprint at a distance of 1 from the packet origin */
static FINLINE float
packet_footprint(const struct ia3* dir)
{
  float main_dir[3] = {0.f, 0.f, 0.f};
  float vec0[3] = {0.f, 0.f, 0.f};
  float vec1[3] = {0.f, 0.f, 0.f};
  float cos_dir;

  f3_normalize(main_dir, f3_add(main_dir, dir->lower, dir->upper));
  /* The interval of directions is not normalized */
  f3_normalize(vec0, dir->lower);
  f3_normalize(vec1, dir->upper);

  cos_dir = MMIN(f3_dot(main_dir, vec0), f3_dot(main_dir, vec1));
  if(cos_dir <= 0) { /* Highly incoherent rays */
    return FLT_MAX;
  } else if(cos_dir >= 1.f) {
    return 0.f;
  } else {
    const float sqr_hypotenuse = 1.f / (cos_dir*cos_dir);
    return (float)sqrt(sqr_hypotenuse - 1.0) /* Pythagore */ * 2.f;
  }
}

/* Define if a node is "tiny" with respect to `packet_footprint' which is the
 * footprint of the packet computed at a distance of 1 from the packet origin
 * along the main packet direction */
static FINLINE int
is_tiny_node
  (const float node_distance, /* Distance from the packet origin to the node */
   const struct aabb* node_aabb, /* Node bounds */
   const float packet_footprint)
{
  float node_size;
  ASSERT(node_aabb && packet_footprint >= 0.f);

  /* Assume that the node is a cube */
  node_size = node_aabb->upper[0] - node_aabb->lower[0];
  ASSERT(eq_epsf(node_size, node_aabb->upper[1] - node_aabb->lower[1], node_size*1.e-4f));
  ASSERT(eq_epsf(node_size, node_aabb->upper[2] - node_aabb->lower[2], node_size*1.e-4f));

  /* Empirically define a tiny node as a node smaller than 3/4 of the packet
   * footprint */
  return node_size < (packet_footprint * 0.75f) * node_distance;
}

/* Smits intersection: "Efficiency issues for ray tracing" */
static FINLINE char
ray_intersect_svo_voxel
  (const struct ray* ray,
   const float aabb_lower_minus_ray_org[3], /* Precomputed value */
   const float aabb_upper_minus_ray_org[3], /* Precomputed value */
   float* hit_t,  /* Near parametric ray intersection values */
   enum vxl_face* iface)
{
  float t0, t1;
  int iaxis[2] = {-1, -1};
  int i;
  ASSERT(ray && ray->is_enabled);
  ASSERT(aabb_lower_minus_ray_org && aabb_upper_minus_ray_org);
  ASSERT(hit_t && iface);

  t0 = ray->range[0]; t1 = ray->range[1];
  FOR_EACH(i, 0, 3) {
    float t_min = aabb_lower_minus_ray_org[i] * ray->rcp_dir[i];
    float t_max = aabb_upper_minus_ray_org[i] * ray->rcp_dir[i];
    if(t_min > t_max) SWAP(float, t_min, t_max);
    if(t_min > t0) {
      iaxis[0] = i;
      t0 = t_min;
    }
    if(t_max < t1) {
      iaxis[1] = i;
      t1 = t_max;
    }
    if(t0 > t1) return 0;
  }

  /* Define the hit voxel face. Hit the lower or upper faces whether the ray
   * direction sign along the hit axis is positive or negative, respectively.
   * Note that the hit face must be reverted when the ray starts into the
   * voxel. Assume that the face enum is left, right, bottom, top, back and
   * front */
  if(iaxis[0] < 0) { /* Rays start into the voxel */
    ASSERT(iaxis[1] >= 0);
    *iface = iaxis[1] * 2;
    if(ray->dir[iaxis[1]] > 0.f) ++(*iface);
    if(hit_t) *hit_t = t1;
  } else {
    *iface = iaxis[0] * 2;
    if(ray->dir[iaxis[0]] < 0.f) ++(*iface);
    if(hit_t) *hit_t = t0;
  }
  return 1;
}

static FINLINE void
setup_hit
  (struct vxl_hit* hit,
   const float dst,
   const int ihit_face,
   const struct svo_node_attrib* attr)
{
  int i;
  ASSERT(hit && attr && ihit_face<6 && ihit_face>=0);
  ASSERT(dst >= 0.f);
  hit->distance = dst;
  hit->face = ihit_face;
  xnormal_get(&attr->normal, hit->normal);
  FOR_EACH(i, 0, 3) hit->color[i] = attr->color[i];
}

/* Return the number of active rays that hit the leaf */
static FINLINE size_t
trace_leaf
  (const struct aabb* aabb,
   const struct svo_node_attrib* attr,
   size_t* iray_1st_active, /* 1st active ray */
   struct ray* rays,
   const size_t nrays,
   struct packet* packet,
   struct vxl_hit* hits)
{
  size_t nhits = 0;
  size_t iray;
  float tmp0[3];
  float tmp1[3];
  float hit_t;
  enum vxl_face face;
  ASSERT(aabb && attr && rays && nrays && packet && hits);
  ASSERT(iray_1st_active && *iray_1st_active < nrays);

  /* Note that all rays shared the same origin. We can thus precompute some
   * values of the ray/AABB intersection test */
  f3_sub(tmp0, aabb->lower, rays[0].org);
  f3_sub(tmp1, aabb->upper, rays[0].org);

  iray = *iray_1st_active;
  if(ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &hit_t, &face)) { /* Early hit */
    rays[iray].is_enabled = 0;
    setup_hit(hits + rays[iray].iray, hit_t, face, attr);
    ++nhits;
    *iray_1st_active = SIZE_MAX;
    FOR_EACH(iray, iray + 1, nrays) {
      if(rays[iray].is_enabled) {
        if(ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &hit_t, &face)) {
          rays[iray].is_enabled = 0;
          setup_hit(hits + rays[iray].iray, hit_t, face, attr);
          ++nhits;
        } else if(*iray_1st_active == SIZE_MAX) {
          *iray_1st_active = iray;
        }
      }
    }
  } else if(packet_intersect_aabb(packet, aabb, NULL)) { /* !Early miss */
    FOR_EACH(iray, iray + 1, nrays) {
      if(rays[iray].is_enabled
      && ray_intersect_svo_voxel(rays + iray, tmp0, tmp1, &hit_t, &face)) {
        rays[iray].is_enabled = 0;
        setup_hit(hits + rays[iray].iray, hit_t, face, attr);
        ++nhits;
      }
    }
  }
  return nhits;
}

/* Single trace all rays in svo_rays in [*iray_1st_active, nrays). Return the
 * number of active rays that hit a voxel and update the iray_1s_active value */
static FINLINE size_t
trace_single_ray
  (struct vxl_be_svo* svo,
   const struct svo_index* inode, /* Index of the node to traverse */
   const float node_lower[3],
   const float node_size,
   const uint32_t lod_range[2],
   size_t* iray_1st_active, /* Index toward the 1st active ray in svo_rays */
   struct ray* rays,
   struct svo_ray* svo_rays,
   const size_t nrays,
   struct vxl_hit* hits)
{
  size_t iray;
  size_t nhits = 0;
  res_T res;
  (void)res;

  ASSERT(svo && node_lower && iray_1st_active && rays && svo_rays && hits);
  ASSERT(*iray_1st_active < nrays);
  ASSERT(lod_range && lod_range[0] <= lod_range[1]);

  iray = *iray_1st_active;
  *iray_1st_active = SIZE_MAX;
  FOR_EACH(iray, iray, nrays) {
    if(!rays[iray].is_enabled)
      continue;

    res = svo_node_trace_ray(svo, inode, node_lower, node_size, lod_range,
      svo_rays + iray, hits + rays[iray].iray);
    ASSERT(res == RES_OK);

    if(VXL_HIT_NONE(hits + rays[iray].iray)) {
      if(*iray_1st_active == SIZE_MAX)
        *iray_1st_active = iray;
    } else {
#if 0 /* For debug */
      hits[rays[iray].iray].color[0] = 255;
      hits[rays[iray].iray].color[1] = 0;
      hits[rays[iray].iray].color[2] = 0;
#endif
      rays[iray].is_enabled = 0;
      ++nhits;
    }
  }
  return nhits;
}

static INLINE void
trace
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const struct svo_index* iroot,
   struct ray* rays,
   struct svo_ray* svo_rays, /* May be NULL <=> no single ray fallback */
   const size_t nrays,
   struct packet* packet, /* The ray packet */
   const float footprint, /* packet footprint */
   const int traversal_ibits[3],
   struct vxl_hit* hits)
{
  #define SCALE_MAX 23 /* #mantisses of a float */
  struct stack_entry {
    struct svo_index inode; /* SVO node index */
    size_t iray;
    int ichild;
    int nchildren; /* # traversed children */
  } stack[SCALE_MAX + 1/*Dummy entry use to avoid invalid read*/];
  struct aabb aabb;
  struct svo_index inode;
  size_t iray;
  size_t nrays_intersected;
  float scale_exp2;
  float t[2];
  int scale_range[2];
  int scale;
  int ichild;
  int nchildren;
  int rt_mixed;
  ASSERT(svo && rays && nrays && packet && hits);
  ASSERT(traversal_ibits && lod_range && lod_range[0] <= lod_range[1]);
  ASSERT(packet->single_org);

  /* The SVO AABB is assumed to be [1, 2]^3 */
  f3_splat(aabb.lower, 1.f);
  f3_splat(aabb.upper, 2.f);

  /* Discard packet that does not intersect the SVO */
  if(!packet_intersect_aabb(packet, &aabb, t))
     return;

  /* The packet is too incoherent with respect to the SVO AABB. */
  rt_mixed = svo_rays != NULL;
  if(rt_mixed && is_tiny_node(t[0], &aabb, footprint)) {
    iray = 0;
    trace_single_ray(svo, iroot, aabb.lower, 1.f, lod_range, &iray, rays,
      svo_rays, nrays, hits);
    return; /* All packetized rays were single ray-traced */
  }

  /* Define the id and the lower left corner of the first child */
  ichild = 0;
  if(packet->org.lower[0] > 1.5f) { ichild |= 4; aabb.lower[0] = 1.5f; }
  if(packet->org.lower[1] > 1.5f) { ichild |= 2; aabb.lower[1] = 1.5f; }
  if(packet->org.lower[2] > 1.5f) { ichild |= 1; aabb.lower[2] = 1.5f; }

  /* Traversal initialisation */
  inode = *iroot;
  iray = 0;
  nchildren = 0;
  nrays_intersected = 0;
  scale_exp2 = 0.5f;
  scale = SCALE_MAX - 1;
  scale_range[0] = (int)(SCALE_MAX - MMIN(lod_range[0], SCALE_MAX));
  scale_range[1] = (int)(SCALE_MAX - MMIN(lod_range[1], SCALE_MAX));

  /* The dummy entry must stop the node popping, i.e. nchildren must be < 8 */
  stack[SCALE_MAX].nchildren = 0;

  /* Iterative SVO traversal */
  while(scale < SCALE_MAX) {
    const struct svo_xnode* node = svo_get_node(svo, &inode);
    const int child_flag = BIT(ichild); /* Flag of the child */
    ++nchildren;

    if(child_flag & node->is_valid) { /* The child node is not empty */

      /* Define the child AABB */
      aabb.upper[0] = aabb.lower[0] + scale_exp2;
      aabb.upper[1] = aabb.lower[1] + scale_exp2;
      aabb.upper[2] = aabb.lower[2] + scale_exp2;

      if((child_flag & node->is_leaf) || scale < scale_range[1]) { /* Leaf */
        const struct svo_node_attrib* attr;
        attr = svo_get_child_attrib(svo, &inode, (uint8_t)ichild);
        nrays_intersected += trace_leaf
          (&aabb, attr, &iray, rays, nrays, packet, hits);
        ASSERT(nrays_intersected <= nrays);

        /* The front to back SVO traversal ensures that one can stop the
         * traversal when all rays hit something */
        if(nrays_intersected == nrays) break;

      } else { /* Sub node */
        int intersect = 0;
        size_t iray_1st = iray;

        if(rays[iray_1st].is_enabled) /* Early hit */
          intersect = ray_intersect_aabb(rays + iray_1st, &aabb, t);

        if(!intersect) {
          iray_1st = nrays;
          intersect = packet_intersect_aabb(packet, &aabb, t);
          if(intersect) { /* !Early fail */
            /* Look for the 1st active ray */
            FOR_EACH(iray_1st, iray + 1, nrays) {
              if(!rays[iray_1st].is_enabled) continue;
              if(ray_intersect_aabb(rays + iray_1st, &aabb, t)) break;
            }
          }
        }

        if(iray_1st < nrays) { /* The packet hit the child node */
          struct svo_index inode_next;

          /* Retriver the index of the child node */
          svo_get_child_index(svo, &inode, (int)ichild, &inode_next);

          if(rt_mixed && is_tiny_node(t[0], &aabb, footprint)) {
            /* Packet is too incoherent: fall-back in single ray traversal */
            nrays_intersected += trace_single_ray(svo, &inode_next, aabb.lower,
              scale_exp2, lod_range, &iray_1st, rays, svo_rays, nrays, hits);

            if(nrays_intersected == nrays) break; /* No more active ray */

          } else { /* Setup next stack entry */
            float mid[3];

            /* Save the current node */
            stack[scale].iray = iray;
            stack[scale].inode = inode;
            stack[scale].ichild = ichild;
            stack[scale].nchildren = nchildren;

            inode = inode_next;
            iray = iray_1st;
            nchildren = 0;
            scale_exp2 *= 0.5f;
            scale -= 1;
            nchildren = 0;

            /* Define the id and the lower left corner of the first child */
            ichild = 0;
            mid[0] = aabb.lower[0] + scale_exp2;
            mid[1] = aabb.lower[1] + scale_exp2;
            mid[2] = aabb.lower[2] + scale_exp2;
            if(packet->org.lower[0] > mid[0]) { ichild |= 4; aabb.lower[0] = mid[0]; }
            if(packet->org.lower[1] > mid[1]) { ichild |= 2; aabb.lower[1] = mid[1]; }
            if(packet->org.lower[2] > mid[2]) { ichild |= 1; aabb.lower[2] = mid[2]; }
            continue;
          }
        }
      }
    }

    if(nchildren < 8 && iray < nrays/*Active ray in the node*/) {
      int iaxis;

      /* Use the traversal bit indices to ensure a front to back traversal of the
       * children. Refer to the setup_traversal_ibits for additional details. */

      ichild ^= BIT(traversal_ibits[0]);
      iaxis = 2 - traversal_ibits[0];
      aabb.lower[iaxis] = uitof(ftoui(aabb.lower[iaxis]) ^ (uint32_t)BIT(scale));

      if(nchildren % 2 == 0) {
        ichild ^= BIT(traversal_ibits[1]);
        iaxis = 2 - traversal_ibits[1];
        aabb.lower[iaxis] = uitof(ftoui(aabb.lower[iaxis]) ^ (uint32_t)BIT(scale));
      }
      if(nchildren % 4 == 0) {
        ichild ^= BIT(traversal_ibits[2]);
        iaxis = 2 - traversal_ibits[2];
        aabb.lower[iaxis] = uitof(ftoui(aabb.lower[iaxis]) ^ (uint32_t)BIT(scale));
      }
    } else { /* Pop node */
      int scale_parent;
      uint32_t mask;

      /* Pop a node with children to traverse. The SCALE_MAX entry prevents
       * overflow */
      for(++scale; stack[scale].nchildren >= 8; ++scale);

      /* Pop the node data */
      inode = stack[scale].inode;
      iray = stack[scale].iray;
      ichild = stack[scale].ichild;
      nchildren = stack[scale].nchildren;

      /* Setup the next child to traverse. Use the traversal bit indices to
       * ensure a front to back traversal of the children. Refer to the
       * setup_traversal_ibits for additional details */
      ichild ^= BIT(traversal_ibits[0]);
      if(nchildren % 2 == 0) ichild ^= BIT(traversal_ibits[1]);
      if(nchildren % 4 == 0) ichild ^= BIT(traversal_ibits[2]);

      /* Compute the node aabb */
      scale_exp2 = uitof(((uint32_t)(scale - SCALE_MAX) + 127) << 23);
      scale_parent = scale + 1;
      mask = ~(uint32_t)((1 << scale_parent) - 1);
      aabb.lower[0] = uitof(ftoui(aabb.lower[0]) & mask);
      aabb.lower[1] = uitof(ftoui(aabb.lower[1]) & mask);
      aabb.lower[2] = uitof(ftoui(aabb.lower[2]) & mask);
      if((ichild & 4) != 0) aabb.lower[0] += scale_exp2;
      if((ichild & 2) != 0) aabb.lower[1] += scale_exp2;
      if((ichild & 1) != 0) aabb.lower[2] += scale_exp2;
    }
  }
}

/*******************************************************************************
 * Local back-end function
 ******************************************************************************/
res_T
svo_trace_packets
  (struct vxl_be_svo* svo,
   const enum vxl_rt_coherency coherency,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float ray_org[3],
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits)
{
  struct ray rays[MAX_PACKET_WIDTH];
  struct svo_ray svo_rays[MAX_PACKET_WIDTH];
  size_t idir, irange;
  size_t dir_step, range_step;
  size_t iray;
  float org[3];
  float svo_size;
  float svo_scale;
  float vxl_size;
  int traversal_ibits[3];
  const char rt_mixed = coherency == VXL_RT_COHERENCY_MIXED;
  size_t packet_width;
  ASSERT(coherency != VXL_RT_COHERENCY_NONE);

  if(!svo
  || !lod_range || lod_range[0] > lod_range[1]
  || !nrays || !ray_org || !dirs || !ranges || !hits)
    return RES_BAD_ARG;

  /* The SVO packet tracing cannot be invoked on bundle whose rays does not
   * share the same origin */
  if(!(mask & VXL_RAYS_SINGLE_ORIGIN))
    return RES_BAD_ARG;

  /* Initialize the hit array. Note that currently, the packet traversal stops
   * at the same SVO level for all rays. The size of the intersected voxel is
   * thus computed once */
  vxl_size = compute_intersected_voxel_size(svo, lod_range[1]);
  FOR_EACH(iray, 0, nrays) {
    hits[iray].distance = (float)INF;
    hits[iray].size = vxl_size;
  }

  /* Define the packet width */
  switch(coherency) {
    case VXL_RT_COHERENCY_HIGH: packet_width = 256; break;
    case VXL_RT_COHERENCY_MEDIUM: packet_width = 64; break;
    case VXL_RT_COHERENCY_LOW: packet_width = 16; break;
    case VXL_RT_COHERENCY_MIXED: packet_width = 64; break;
    default: FATAL("Unreachable code\n"); break;
  }
  ASSERT(packet_width <= MAX_PACKET_WIDTH);

  /* Assume that the SVO is a cube */
  svo_size = svo->upper[0] - svo->lower[0];
  ASSERT(eq_epsf(svo->upper[1] - svo->lower[1], svo_size, svo_size*1.e-4f));
  ASSERT(eq_epsf(svo->upper[2] - svo->lower[2], svo_size, svo_size*1.e-4f));

  /* Transform the ray org in [1, 2] space */
  svo_scale = 1.f / svo_size;
  org[0] = (ray_org[0] - svo->lower[0]) * svo_scale + 1;
  org[1] = (ray_org[1] - svo->lower[1]) * svo_scale + 1;
  org[2] = (ray_org[2] - svo->lower[2]) * svo_scale + 1;

  /* Setup ray data layout */
  dir_step = mask & VXL_RAYS_SINGLE_DIRECTION ? 0 : 3;
  range_step = mask & VXL_RAYS_SINGLE_RANGE ? 0 : 2;
  idir = 0, irange = 0;

  iray = 0;
  while(iray < nrays) {
    struct packet packet;
    struct ia3 iadir;
    struct ia3 iaorg;
    float packet_main_dir[3];
    float iarange[2] = { FLT_MAX, -FLT_MAX };
    size_t iray_tmp = 0;
    size_t iray_packet = 0;
    size_t nrays_packet;

    ia3_set1(&iadir, FLT_MAX, -FLT_MAX);
    ia3_set(&iaorg, org, org);

    FOR_EACH(iray_tmp, 0, packet_width) { /* Fill the ray packet */
      if(ranges[irange + 0] < ranges[irange + 1]) {
        if(ranges[irange + 0] < 0.f) return RES_BAD_ARG;

        /* Setup the packet ray in the [0, 1] space */
        f3_set(rays[iray_packet].org, org);
        f3_set(rays[iray_packet].dir, dirs + idir);
        rays[iray_packet].rcp_dir[0] = 1.f / rays[iray_packet].dir[0];
        rays[iray_packet].rcp_dir[1] = 1.f / rays[iray_packet].dir[1];
        rays[iray_packet].rcp_dir[2] = 1.f / rays[iray_packet].dir[2];
        f2_mulf(rays[iray_packet].range, ranges + irange, svo_scale);
        rays[iray_packet].iray = iray;
        rays[iray_packet].is_enabled = 1;

        /* Update the ray packet interval */
        f3_min(iadir.lower, iadir.lower, rays[iray_packet].dir);
        f3_max(iadir.upper, iadir.upper, rays[iray_packet].dir);
        iarange[0] = MMIN(iarange[0], rays[iray_packet].range[0]);
        iarange[1] = MMAX(iarange[1], rays[iray_packet].range[1]);

        /* Setup the single ray fallback */
        if(rt_mixed) {
          res_T res = svo_setup_ray(svo, ray_org, dirs + idir, NULL, NULL,
            ranges + irange, &svo_rays[iray_packet]);
          ASSERT(res == RES_OK), (void)res;
        }

        ++iray_packet;
      }
      /* Fetch next ray attributes */
      if(++iray >= nrays) {
        break;
      } else {
        idir += dir_step;
        irange += range_step;
      }
    }
    if(!iray_packet) continue; /* No valid ray */

    /* Setup the ray packet */
    nrays_packet = iray_packet;
    packet_setup(&packet, &iaorg, &iadir, iarange);

    /* Define the SVO node traversal order */
    f3_add(packet_main_dir, iadir.lower, iadir.upper);
    f3_mulf(packet_main_dir, packet_main_dir, 0.5f);
    setup_traversal_ibits(traversal_ibits, packet_main_dir);

    /* The packet footprint is used to switch from packet tracing to single ray
     * tracing when the bundle becomes too incoherent */
    if(!rt_mixed) {
      trace(svo, lod_range, &svo->root, rays, NULL, nrays_packet, &packet, 0,
        traversal_ibits, hits);
    } else {
      const float footprint = packet_footprint(&iadir);
      trace(svo, lod_range, &svo->root, rays, svo_rays, nrays_packet, &packet,
        footprint, traversal_ibits, hits);
    }
  }
  return RES_OK;
}

