/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_be_c.h"
#include "vxl_be_svo_c.h"
#include "vxl_be_svo_trace.h"

#include <rsys/float2.h>
#include <rsys/float3.h>

/*
 * Implement the ray/SVO intersection routines. Based on the Revelles' paper
 * "An Efficient Parametric Algorithm for Octree Traversal" and the the Laine's
 * "stack popping" optimisations presented in "Efficient Sparse Voxel Octree -
 * Analysis, Extensions and implementation".
 *
 * SVO node memory layout:
 *      +...+...+      #---#---#
 *      |`#--`#--`#    |`#--`#--`#
 *      + |`#--`#--`#  # | 3 | 7 |`+
 *      '`# | 2 | 6 |  |`#---#---# .
 *   Y  + |`#---#---#  # | 1 | 5 |`+
 * Z |   `# | 0 | 4 |   `#---#---# .
 *  \|     `#---#---#     `+..`+..`+
 *   o--X
 */

/*******************************************************************************
 * Helper functins
 ******************************************************************************/
static FINLINE void
setup_hit
  (const float distance,
   const float size, /* World space size of the intersected voxel */
   const enum vxl_face face, /* Hit voxel face */
   const struct svo_node_attrib* attrib,
   struct vxl_hit* hit)
{
  ASSERT(hit && attrib);
  ASSERT((unsigned)face <= 6);
  hit->distance = distance;
  hit->size = size;
  hit->face = face;
  xnormal_get(&attrib->normal, hit->normal);
  hit->color[0] = attrib->color[0];
  hit->color[1] = attrib->color[1];
  hit->color[2] = attrib->color[2];
}

/* Compute the ray footprint at a distance of one from the ray origin */
static FINLINE float
ray_footprint
  (const float dir[3],
   const float dir_dx[3],
   const float dir_dy[3])
{
  float cos_dir, sqr_hypotenuse, side_len, diag_len;
  ASSERT(dir && dir_dx && dir_dy);
  ASSERT(f3_is_normalized(dir));
  ASSERT(f3_is_normalized(dir_dx));
  ASSERT(f3_is_normalized(dir_dy));

  cos_dir = MMIN(f3_dot(dir, dir_dx), f3_dot(dir, dir_dy));
  if(!cos_dir)
    return 0.f;

  sqr_hypotenuse = 1.f / (cos_dir * cos_dir);
  side_len = (float)sqrt(sqr_hypotenuse - 1.0); /* Pythagore */
  diag_len = side_len * 1.41421356237309504880f /* sqrt(2) */;
  return diag_len;
}

/* Return 1 if the node size is smaller than the world space ray footprint */
static FINLINE int
is_tiny_node
  (const float node_distance, /* Distance from the ray origin */
   const float node_size,
   const float ray_footprint)
{
  ASSERT(node_distance >= 0.f && node_size > 0.f && ray_footprint >= 0.f);
  return node_size < ray_footprint * node_distance;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
svo_setup_ray
  (const struct vxl_be_svo* svo,
   const float org[3],
   const float dir[3],
   const float dir_dx[3], /* May be NULL <=> no differential ray */
   const float dir_dy[3], /* May be NULL <=> no differential ray */
   const float range[3],
   struct svo_ray* ray)
{
  float svo_size, svo_scale;
  const int has_differential = dir_dx && dir_dy;
  ASSERT(ray);

  if(!svo || !org || !dir || !range || range[0] < 0.f)
    return RES_BAD_ARG;

  if(range[0] >= range[1]) /* Disabled ray */
    return RES_BAD_OP;

  /* Rays parallel to an axis and that does not intersect the SVO */
  if((!dir[0] && (org[0] < svo->lower[0] || org[0] > svo->upper[0]))
  || (!dir[1] && (org[1] < svo->lower[1] || org[1] > svo->upper[1]))
  || (!dir[2] && (org[2] < svo->lower[2] || org[1] > svo->upper[2])))
    return RES_BAD_OP;

  /* Assume that the SVO is a cube */
  svo_size = svo->upper[0] - svo->lower[0];
  ASSERT(eq_epsf(svo->upper[1] - svo->lower[1], svo_size, svo_size*1.e-4f));
  ASSERT(eq_epsf(svo->upper[2] - svo->lower[2], svo_size, svo_size*1.e-4f));

  /* Transform the ray org in [1, 2] space */
  svo_scale = 1.f / svo_size;
  ray->org[0] = (org[0] - svo->lower[0]) * svo_scale + 1.f;
  ray->org[1] = (org[1] - svo->lower[1]) * svo_scale + 1.f;
  ray->org[2] = (org[2] - svo->lower[2]) * svo_scale + 1.f;

  /* Transform the ray range in the normalized SVO space */
  ray->range[0] = range[0] * svo_scale;
  ray->range[1] = range[1] * svo_scale;

  /* Let ray define as org + t*dir and X the coordinate of an axis aligned
   * plane. ray intersects X at t = (X - org)/dir = (X - org) * ts; with
   * ts = 1/dir. Note that one assume that dir is always negative. */
  ray->ts[0] = 1.f / -absf(dir[0]);
  ray->ts[1] = 1.f / -absf(dir[1]);
  ray->ts[2] = 1.f / -absf(dir[2]);

  /* Mirror rays with positive directions */
  ray->octant_mask = 0;
  if(dir[0] > 0.f) { ray->octant_mask ^= 4; ray->org[0] = 3.f - ray->org[0]; }
  if(dir[1] > 0.f) { ray->octant_mask ^= 2; ray->org[1] = 3.f - ray->org[1]; }
  if(dir[2] > 0.f) { ray->octant_mask ^= 1; ray->org[2] = 3.f - ray->org[2]; }

  if(has_differential) {
    ray->footprint = ray_footprint(dir, dir_dx, dir_dy);
  } else {
    ray->footprint = -1.f;
  }

  return RES_OK;
}

res_T
svo_trace_ray
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const float org[3],
   const float dir[3],
   const float dir_dx[3],
   const float dir_dy[3],
   const float range[2],
   struct vxl_hit* hit)
{
  const float lower[3] = {1.f, 1.f, 1.f};
  struct svo_ray ray;
  res_T res;

  if(!hit || !lod_range || lod_range[0] > lod_range[1])
    return RES_BAD_ARG;

  hit->distance = (float)INF;
  res = svo_setup_ray(svo, org, dir, dir_dx, dir_dy, range, &ray);
  if(res == RES_BAD_OP) { /* Disabled ray or // ray that does not hit the SVO */
    return RES_OK;
  } else if(res != RES_OK) {
    return res;
  }

  return svo_node_trace_ray(svo, &svo->root, lower, 1.f, lod_range, &ray, hit);
}

res_T
svo_node_trace_ray
  (struct vxl_be_svo* svo,
   const struct svo_index* node_id,
   const float node_lower[3],
   const float node_size,
   const uint32_t lod_range[2],
   const struct svo_ray* ray,
   struct vxl_hit* hit)
{
  #define SCALE_MAX 23 /* #matisse bits */
  struct stack_entry {
    struct svo_index inode;
    float t_max;
  } stack[SCALE_MAX + 1/*Dummy entry use to avoid invalid read*/];

  struct svo_index inode;
  float t_min, t_max; /* Min/Max ray intersection with a node */
  float svo_size;
  float lower[3], upper[3], mid[3];
  float corner[3];
  float scale_exp2;
  uint32_t scale_range[2];
  uint32_t scale; /* <=> stack index */
  uint32_t scale_max;
  uint32_t ichild;

  ASSERT(svo && node_lower && ray && hit);
  ASSERT(lod_range && lod_range[0] <= lod_range[1]);
  ASSERT(node_id && !SVO_INDEX_EQ(node_id, &SVO_INDEX_NULL));

  /* Assume that the SVO is a cube */
  svo_size = svo->upper[0] - svo->lower[0];
  ASSERT(eq_epsf(svo->upper[1] - svo->lower[1], svo_size, svo_size*1.e-4f));
  ASSERT(eq_epsf(svo->upper[2] - svo->lower[2], svo_size, svo_size*1.e-4f));

  /* Initialise the hit to "no intersection" */
  hit->distance = (float)INF;

  /* Define the submitted node AABB */
  lower[0] = node_lower[0]; upper[0] = node_lower[0] + node_size;
  lower[1] = node_lower[1]; upper[1] = node_lower[1] + node_size;
  lower[2] = node_lower[2]; upper[2] = node_lower[2] + node_size;

  /* Flip the AABB coordinate with respect to positive ray directions */
  if(ray->octant_mask & 4) {
    lower[0] = 3 - lower[0];
    upper[0] = 3 - upper[0];
    SWAP(float, lower[0], upper[0]);
  }
  if(ray->octant_mask & 2) {
    lower[1] = 3 - lower[1];
    upper[1] = 3 - upper[1];
    SWAP(float, lower[1], upper[1]);
  }
  if(ray->octant_mask & 1) {
    lower[2] = 3 - lower[2];
    upper[2] = 3 - upper[2];
    SWAP(float, lower[2], upper[2]);
  }

  /* Compute the min/max ray intersection with the node */
  t_min =      (upper[0] - ray->org[0]) * ray->ts[0];
  t_min = MMAX((upper[1] - ray->org[1]) * ray->ts[1], t_min);
  t_min = MMAX((upper[2] - ray->org[2]) * ray->ts[2], t_min);
  t_max =      (lower[0] - ray->org[0]) * ray->ts[0];
  t_max = MMIN((lower[1] - ray->org[1]) * ray->ts[1], t_max);
  t_max = MMIN((lower[2] - ray->org[2]) * ray->ts[2], t_max);
  t_min = MMAX(ray->range[0], t_min);
  t_max = MMIN(ray->range[1], t_max);
  if(t_min >= t_max) return RES_OK; /* No intersection */

  /* Traversal initialisation */
  inode = *node_id;
  scale_range[0] = SCALE_MAX - MMIN(lod_range[0], SCALE_MAX);
  scale_range[1] = SCALE_MAX - MMIN(lod_range[1], SCALE_MAX);
  scale_exp2 = node_size * 0.5f;
  scale = SCALE_MAX + ((ftoui(scale_exp2) >> 23) - 127);
  ASSERT(scale < SCALE_MAX);
  ASSERT(1.f / (float)(1 << (SCALE_MAX - scale)) == scale_exp2);

  /* Define the first child id and the position of its lower left corner */
  ichild = 0;
  corner[0] = lower[0]; mid[0] = lower[0] + scale_exp2;
  corner[1] = lower[1]; mid[1] = lower[1] + scale_exp2;
  corner[2] = lower[2]; mid[2] = lower[2] + scale_exp2;
  if((mid[0] - ray->org[0])*ray->ts[0] > t_min) { ichild^=4; corner[0]=mid[0]; }
  if((mid[1] - ray->org[1])*ray->ts[1] > t_min) { ichild^=2; corner[1]=mid[1]; }
  if((mid[2] - ray->org[2])*ray->ts[2] > t_min) { ichild^=1; corner[2]=mid[2]; }

  /* Iterative SVO traversal */
  scale_max = scale + 1;
  while(scale < scale_max) {
    const struct svo_xnode* node = svo_get_node(svo, &inode);
    float t_corner[3];
    float t_max_corner;
    float t_max_child;
    uint32_t ichild_adjusted = ichild ^ ray->octant_mask;
    uint32_t ichild_flag = (uint32_t)BIT(ichild_adjusted);
    uint32_t istep;

    /* Compute the exit point of the ray in the current child node */
    t_corner[0] = (corner[0] - ray->org[0])*ray->ts[0];
    t_corner[1] = (corner[1] - ray->org[1])*ray->ts[1];
    t_corner[2] = (corner[2] - ray->org[2])*ray->ts[2];
    t_max_corner = MMIN(t_corner[0], MMIN(t_corner[1], t_corner[2]));

    /* Traverse the current child */
    if((node->is_valid & ichild_flag)
    && t_min <= (t_max_child = MMIN(t_max, t_max_corner))) {
      float scale_exp2_child;
      float t_max_parent;
      float t_center[3]; /* Center of the child node */

      t_max_parent = t_max;
      t_max = t_max_child;

      if(node->is_leaf & ichild_flag) break; /* Leaf node */
      if(scale < scale_range[1]) break; /* Clamp to the LOD max */

      /* Ensure that the child node is greater than the ray footprint. Since
       * the distance from the ray_origin to the node center is not computed we
       * conservatively use the distance to the node exit point */
      if(ray->footprint > 0.f && scale < scale_range[0]
      && is_tiny_node(t_max_child, scale_exp2, ray->footprint))
        break;

      scale_exp2_child = scale_exp2 * 0.5f;

      /* center = corner - child_scale_exp2 =>
       * t_center = ts*(corner - child_scale_exp2) + tb;
       * t_center = t_corner + ts*child_scale_exp2 */
      t_center[0] = (corner[0] - ray->org[0] + scale_exp2_child)*ray->ts[0];
      t_center[1] = (corner[1] - ray->org[1] + scale_exp2_child)*ray->ts[1];
      t_center[2] = (corner[2] - ray->org[2] + scale_exp2_child)*ray->ts[2];

      /* Push the parent node */
      stack[scale].t_max = t_max_parent;
      stack[scale].inode = inode;

      /* Get children data */
      svo_get_child_index(svo, &inode, (int)ichild_adjusted, &inode);

      /* Define the id and the lower left corner of the first child */
      ichild = 0;
      if(t_center[0] > t_min) { ichild ^= 4; corner[0] += scale_exp2_child; }
      if(t_center[1] > t_min) { ichild ^= 2; corner[1] += scale_exp2_child; }
      if(t_center[2] > t_min) { ichild ^= 1; corner[2] += scale_exp2_child; }

      --scale;
      scale_exp2 = scale_exp2_child;
      continue;
    }

    /* Define the id and the lower left corner of the next child */
    istep = 0;
    if(t_corner[0] <= t_max_corner) { istep ^= 4; corner[0] -= scale_exp2; }
    if(t_corner[1] <= t_max_corner) { istep ^= 2; corner[1] -= scale_exp2; }
    if(t_corner[2] <= t_max_corner) { istep ^= 1; corner[2] -= scale_exp2; }
    ichild ^= istep;

    t_min = t_max_corner; /* Min ray intersection with the next child */

    if((ichild & istep) != 0) { /* The ray exits the child node */
      uint32_t diff_bits = 0;
      uint32_t shift[3];

      /* Smart trick that pops the nodes up to the next node to traverse rather
       * than popping the current node only. */
      if(istep & 4) diff_bits |= ftoui(corner[0]) ^ ftoui(corner[0]+scale_exp2);
      if(istep & 2) diff_bits |= ftoui(corner[1]) ^ ftoui(corner[1]+scale_exp2);
      if(istep & 1) diff_bits |= ftoui(corner[2]) ^ ftoui(corner[2]+scale_exp2);
      scale = (ftoui((float)diff_bits) >> 23) - 127;
      scale_exp2 = uitof(((scale - SCALE_MAX) + 127) << 23);

      inode = stack[scale].inode;
      t_max = stack[scale].t_max;

      shift[0] = ftoui(corner[0]) >> scale;
      shift[1] = ftoui(corner[1]) >> scale;
      shift[2] = ftoui(corner[2]) >> scale;
      corner[0] = uitof(shift[0] << scale);
      corner[1] = uitof(shift[1] << scale);
      corner[2] = uitof(shift[2] << scale);

      /* Define the child to traverse in the popped node */
      ichild = ((shift[0] & 1) << 2) | ((shift[1] & 1) << 1) | (shift[2] & 1);
    }
  }
  if(scale < scale_max) {
    const uint8_t ichild_adjusted = (uint8_t)(ichild ^ ray->octant_mask);
    const struct svo_node_attrib* attr;
    float hit_t;
    int hit_face;

    /* Retrieve the node attribs */
    attr = svo_get_child_attrib(svo, &inode, ichild_adjusted);

    /* Define the hit voxel face. Hit the lower or upper faces whether the ray
     * starts into or outside the hit voxel, respectively. Note that one have
     * to flip the hit face with respect to the ray `octant_mask'. */
    if(t_min <= ray->range[0]) { /* Ray starts into the voxel */
      hit_t = t_max;
      hit_face =
        (corner[0]-ray->org[0])*ray->ts[0] == t_max ? VXL_FACE_LEFT :
        (corner[1]-ray->org[1])*ray->ts[1] == t_max ? VXL_FACE_BOTTOM :
        VXL_FACE_BACK;
      if(ray->octant_mask & (uint8_t)(1<<(2-hit_face/2))) ++hit_face;
    } else {
      hit_t = t_min;
      hit_face =
        (corner[0]+scale_exp2-ray->org[0])*ray->ts[0] == t_min ? VXL_FACE_RIGHT :
        (corner[1]+scale_exp2-ray->org[1])*ray->ts[1] == t_min ? VXL_FACE_TOP :
        VXL_FACE_FRONT;
      if(ray->octant_mask & (uint8_t)(1<<(2-hit_face/2))) --hit_face;
    }
    setup_hit(hit_t * svo_size, svo_size * scale_exp2, hit_face, attr, hit);
  }
  return RES_OK;
}

res_T
svo_trace_rays
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* dirs_dx, /* May be NULL */
   const float* dirs_dy, /* May be NULL */
   const float* ranges,
   struct vxl_hit* hits)
{
  size_t iorg, idir, irange;
  size_t iray;
  size_t org_step, dir_step, range_step;
  res_T res = RES_OK;
  const char has_differentials = dirs_dx != NULL;

  if((!dirs_dx && dirs_dy) || (dirs_dx && !dirs_dy))
    return RES_BAD_ARG;

  org_step = mask & VXL_RAYS_SINGLE_ORIGIN ? 0 : 3;
  dir_step = mask & VXL_RAYS_SINGLE_DIRECTION ? 0 : 3;
  range_step = mask & VXL_RAYS_SINGLE_RANGE ? 0 : 2;
  iorg = idir = irange = 0;

  if(has_differentials) {
    FOR_EACH(iray, 0, nrays) {
      res = svo_trace_ray(svo, lod_range, orgs+iorg, dirs+idir,
        dirs_dx+idir, dirs_dy+idir, ranges+irange, hits+iray);
      if(UNLIKELY(res != RES_OK)) break;
      iorg += org_step, idir += dir_step, irange += range_step;
    }
  } else {
    FOR_EACH(iray, 0, nrays) {
      res = svo_trace_ray(svo, lod_range, orgs+iorg, dirs+idir, NULL,
        NULL, ranges+irange, hits+iray);
      if(UNLIKELY(res != RES_OK)) break;
      iorg += org_step, idir += dir_step, irange += range_step;
    }
  }
  return res;
}

