/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_RAY_H
#define VXL_BE_RAY_H

#include "vxl_be_aabb.h"
#include <rsys/float2.h>
#include <rsys/float3.h>

struct ray {
  float org[3]; /* Origin */
  float dir[3]; /* Direction */
  float rcp_dir[3]; /* Reciprocal direction */
  float range[2]; /* Near/Far ray range */
  size_t iray; /* Ray identifier */
  char is_enabled; /* Define if the ray is active or not */
};

static FINLINE void
ray_setup
  (struct ray* ray,
   const float org[3], /* World space ray origin */
   const float dir[3], /* World space ray direction. Should be normalized */
   const float range[2], /* Near/Far ray range */
   const size_t iray, /* Ray identifier */
   const float scale) /* Ray scale factor */
{
  ASSERT(ray && org && dir && range && scale && f3_is_normalized(dir));
  f3_mulf(ray->org, org, scale);
  f3_mulf(ray->dir, dir, scale);
  f2_mulf(ray->range, range, scale);
  f3(ray->rcp_dir, 1.f/ray->dir[0], 1.f/ray->dir[1], 1.f/ray->dir[2]);
  ray->iray = iray;
  ray->is_enabled = 1;
}

/* Smits intersection: "Efficiency issues for ray tracing" */
static FINLINE char
ray_intersect_aabb
  (const struct ray* ray,
   const struct aabb* aabb,
   float t[2]) /* Near/Far parametric ray intersection values. May be NULL */
{
  float t0, t1;
  int i;
  ASSERT(ray && aabb && ray->is_enabled);
  t0 = ray->range[0]; t1 = ray->range[1];
  FOR_EACH(i, 0, 3) {
    float t_min = (aabb->lower[i] - ray->org[i]) * ray->rcp_dir[i];
    float t_max = (aabb->upper[i] - ray->org[i]) * ray->rcp_dir[i];
    if(t_min > t_max) SWAP(float, t_min, t_max);
    t0 = t_min > t0 ? t_min : t0;
    t1 = t_max < t1 ? t_max : t1;
    if(t0 > t1) return 0;
  }
  if(t) f2(t, t0, t1);
  return 1;
}

#endif /* VXL_BE_RAY_H */

