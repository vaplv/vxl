/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_PACKET_H
#define VXL_BE_PACKET_H

#include "vxl_be_aabb.h"
#include "vxl_be_interval.h"
#include <rsys/float2.h>

struct packet { /* Ray packet */
  struct ia3 org; /* Interval of the ray origins */
  struct ia3 rcp_dir; /* Interval of the reciprocal ray directions */
  float range[2]; /* Interval of the ray ranges */
  int single_org; /* Define whether the org interval is degenerated or not */
};

static FINLINE void
packet_setup
  (struct packet* packet,
   const struct ia3* org,
   const struct ia3* dir,
   const float range[2])
{
  ASSERT(packet && org && dir);
  packet->org = *org;
  ia3_rcp(&packet->rcp_dir, dir);
  f2_set(packet->range, range);
  packet->single_org = f3_eq_eps(org->lower, org->upper, 1.e-6f);
}

static FINLINE char
packet_intersect_aabb
  (const struct packet* packet,
   const struct aabb* aabb,
   float out_t[2]) /* May be NULL */
{
  struct ia3 t, box;
  float t0, t1;
  ASSERT(packet && aabb);
  f3_set(box.lower, aabb->lower);
  f3_set(box.upper, aabb->upper);
  ia3_sub(&t, &box, &packet->org);
  ia3_mul(&t, &t, &packet->rcp_dir);

  t0 = MMAX(MMAX(MMAX(t.lower[0], t.lower[1]), t.lower[2]), packet->range[0]);
  t1 = MMIN(MMIN(MMIN(t.upper[0], t.upper[1]), t.upper[2]), packet->range[1]);
  if(out_t) {
    out_t[0] = t0;
    out_t[1] = t1;
  }
  return t0 <= t1;
}

#endif /* VXL_BE_PACKET_H */

