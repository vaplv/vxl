/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"
#include "vxl_be_c.h"
#include "vxl_be_device_c.h"
#include "vxl_be_triangle.h"

#include <rsys/float2.h>
#include <rsys/float3.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

/* Constant defining an axis aligned plane */
#define XY 2
#define XZ 1
#define YZ 0

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE void
project_position
  (const float V0[3],
   const float E0[3],
   const float N[3],
   const float NxE1[3],
   const float rcp_det,
   const float pos[3],
   float uvw[3])
{
  float T[3], Q[3], k;
  ASSERT(V0 && E0 && N && NxE1 && pos && uvw);

  f3_sub(T, pos, V0);
  uvw[0] = f3_dot(T, NxE1) * rcp_det;
  f3_cross(Q, T, E0);
  uvw[1] = f3_dot(Q, N) * rcp_det;
  uvw[2] = 1.f - uvw[0] - uvw[1];

  if(uvw[0] >= 0.f && uvw[1] >= 0.f && uvw[2] >= 0.f) {
    ASSERT(eq_eps(uvw[0] + uvw[1] + uvw[2], 1.f, 1.e-6f));
    return;
  }

  /* Clamp barycentric coordinates to triangle edges */
  if(uvw[0] >= 0.f) {
    if(uvw[1] >= 0.f) {
      k = 1.f / (uvw[0] + uvw[1]);
      uvw[0] *= k;
      uvw[1] *= k;
      uvw[2] = 0.f;
    } else if( uvw[2] >= 0.f) {
      k = 1.f / (uvw[0] + uvw[2]);
      uvw[0] *= k;
      uvw[1] = 0.f;
      uvw[2] *= k;
    } else {
      ASSERT(uvw[0] >= 1.f);
      f3(uvw, 1.f, 0.f, 0.f);
    }
  } else if(uvw[1] >= 0.f) {
    if(uvw[2] >= 0.f) {
      k = 1.f / (uvw[1] + uvw[2]);
      uvw[0] = 0.f;
      uvw[1] *= k;
      uvw[2] *= k;
    } else {
      ASSERT(uvw[1] >= 1.f);
      f3(uvw, 0.f, 1.f, 0.f);
    }
  } else {
    ASSERT(uvw[2] >= 1.f);
    f3(uvw, 0.f, 0.f, 1.f);
  }
}

static FINLINE res_T
emit_voxel
  (struct darray_voxacc* voxaccs,
   struct htable_voxacc* mcode2voxacc,
   const struct voxel* vxl)
{
  size_t* pivoxacc;
  ASSERT(voxaccs && mcode2voxacc && vxl);

  pivoxacc = htable_voxacc_find(mcode2voxacc, &vxl->mcode);
  if(pivoxacc) { /* Accumulate the voxel attributes  */
    struct voxacc* voxacc = darray_voxacc_data_get(voxaccs) + *pivoxacc;
    int32_t col32[3];
    ASSERT(*pivoxacc < darray_voxacc_size_get(voxaccs));

    /* Accumulate the voxel attributes if the result is not going to overflow */
    if((col32[0] = voxacc->col[0] + vxl->col[0]) <= UINT16_MAX
    && (col32[1] = voxacc->col[1] + vxl->col[1]) <= UINT16_MAX
    && (col32[2] = voxacc->col[2] + vxl->col[2]) <= UINT16_MAX) {
      f3_add(voxacc->normal, voxacc->normal, vxl->normal);
      voxacc->col[0] = (uint16_t)col32[0];
      voxacc->col[1] = (uint16_t)col32[1];
      voxacc->col[2] = (uint16_t)col32[2];
      ++voxacc->naccums;
    }
  } else { /* Register a new voxel */
    struct voxacc* voxacc;
    size_t ivoxacc;
    res_T res = RES_OK;

    /* Allocate the voxel accum */
    ivoxacc = darray_voxacc_size_get(voxaccs);
    res = darray_voxacc_resize(voxaccs, ivoxacc + 1);
    if(res != RES_OK) return res;
    voxacc = darray_voxacc_data_get(voxaccs) + ivoxacc;

    /* Setup the voxel accum */
    voxacc->naccums = 1;
    voxacc->mcode = vxl->mcode;
    voxacc->col[0] = vxl->col[0];
    voxacc->col[1] = vxl->col[1];
    voxacc->col[2] = vxl->col[2];
    f3_set(voxacc->normal, vxl->normal);

    /* Register the voxel accum against the accumulator */
    res = htable_voxacc_set(mcode2voxacc, &vxl->mcode, &ivoxacc);
    if(res != RES_OK) {
      darray_voxacc_pop_back(voxaccs); /* Free the voxel accum */
      return res;
    }
  }
  return RES_OK;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
triangle_setup
  (struct triangle* t,
   const size_t imesh,
   const float origin[3], /* World space origin of the voxels */
   const uint32_t definition[3],
   const float vxl_sz,
   float v[3][3]) /* triangle vertices */
{
  float N_abs[3]; /* Absolute value of the triangle normal */
  float lower[3], upper[3]; /* Triangle AABB */
  float e[3][3]; /* Edge vector v[i+1 mod 3] - v[i]*/
  float area; /* Triangle area */
  float col_sz[3]; /* Size of a voxel column */
  ASSERT(t && v && vxl_sz > 0.f);
  (void)definition;

  /* Compute the triangle AABB */
  lower[0] = MMIN(MMIN(v[0][0], v[1][0]), v[2][0]);
  lower[1] = MMIN(MMIN(v[0][1], v[1][1]), v[2][1]);
  lower[2] = MMIN(MMIN(v[0][2], v[1][2]), v[2][2]);
  upper[0] = MMAX(MMAX(v[0][0], v[1][0]), v[2][0]);
  upper[1] = MMAX(MMAX(v[0][1], v[1][1]), v[2][1]);
  upper[2] = MMAX(MMAX(v[0][2], v[1][2]), v[2][2]);

  /* Transform the triangle AABB to voxel space */
  t->vlower[0] = (uint32_t)((lower[0]-origin[0]) / vxl_sz);
  t->vlower[1] = (uint32_t)((lower[1]-origin[1]) / vxl_sz);
  t->vlower[2] = (uint32_t)((lower[2]-origin[2]) / vxl_sz);
  t->vupper[0] = (uint32_t)snap_float(upper[0]-origin[0], vxl_sz, SNAP_CEIL);
  t->vupper[1] = (uint32_t)snap_float(upper[1]-origin[1], vxl_sz, SNAP_CEIL);
  t->vupper[2] = (uint32_t)snap_float(upper[2]-origin[2], vxl_sz, SNAP_CEIL);
  ASSERT(t->vlower[0] < definition[0]);
  ASSERT(t->vlower[1] < definition[1]);
  ASSERT(t->vlower[2] < definition[2]);
  ASSERT(t->vupper[0] <= definition[0]);
  ASSERT(t->vupper[1] <= definition[1]);
  ASSERT(t->vupper[2] <= definition[2]);

  /* Compute the triangle edges */
  f3_sub(e[0], v[1], v[0]);
  f3_sub(e[1], v[2], v[1]);
  f3_sub(e[2], v[0], v[2]);

  /* Compute the triangle normal */
  f3_cross(t->N, e[0], e[1]);
  area = f3_normalize(t->N, t->N);
  t->is_valid = area > 0.f;
  if(!t->is_valid) return; /* Degenerated triangle */

  /* Compute the Moller Trumbore triangle parameters */
  f3_cross(t->NxE1, t->N, e[1]);
  t->rcp_det = 1.f / f3_dot(t->NxE1, e[0]);

  /* Define the dominant normal axis */
  N_abs[0] = absf(t->N[0]);
  N_abs[1] = absf(t->N[1]);
  N_abs[2] = absf(t->N[2]);
  t->iaxism = N_abs[0] > N_abs[1]
    ? (N_abs[0] > N_abs[2] ? 0 : 2)
    : (N_abs[1] > N_abs[2] ? 1 : 2);

  /* Define the remaining non dominant normal axis */
  t->iaxis0 = (t->iaxism + 1) % 3;
  t->iaxis1 = (t->iaxism + 2) % 3;

  /* Define the world space size of a column for the dominant axis of N */
  f3_splat(col_sz, vxl_sz);
  col_sz[t->iaxism] = (float)
    (t->vupper[t->iaxism] - t->vlower[t->iaxism]) * vxl_sz;

  /* Let P = Ax+By+Cz+D = 0 with {A, B, C} = N the plane normal and
   * D = -(N.v[0]), z the dominant axis of N, and
   * c = N.z > 0 ? (N.xy > 0 ? sz : 0) : (N.xy <= 0 ? sz : 0). The triangle
   * plane intersects the voxel Z-column [C, C+S] in a range whose bounds are
   * computed as follow:
   *
   *         P(C + c) = Nx*(Cx+c) + Ny*(Cy+c) + Nz*Cz - N.v[0]
   *        Zmin = Cz = (-Nx*Cx - Ny*Cy - (Nx*cx + Ny*cy - N.v[0]))/Nz
   *                  = (-Nx*Cx - Ny*Cy - b[0]) * a
   *
   *  P(C+(S-c)) = N.C + N.(S-c) - N.v[0]
   *                  = N.x*(Cx+Sx-cx) + Ny*(Cy+Sy-cy) + Nz*Cz - N.v[0]
   *        Zmax = Cz = (-Nx*Cx - Ny*Cy - (Nx*(Sx-cx) + Ny*(Sy-cy) - N.v[0]))/Nz
   *                  = (-Nx*Cx - Ny*Cy - b[1]) * a
   *
   * Note that c uses the sign N principal axis to select the 2 column corners
   * that, once projected onto the triangle plane, define the lower and the
   * upper bound of the triangle into the voxels column */
  t->a = 1.f/t->N[t->iaxism];
  t->b[0] = t->b[1] = -f3_dot(t->N, v[0]);
  if(t->N[t->iaxism] > 0) {
    t->b[t->N[t->iaxis0] <= 0] += t->N[t->iaxis0] * col_sz[t->iaxis0];
    t->b[t->N[t->iaxis1] <= 0] += t->N[t->iaxis1] * col_sz[t->iaxis1];
  } else {
    t->b[t->N[t->iaxis0] > 0] += t->N[t->iaxis0] * col_sz[t->iaxis0];
    t->b[t->N[t->iaxis1] > 0] += t->N[t->iaxis1] * col_sz[t->iaxis1];
  }

  /* Compute the triangle edge normals in the XY, XZ and YZ plane. Ensure that
   * the normals point *into* the triangle. */
  if(t->N[2] >= 0) {
    f2(t->N2d[XY][0], -e[0][1], e[0][0]);
    f2(t->N2d[XY][1], -e[1][1], e[1][0]);
    f2(t->N2d[XY][2], -e[2][1], e[2][0]);
  } else {
    f2(t->N2d[XY][0], e[0][1], -e[0][0]);
    f2(t->N2d[XY][1], e[1][1], -e[1][0]);
    f2(t->N2d[XY][2], e[2][1], -e[2][0]);
  }
  if(t->N[1] >= 0) {
    f2(t->N2d[XZ][0], e[0][2], -e[0][0]);
    f2(t->N2d[XZ][1], e[1][2], -e[1][0]);
    f2(t->N2d[XZ][2], e[2][2], -e[2][0]);
  } else {
    f2(t->N2d[XZ][0], -e[0][2], e[0][0]);
    f2(t->N2d[XZ][1], -e[1][2], e[1][0]);
    f2(t->N2d[XZ][2], -e[2][2], e[2][0]);
  }
  if(t->N[0] >= 0) {
    f2(t->N2d[YZ][0], -e[0][2], e[0][1]);
    f2(t->N2d[YZ][1], -e[1][2], e[1][1]);
    f2(t->N2d[YZ][2], -e[2][2], e[2][1]);
  } else {
    f2(t->N2d[YZ][0], e[0][2], -e[0][1]);
    f2(t->N2d[YZ][1], e[1][2], -e[1][1]);
    f2(t->N2d[YZ][2], e[2][2], -e[2][1]);
  }

  /* Let E = Ax + By + C = 0 with {A, B} = N the edge normal and D = -(N.v),
   * and c = N > 0 ? sz : 0, the triangle intersects the voxel rectangle
   * (l,l+sz) in a given 2D plane if the E(l+(sz-c)) >= 0 for all 3 triangle
   * edges with E(l+(sz-c)) = N(l+(sz-c)) + D = N.l + N.(sz-c) - N.v = N.l+d */
  /* XY plane */
  t->d2d[XY][0] = -(t->N2d[XY][0][0]*v[0][0] + t->N2d[XY][0][1]*v[0][1]);
  t->d2d[XY][1] = -(t->N2d[XY][1][0]*v[1][0] + t->N2d[XY][1][1]*v[1][1]);
  t->d2d[XY][2] = -(t->N2d[XY][2][0]*v[2][0] + t->N2d[XY][2][1]*v[2][1]);
  if(t->N2d[XY][0][0] > 0) t->d2d[XY][0] += t->N2d[XY][0][0]*vxl_sz;
  if(t->N2d[XY][0][1] > 0) t->d2d[XY][0] += t->N2d[XY][0][1]*vxl_sz;
  if(t->N2d[XY][1][0] > 0) t->d2d[XY][1] += t->N2d[XY][1][0]*vxl_sz;
  if(t->N2d[XY][1][1] > 0) t->d2d[XY][1] += t->N2d[XY][1][1]*vxl_sz;
  if(t->N2d[XY][2][0] > 0) t->d2d[XY][2] += t->N2d[XY][2][0]*vxl_sz;
  if(t->N2d[XY][2][1] > 0) t->d2d[XY][2] += t->N2d[XY][2][1]*vxl_sz;
  /* XZ plane */
  t->d2d[XZ][0] = -(t->N2d[XZ][0][0]*v[0][0] + t->N2d[XZ][0][1]*v[0][2]);
  t->d2d[XZ][1] = -(t->N2d[XZ][1][0]*v[1][0] + t->N2d[XZ][1][1]*v[1][2]);
  t->d2d[XZ][2] = -(t->N2d[XZ][2][0]*v[2][0] + t->N2d[XZ][2][1]*v[2][2]);
  if(t->N2d[XZ][0][0] > 0) t->d2d[XZ][0] += t->N2d[XZ][0][0]*vxl_sz;
  if(t->N2d[XZ][0][1] > 0) t->d2d[XZ][0] += t->N2d[XZ][0][1]*vxl_sz;
  if(t->N2d[XZ][1][0] > 0) t->d2d[XZ][1] += t->N2d[XZ][1][0]*vxl_sz;
  if(t->N2d[XZ][1][1] > 0) t->d2d[XZ][1] += t->N2d[XZ][1][1]*vxl_sz;
  if(t->N2d[XZ][2][0] > 0) t->d2d[XZ][2] += t->N2d[XZ][2][0]*vxl_sz;
  if(t->N2d[XZ][2][1] > 0) t->d2d[XZ][2] += t->N2d[XZ][2][1]*vxl_sz;
  /* YZ plane0*/
  t->d2d[YZ][0] = -(t->N2d[YZ][0][0]*v[0][1] + t->N2d[YZ][0][1]*v[0][2]);
  t->d2d[YZ][1] = -(t->N2d[YZ][1][0]*v[1][1] + t->N2d[YZ][1][1]*v[1][2]);
  t->d2d[YZ][2] = -(t->N2d[YZ][2][0]*v[2][1] + t->N2d[YZ][2][1]*v[2][2]);
  if(t->N2d[YZ][0][0] > 0) t->d2d[YZ][0] += t->N2d[YZ][0][0]*vxl_sz;
  if(t->N2d[YZ][0][1] > 0) t->d2d[YZ][0] += t->N2d[YZ][0][1]*vxl_sz;
  if(t->N2d[YZ][1][0] > 0) t->d2d[YZ][1] += t->N2d[YZ][1][0]*vxl_sz;
  if(t->N2d[YZ][1][1] > 0) t->d2d[YZ][1] += t->N2d[YZ][1][1]*vxl_sz;
  if(t->N2d[YZ][2][0] > 0) t->d2d[YZ][2] += t->N2d[YZ][2][0]*vxl_sz;
  if(t->N2d[YZ][2][1] > 0) t->d2d[YZ][2] += t->N2d[YZ][2][1]*vxl_sz;

  f3_set(t->E0, e[0]);
  f3_set(t->V0, v[0]);
  t->imesh = imesh;
}

/* Conservative voxelization based on the Triangle/box overlap test presented
 * in "Fast Parallel Surface and Solid Voxelization on GPUs" of Schwarz and
 * Seidel */
res_T
triangle_voxelize
  (const struct triangle* t,
   struct darray_voxacc* voxaccs,
   struct htable_voxacc* mcode2voxacc,
   const uint32_t lower[3],/*Low bound of the AABB in which itri is voxelized*/
   const uint32_t upper[3],/*Upp bound of the AABB in which itri is voxelized*/
   const float vxl_sz, /* World space voxel size */
   const size_t itri, /* Id of the triangle to voxelize */
   const struct vxl_mesh_desc* mesh,
   const float origin[3]) /* Voxel space origin */
{
  uint32_t t_low[3], t_upp[3]; /* Adjusted voxelization domain of the triangle */
  uint32_t pos[3];

  /* Voxel attributes */
  struct voxel v;
  uint64_t mcode[3]; /* Morton code cache */
  float vlow[3]; /* World space voxel lower bound */
  float vmid[3]; /* World space voxel center. Used as the sample position */

  /* Voxels column data */
  float col_range[2]; /* Column range intersected by the triangle */
  uint32_t vcol_range[2]; /* Column range in voxel space */

  /* Miscellaneous variables */
  float uvw[3];
  int i, j, k;
  res_T res;

  /* Emperical epsilon that handles numerical inaccuracy */
  const float eps = vxl_sz*1.e-4f;

  ASSERT(t && voxaccs && mcode2voxacc && vxl_sz > 0.f && mesh && origin);

  /* The triangle AABB should intersect the voxelization domain */
  ASSERT(t->vlower[0] < upper[0] && t->vupper[0] > lower[0]);
  ASSERT(t->vlower[1] < upper[1] && t->vupper[1] > lower[1]);
  ASSERT(t->vlower[2] < upper[2] && t->vupper[2] > lower[2]);

  /* Shrink the triangle AABB to the voxelization domain */
  t_low[0] = MMAX(t->vlower[0], lower[0]);
  t_low[1] = MMAX(t->vlower[1], lower[1]);
  t_low[2] = MMAX(t->vlower[2], lower[2]);
  t_upp[0] = MMIN(t->vupper[0], upper[0]);
  t_upp[1] = MMIN(t->vupper[1], upper[1]);
  t_upp[2] = MMIN(t->vupper[2], upper[2]);

  FOR_EACH(pos[t->iaxis0], t_low[t->iaxis0], t_upp[t->iaxis0]) {
    vlow[t->iaxis0] = (float)pos[t->iaxis0]*vxl_sz + origin[t->iaxis0];
    vmid[t->iaxis0] = vlow[t->iaxis0] + vxl_sz*0.5f;
    mcode[t->iaxis0] = morton3D_encode_u21(pos[t->iaxis0]) << (2-t->iaxis0);

    FOR_EACH(pos[t->iaxis1], t_low[t->iaxis1], t_upp[t->iaxis1]) {
      vlow[t->iaxis1] = (float)pos[t->iaxis1]*vxl_sz + origin[t->iaxis1];
      vmid[t->iaxis1] = vlow[t->iaxis1] + vxl_sz*0.5f;
      mcode[t->iaxis1] = mcode[t->iaxis0] |
        morton3D_encode_u21(pos[t->iaxis1]) << (2-t->iaxis1);

      /* Test the triangle voxel intersection in the {iaxis0, iaxis1} plane */
      i = t->iaxism; j = t->iaxis0; k = t->iaxis1;
      if(j > k) SWAP(int, j, k);
      if(t->N2d[i][0][0]*vlow[j]+t->N2d[i][0][1]*vlow[k]+t->d2d[i][0] < -eps
      || t->N2d[i][1][0]*vlow[j]+t->N2d[i][1][1]*vlow[k]+t->d2d[i][1] < -eps
      || t->N2d[i][2][0]*vlow[j]+t->N2d[i][2][1]*vlow[k]+t->d2d[i][2] < -eps)
        continue;

      /* Compute the column range intersected by the triangle */
      col_range[0] = col_range[1] = -t->N[j]*vlow[j] -t->N[k]*vlow[k];
      col_range[0] = (col_range[0] - t->b[0]) * t->a;
      col_range[1] = (col_range[1] - t->b[1]) * t->a;
      ASSERT(col_range[0] <= col_range[1]);

      /* Transform the intersected column range in voxel space */
      col_range[0] = col_range[0] - origin[i];
      col_range[1] = col_range[1] - origin[i];
      vcol_range[0] = (uint32_t)(col_range[0] / vxl_sz);
      vcol_range[1] = (uint32_t)snap_float(col_range[1], vxl_sz, SNAP_CEIL);
      vcol_range[0] = MMAX(vcol_range[0], t_low[t->iaxism]);
      vcol_range[1] = MMIN(vcol_range[1], t_upp[t->iaxism]);

      FOR_EACH(pos[t->iaxism], vcol_range[0], vcol_range[1]) {
        vlow[t->iaxism] = (float)pos[t->iaxism]*vxl_sz + origin[t->iaxism];
        vmid[t->iaxism] = vlow[t->iaxism] + vxl_sz*0.5f;
        mcode[t->iaxism] = mcode[t->iaxis1] |
          morton3D_encode_u21(pos[t->iaxism]) << (2-t->iaxism);

        /* Test the triangle/voxel intersection in the {iaxism, iaxis1} plane */
        i = t->iaxis0; j = t->iaxism; k = t->iaxis1;
        if(j > k) SWAP(int, j, k);
        if(t->N2d[i][0][0]*vlow[j]+t->N2d[i][0][1]*vlow[k]+t->d2d[i][0] < -eps
        || t->N2d[i][1][0]*vlow[j]+t->N2d[i][1][1]*vlow[k]+t->d2d[i][1] < -eps
        || t->N2d[i][2][0]*vlow[j]+t->N2d[i][2][1]*vlow[k]+t->d2d[i][2] < -eps)
          continue;

        /* Test the triangle/voxel intersection in the {iaxism, iaxis0} plane */
        i = t->iaxis1; j = t->iaxism; k = t->iaxis0;
        if(j > k) SWAP(int, j, k);
        if(t->N2d[i][0][0]*vlow[j]+t->N2d[i][0][1]*vlow[k]+t->d2d[i][0] < -eps
        || t->N2d[i][1][0]*vlow[j]+t->N2d[i][1][1]*vlow[k]+t->d2d[i][1] < -eps
        || t->N2d[i][2][0]*vlow[j]+t->N2d[i][2][1]*vlow[k]+t->d2d[i][2] < -eps)
          continue;

        /* Retrieve the voxel attributes */
        project_position(t->V0, t->E0, t->N, t->NxE1, t->rcp_det, vmid, uvw);
        mesh->get_color(itri, uvw, v.col, mesh->data);
        mesh->get_normal(itri, uvw, v.normal, mesh->data);
        v.mcode = mcode[t->iaxism];

        /* Emit a voxel */
        res = emit_voxel(voxaccs, mcode2voxacc, &v);
        if(res != RES_OK) return res;
      }
    }
  }
  return RES_OK;
}


