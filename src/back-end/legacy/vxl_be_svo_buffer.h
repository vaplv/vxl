/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_BUFFER_H
#define VXL_BE_SVO_BUFFER_H

#include "vxl_be_svo_buffer_incore.h"
#include "vxl_be_svo_buffer_oocore.h"

enum svo_buffer_type {
  SVO_BUFFER_INCORE,
  SVO_BUFFER_OOCORE,
  SVO_BUFFER_NONE__
};

struct svo_buffer {
  enum svo_buffer_type type;
  union {
    struct svo_buffer_oocore oocore;
    struct svo_buffer_incore incore;
  } data;
  struct mem_allocator* allocator;
};

#define SVO_BUFFER_FUNC__(Func, Buf, Args)                                     \
  (ASSERT((Buf) && (unsigned)(Buf)->type < SVO_BUFFER_NONE__),                 \
   (Buf)->type == SVO_BUFFER_INCORE                                            \
   ? svo_buffer##_incore_##Func(&(Buf)->data.incore COMMA_##Args LIST_##Args)  \
   : svo_buffer##_oocore_##Func(&(Buf)->data.oocore COMMA_##Args LIST_##Args))

static INLINE void
svo_buffer_init(struct mem_allocator* allocator, struct svo_buffer* buf)
{
  ASSERT(buf);
  buf->allocator = allocator;
  buf->type = SVO_BUFFER_NONE__;
}

static INLINE void
svo_buffer_release(struct svo_buffer* buf)
{
  ASSERT(buf);
  switch(buf->type) {
    case SVO_BUFFER_INCORE: svo_buffer_incore_release(&buf->data.incore); break;
    case SVO_BUFFER_OOCORE: svo_buffer_oocore_release(&buf->data.oocore); break;
    case SVO_BUFFER_NONE__: /* Do nothing */ break;
    default: FATAL("Unreachable code\n"); break;
  }
}

static INLINE res_T
svo_buffer_setup
  (struct svo_buffer* buf,
   const enum svo_buffer_type type,
   const uint16_t identifier,
   const size_t pagesize)
{
  res_T res = RES_OK;
  ASSERT(buf);

  switch(type) {
    case SVO_BUFFER_INCORE:
      svo_buffer_incore_init(buf->allocator, &buf->data.incore);
      break;
    case SVO_BUFFER_OOCORE:
      svo_buffer_oocore_init(buf->allocator, &buf->data.oocore);
      break;
    default: FATAL("Unreachable code\n"); break;
  }
  buf->type = type;
  res = SVO_BUFFER_FUNC__(setup, buf, ARG2(identifier, pagesize));
  if(res != RES_OK) goto error;
    
exit:
  return res;
error:
  goto exit;
}

static INLINE res_T
svo_buffer_alloc_nodes
  (struct svo_buffer* buf,
   const size_t nnodes,
   struct svo_index* first_node, /* Index toward the 1st allocated node */
   struct svo_xnode** nodes, /* Allocated nodes */
   struct svo_node_attrib** attribs) /* Allocated attributes */
{
  return SVO_BUFFER_FUNC__
    (alloc_nodes, buf, ARG4(nnodes, first_node, nodes, attribs));
}

static INLINE res_T
svo_buffer_alloc_far_index
  (struct svo_buffer* buf,
   struct svo_index* id, /* Index toward the far index */
   struct svo_index** far_index) /* Allocated far index */
{
  return SVO_BUFFER_FUNC__(alloc_far_index, buf, ARG2(id, far_index));
}

static INLINE res_T
svo_buffer_alloc_leaf_attribs
  (struct svo_buffer* buf,
   const size_t nleafs,
   struct svo_index* first_attrib,
   struct svo_node_attrib** attribs) /* Allocated leaf attributes */
{
  return SVO_BUFFER_FUNC__
    (alloc_leaf_attribs, buf, ARG3(nleafs, first_attrib, attribs));
}

static INLINE struct svo_xnode*
svo_buffer_get_node
  (struct svo_buffer* buf, const struct svo_index* id)
{
  return SVO_BUFFER_FUNC__(get_node, buf, ARG1(id));
}

static INLINE struct svo_node_attrib*
svo_buffer_get_node_attrib
  (struct svo_buffer* buf, const struct svo_index* id)
{
  return SVO_BUFFER_FUNC__(get_node_attrib, buf, ARG1(id));
}

static INLINE struct svo_index*
svo_buffer_get_far_index
  (struct svo_buffer* buf, const struct svo_index* id)
{
  return SVO_BUFFER_FUNC__(get_far_index, buf, ARG1(id));
}

static INLINE struct svo_node_attrib*
svo_buffer_get_leaf_attrib
  (struct svo_buffer* buf, const struct svo_index* id)
{
  return SVO_BUFFER_FUNC__(get_leaf_attrib, buf, ARG1(id));
}

static INLINE void
svo_buffer_get_child_index
  (struct svo_buffer* buf,
   const struct svo_index* id,
   const int ichild, /* in [0, 7] */
   struct svo_index* out_id)
{
  SVO_BUFFER_FUNC__(get_child_index, buf, ARG3(id, ichild, out_id));
}

static INLINE res_T
svo_buffer_finalize(struct svo_buffer* buf)
{
  ASSERT(buf);
  if(buf->type == SVO_BUFFER_OOCORE) {
    return svo_buffer_oocore_finalize(&buf->data.oocore);
  } else {
    return RES_OK;
  }
}

#undef SVO_BUFFER_FUNC__

#endif /* VXL_BE_SVO_BUFFER_H */

