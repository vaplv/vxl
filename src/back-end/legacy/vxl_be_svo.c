/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"
#include "vxl_be_device_c.h"
#include "vxl_be_svo_c.h"

#include <rsys/float3.h>
#include <rsys/mem_allocator.h>

static void
be_svo_release(ref_T* ref)
{
  struct vxl_be_device* dev = NULL;
  struct vxl_be_svo* svo = NULL;
  ASSERT(ref);
  svo = CONTAINER_OF(ref, struct vxl_be_svo, ref);
  dev = svo->dev;
  darray_buffer_release(&svo->buffers);
  mem_shutdown_regular_allocator(&svo->allocator);
  MEM_RM(dev->allocator, svo);
  VXL(be_device_ref_put(dev));
}

/*******************************************************************************
 * Exported back-end functions
 ******************************************************************************/
VXL_DECLARE_REF_FUNCS(be_svo)

res_T
vxl_be_svo_create(struct vxl_be_device* dev, struct vxl_be_svo** out_svo)
{
  struct vxl_be_svo* svo = NULL;
  res_T res = RES_OK;

  if(!dev || !out_svo) {
    res = RES_BAD_ARG;
    goto error;
  }
  svo = MEM_CALLOC(dev->allocator, 1, sizeof(struct vxl_be_svo));
  if(!svo) {
    res = RES_MEM_ERR;
    goto error;
  }
  mem_init_regular_allocator(&svo->allocator);
  VXL(be_device_ref_get(dev));
  svo->dev = dev;
  ref_init(&svo->ref);
  svo->root = SVO_INDEX_NULL;
  darray_buffer_init(&svo->allocator, &svo->buffers);

  f3_splat(svo->lower, FLT_MAX);
  f3_splat(svo->upper,-FLT_MAX);
  svo->nvoxels = 0;
  svo->definition = 0;

exit:
  if(out_svo)
    *out_svo = svo;
  return res;
error:
  if(svo) {
    VXL(be_svo_ref_put(svo));
    svo = NULL;
  }
  goto exit;
}

res_T
vxl_be_svo_clear(struct vxl_be_svo* svo)
{
  if(!svo) return RES_BAD_ARG;

  darray_buffer_purge(&svo->buffers);
  svo->root = SVO_INDEX_NULL;
  f3_splat(svo->lower, FLT_MAX);
  f3_splat(svo->upper,-FLT_MAX);
  svo->definition = 0;
  svo->nvoxels = 0;
  return RES_OK;
}

res_T
vxl_be_svo_get_desc(const struct vxl_be_svo* svo, struct vxl_be_svo_desc* desc)
{
  if(!svo || !desc) return RES_BAD_ARG;
  f3_set(desc->lower, svo->lower);
  f3_set(desc->upper, svo->upper);
  desc->voxels_count = svo->nvoxels;
  desc->definition = svo->definition;
  desc->mem_size =
    MEM_ALLOCATED_SIZE(&svo->allocator) + sizeof(struct vxl_be_svo);
  return RES_OK;
}

