/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_TRIANGLE_H
#define VXL_BE_TRIANGLE_H

#include "vxl_be.h"
#include "vxl_be_voxel.h"

#include <rsys/dynamic_array_size_t.h>
#include <rsys/hash_table.h>
#include <rsys/ref_count.h>

/* Intermediary data structure used to represent a triangular during its
 * voxelization process. */
struct triangle {
  /* Triangle attribute */
  float N[3]; /* Triangle normal */
  uint32_t vlower[3], vupper[3]; /* Triangle AABB in voxel space */

  /* Used in the "Moller Trumbore" based projection of an arbitrary position
   * onto the parametric domain of a triangle */
  float V0[3]; /* First vertex <=> v[0] */
  float E0[3]; /* First edge <=> e[0] */
  float NxE1[3]; /* Normal x e[1] */
  float rcp_det; /* 1 / (NxE1 . e[0]) */

  /* Triangle edges parameters */
  float N2d[3][3][2]; /* Edge normals in the XY, YZ and XZ planes */
  float d2d[3][3];

  /* Miscellaneous variables */
  size_t imesh; /* Index of the mesh to wich the triangle belongs to */
  float a, b[2];
  int iaxism; /* Index of the main normal axis */
  int iaxis0, iaxis1; /* Index of the non dominant normal axises */
  int is_valid; /* Is the triangle valid, i.e. not degenerated */
};

/* Declare the darray_triangle data structure */
#define DARRAY_NAME triangle
#define DARRAY_DATA struct triangle
#include <rsys/dynamic_array.h>

extern LOCAL_SYM void
triangle_setup
  (struct triangle* triangle,
   const size_t imesh, /* Id of the mesh the triangle belongs to */
   const float origin[3], /* World space origin of the voxel space */
   const uint32_t definition[3], /* Overall voxel space definition */
   const float vxl_size, /* World space size of a voxel */
   float vertices[3][3]); /*  Triangle vertices */

/* Conservative triangle voxelization */
extern LOCAL_SYM res_T
triangle_voxelize
  (const struct triangle* triangle,
   struct darray_voxacc* voxaccs, /* Accumulator */
   struct htable_voxacc* mcode2voxacc, /* Map a voxel mcode to its accum */
   const uint32_t lower[3],/*Low bound of the AABB in which itri is voxelized*/
   const uint32_t upper[3],/*Upp bound of the AABB in which itri is voxelized*/
   const float vxl_sz, /* World space voxel size */
   const size_t itri, /* Id of the triangle to voxelize */
   const struct vxl_mesh_desc* mesh, /* Mesh to which the triangle belongs to */
   const float origin[3]); /* Voxel space origin */

#endif /* VXL_BE_TRIANGLE_H */

