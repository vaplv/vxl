/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_be_svo_buffer_incore.h"

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
ensure_allocated_nodes(struct svo_buffer_incore* buf, const size_t nnodes)
{
  char* node_page = NULL;
  struct svo_node_attrib* attr_page = NULL;
  size_t nnode_pages = 0;
  size_t nattr_pages = 0;
  res_T res = RES_OK;
  ASSERT(buf);

  /* Sufficient memory space in the current page */
  if(buf->node_head.ipage != SVO_INDEX_NULL.ipage
  && buf->node_head.inode + nnodes <= buf->page_nnodes)
    goto exit;

  nnode_pages = darray_node_page_size_get(&buf->node_pages);
  nattr_pages = darray_attr_page_size_get(&buf->attr_pages);
  ASSERT(nnode_pages == nattr_pages);
  ASSERT(nnode_pages == buf->node_head.ipage + 1);

  /* Alloc and register a node page containing the node and the far indices */
  node_page = MEM_ALLOC(buf->allocator, buf->pagesize);
  if(!node_page) {
    res = RES_MEM_ERR;
    goto error;
  }
  res = darray_node_page_push_back(&buf->node_pages, &node_page);
  if(res != RES_OK) goto error;

  /* Alloc and register a page of node attribs */
  attr_page = MEM_ALLOC(buf->allocator, buf->pagesize);
  if(!attr_page) {
    res = RES_MEM_ERR;
    goto error;
  }
  res = darray_attr_page_push_back(&buf->attr_pages, &attr_page);
  if(res != RES_OK) goto error;

  /* Set the buffer head to the first element of the newly allocated page */
  ASSERT(nnode_pages <= SVO_INDEX_IPAGE_MAX);
  buf->node_head.inode = 0;
  buf->node_head.ipage = (page_id_T)nnode_pages;

exit:
  return res;
error:
  if(node_page) MEM_RM(buf->allocator, node_page);
  if(attr_page) MEM_RM(buf->allocator, attr_page);
  darray_node_page_resize(&buf->node_pages, nnode_pages);
  darray_attr_page_resize(&buf->attr_pages, nattr_pages);
  goto exit;
}

static INLINE res_T
ensure_allocated_leafs(struct svo_buffer_incore* buf, const size_t nleafs)
{
  struct svo_node_attrib* leaf_page = NULL;
  size_t nleaf_pages = 0;
  res_T res = RES_OK;
  ASSERT(buf);

  /* Sufficient memory space in the current page */
  if(buf->leaf_head.ipage != SVO_INDEX_NULL.ipage
  && buf->leaf_head.inode + nleafs <= buf->page_nnodes)
    goto exit;

  nleaf_pages = darray_attr_page_size_get(&buf->leaf_pages);
  ASSERT(nleaf_pages == buf->leaf_head.ipage + 1);

  /* Alloc and register a page of leaf attribs */
  leaf_page = MEM_ALLOC(buf->allocator, buf->pagesize);
  if(!leaf_page) {
    res = RES_MEM_ERR;
    goto error;
  }
  res = darray_attr_page_push_back(&buf->leaf_pages, &leaf_page);
  if(res != RES_OK) goto error;

  ASSERT(nleaf_pages <= SVO_INDEX_IPAGE_MAX);
  buf->leaf_head.inode = 0;
  buf->leaf_head.ipage = (page_id_T)nleaf_pages;

exit:
  return res;
error:
  if(leaf_page) MEM_RM(buf->allocator, leaf_page);
  darray_attr_page_resize(&buf->leaf_pages, nleaf_pages);
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
svo_buffer_incore_init
  (struct mem_allocator* allocator, struct svo_buffer_incore* buf)
{
  ASSERT(allocator && buf);
  memset(buf, 0, sizeof(struct svo_buffer_incore));
  darray_node_page_init(allocator, &buf->node_pages);
  darray_attr_page_init(allocator, &buf->attr_pages);
  darray_attr_page_init(allocator, &buf->leaf_pages);
  buf->node_head = SVO_INDEX_NULL;
  buf->leaf_head = SVO_INDEX_NULL;
  buf->allocator = allocator;
}

void
svo_buffer_incore_release(struct svo_buffer_incore* buf)
{
  ASSERT(buf);
  svo_buffer_incore_clear(buf);
  darray_node_page_release(&buf->node_pages);
  darray_attr_page_release(&buf->attr_pages);
  darray_attr_page_release(&buf->leaf_pages);
}

res_T
svo_buffer_incore_setup
  (struct svo_buffer_incore* buf,
   const uint16_t identifier,
   const size_t pagesize)
{
  ASSERT(buf && pagesize);
  ASSERT(buf->node_head.ibuffer == SVO_INDEX_NULL.ibuffer);
  ASSERT(buf->leaf_head.ibuffer == SVO_INDEX_NULL.ibuffer);

  buf->pagesize = pagesize;
  buf->page_nnodes = pagesize/sizeof(struct svo_xnode);
  buf->page_nnodes = MMIN(buf->page_nnodes, SVO_INDEX_INODE_MAX);

  buf->node_head.ibuffer = identifier;
  buf->leaf_head.ibuffer = identifier;

  return RES_OK;
}

res_T
svo_buffer_incore_copy
  (struct svo_buffer_incore* dst, const struct svo_buffer_incore* src)
{
  char* dst_node_page = NULL;
  const char* src_node_page = NULL;
  struct svo_node_attrib* dst_attr_page = NULL;
  const struct svo_node_attrib* src_attr_page = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(dst && src && dst->pagesize == src->pagesize);

  if(dst == src) return RES_OK;
  svo_buffer_incore_clear(dst);

  /* Copy the node pages */
  FOR_EACH(i, 0, darray_node_page_size_get(&src->node_pages)) {
    src_node_page = darray_node_page_cdata_get(&src->node_pages)[i];
    dst_node_page = MEM_ALLOC(dst->allocator, dst->pagesize);
    if(!dst_node_page) {
      res = RES_MEM_ERR;
      goto error;
    }
    memcpy(dst_node_page, src_node_page, src->pagesize);
    res = darray_node_page_push_back(&dst->node_pages, &dst_node_page);
    if(res != RES_OK) goto error;
    dst_node_page = NULL;
  }

  /* Copy the node attrib pages */
  FOR_EACH(i, 0, darray_attr_page_size_get(&src->attr_pages)) {
    src_attr_page = darray_attr_page_cdata_get(&src->attr_pages)[i];
    dst_attr_page = MEM_ALLOC(dst->allocator, dst->pagesize);
    if(!dst_attr_page) {
      res = RES_MEM_ERR;
      goto error;
    }
    memcpy(dst_attr_page, src_attr_page, src->pagesize);
    res = darray_attr_page_push_back(&dst->attr_pages, &dst_attr_page);
    if(res != RES_OK) goto error;
    dst_attr_page = NULL;
  }

  /* Copy the leaf attrib pages */
  FOR_EACH(i, 0, darray_attr_page_size_get(&src->leaf_pages)) {
    src_attr_page = darray_attr_page_cdata_get(&src->leaf_pages)[i];
    dst_attr_page = MEM_ALLOC(dst->allocator, dst->pagesize);
    if(!dst_attr_page) {
      res = RES_MEM_ERR;
      goto error;
    }
    memcpy(dst_attr_page, src_attr_page, src->pagesize);
    res = darray_attr_page_push_back(&dst->leaf_pages, &dst_attr_page);
    if(res != RES_OK) goto error;
    dst_attr_page = NULL;
  }

  /* Copy miscellaneous parameters */
  dst->node_head = src->node_head;
  dst->leaf_head = src->leaf_head;
  dst->pagesize = src->pagesize;
  dst->page_nnodes = src->page_nnodes;

exit:
  return res;
error:
  if(dst_node_page) MEM_RM(dst->allocator, dst_node_page);
  if(dst_attr_page) MEM_RM(dst->allocator, dst_attr_page);
  svo_buffer_incore_clear(dst);
  goto exit;
}

res_T
svo_buffer_incore_copy_and_clear
  (struct svo_buffer_incore* dst, struct svo_buffer_incore* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);

  if(dst == src) return RES_OK;
  svo_buffer_incore_clear(dst);

  if(dst->allocator != src->allocator) {
    res = svo_buffer_incore_copy(dst, src);
    if(res != RES_OK) goto error;
  } else {
    res = darray_node_page_copy_and_clear(&dst->node_pages, &src->node_pages);
    if(res != RES_OK) goto error;
    res = darray_attr_page_copy_and_clear(&dst->attr_pages, &src->attr_pages);
    if(res != RES_OK) goto error;
    res = darray_attr_page_copy_and_clear(&dst->leaf_pages, &src->leaf_pages);
    if(res != RES_OK) goto error;
    dst->node_head = src->node_head;
    dst->leaf_head = src->leaf_head;
    dst->pagesize = src->pagesize;
    dst->page_nnodes = src->page_nnodes;
  }
  svo_buffer_incore_clear(src);
exit:
  return res;
error:
  svo_buffer_incore_clear(dst);
  goto exit;
}

res_T
svo_buffer_incore_copy_and_release
  (struct svo_buffer_incore* dst, struct svo_buffer_incore* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  if(dst == src) {
    svo_buffer_incore_release(dst);
  } else {
    res = svo_buffer_incore_copy_and_clear(dst, src);
    if(res == RES_OK) svo_buffer_incore_release(src);
  }
  return res;
}

res_T
svo_buffer_incore_alloc_nodes
  (struct svo_buffer_incore* buf,
   const size_t nnodes,
   struct svo_index* first_node,
   struct svo_xnode** nodes,
   struct svo_node_attrib** attribs)
{
  res_T res = RES_OK;
  ASSERT(buf && nnodes && first_node && nodes && attribs);

  if(nnodes > buf->page_nnodes) return RES_MEM_ERR;

  res = ensure_allocated_nodes(buf, nnodes);
  if(res != RES_OK) return res;

  *first_node = buf->node_head;
  buf->node_head.inode = (node_id_T)(buf->node_head.inode + nnodes);

  *nodes = svo_buffer_incore_get_node(buf, first_node);
  *attribs = svo_buffer_incore_get_node_attrib(buf, first_node);
  return RES_OK;
}

res_T
svo_buffer_incore_alloc_far_index
  (struct svo_buffer_incore* buf,
   struct svo_index* id,
   struct svo_index** far_index)
{
  size_t remaining_size;
  size_t skipped_nnodes;
  STATIC_ASSERT(sizeof(struct svo_index) >= sizeof(struct svo_xnode),
    Unexpected_type_size);
  ASSERT(buf && id);
  ASSERT(far_index);

  remaining_size = buf->page_nnodes - buf->node_head.inode;
  remaining_size = remaining_size * sizeof(struct svo_xnode);

  /* Not enough memory in the current page */
  if(sizeof(struct svo_index) > remaining_size) return RES_MEM_ERR;

  *id = buf->node_head;
  skipped_nnodes = sizeof(struct svo_index) / sizeof(struct svo_xnode);
  buf->node_head.inode = (node_id_T)(buf->node_head.inode + skipped_nnodes);

  *far_index = svo_buffer_incore_get_far_index(buf, id);
  return RES_OK;
}

res_T
svo_buffer_incore_alloc_leaf_attribs
  (struct svo_buffer_incore* buf,
   const size_t nleafs,
   struct svo_index* first_attrib,
   struct svo_node_attrib** attribs)
{
  res_T res = RES_OK;
  ASSERT(buf && nleafs && first_attrib && attribs);

  if(nleafs > buf->page_nnodes) return RES_MEM_ERR;

  res = ensure_allocated_leafs(buf, nleafs);
  if(res != RES_OK) return res;

  *first_attrib = buf->leaf_head;
  buf->leaf_head.inode = (node_id_T)(buf->leaf_head.inode + nleafs);

  *attribs = svo_buffer_incore_get_leaf_attrib(buf, first_attrib);
  return RES_OK;
}

void
svo_buffer_incore_clear(struct svo_buffer_incore* buf)
{
  uint16_t id;
  size_t i;
  ASSERT(buf);

  id = buf->node_head.ibuffer;
  ASSERT(id == buf->leaf_head.ibuffer);

  FOR_EACH(i, 0, darray_node_page_size_get(&buf->node_pages)) {
    MEM_RM(buf->allocator, darray_node_page_data_get(&buf->node_pages)[i]);
  }
  FOR_EACH(i, 0, darray_attr_page_size_get(&buf->attr_pages)) {
    MEM_RM(buf->allocator, darray_attr_page_data_get(&buf->attr_pages)[i]);
  }
  FOR_EACH(i, 0, darray_attr_page_size_get(&buf->leaf_pages)) {
    MEM_RM(buf->allocator, darray_attr_page_data_get(&buf->leaf_pages)[i]);
  }
  darray_node_page_purge(&buf->node_pages);
  darray_attr_page_purge(&buf->attr_pages);
  darray_attr_page_purge(&buf->leaf_pages);
  buf->node_head = SVO_INDEX_NULL;
  buf->leaf_head = SVO_INDEX_NULL;
  buf->node_head.ibuffer = id;
  buf->leaf_head.ibuffer = id;
}

