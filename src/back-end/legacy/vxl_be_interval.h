/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_INTERVAL_H
#define VXL_BE_INTERVAL_H

#include <rsys/float3.h>

struct ia3 { float lower[3], upper[3]; };

static FINLINE struct ia3*
ia3_set(struct ia3* dst, const float lower[3], const float upper[3])
{
  ASSERT(dst && lower && upper);
  f3_set(dst->lower, lower);
  f3_set(dst->upper, upper);
  return dst;
}

static FINLINE struct ia3*
ia3_set1(struct ia3* dst, const float lower, const float upper)
{
  ASSERT(dst);
  f3_splat(dst->lower, lower);
  f3_splat(dst->upper, upper);
  return dst;
}

static FINLINE struct ia3*
ia3_add(struct ia3* dst, const struct ia3* a, const struct ia3* b)
{
  ASSERT(dst && a && b);
  f3_add(dst->lower, a->lower, b->lower);
  f3_add(dst->upper, a->upper, b->upper);
  return dst;
}

static FINLINE struct ia3*
ia3_sub(struct ia3* dst, const struct ia3* a, const struct ia3* b)
{
  struct ia3 tmp;
  ASSERT(dst && a && b);
  f3_sub(tmp.lower, a->lower, b->upper);
  f3_sub(tmp.upper, a->upper, b->lower);
  *dst = tmp;
  return dst;
}

static FINLINE struct ia3*
ia3_mul(struct ia3* dst, const struct ia3* a, const struct ia3* b)
{
  float ll[3], lu[3], ul[3], uu[3];
  ASSERT(dst && a && b);

  f3_mul(ll, a->lower, b->lower);
  f3_mul(lu, a->lower, b->upper);
  f3_mul(ul, a->upper, b->lower);
  f3_mul(uu, a->upper, b->upper);

  f3_min(dst->lower, ll, lu);
  f3_min(dst->lower, dst->lower, ul);
  f3_min(dst->lower, dst->lower, uu);
  f3_max(dst->upper, ll, lu);
  f3_max(dst->upper, dst->upper, ul);
  f3_max(dst->upper, dst->upper, uu);
  return dst;
}


static FINLINE struct ia3*
ia3_rcp(struct ia3* dst, const struct ia3* a)
{
  struct ia3 tmp;
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, 3) {
    if(signf(a->lower[i]) != signf(a->upper[i])) {
      tmp.lower[i] = -(float)FLT_MAX;
      tmp.upper[i] =  (float)FLT_MAX;
    } else {
      tmp.lower[i] = 1.f / a->upper[i];
      tmp.upper[i] = 1.f / a->lower[i];
    }
  }
  *dst = tmp;
  return dst;
}

#endif /* VXL_BE_INTERVAL_H */
