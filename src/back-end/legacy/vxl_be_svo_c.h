/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_C_H
#define VXL_BE_SVO_C_H

#include "vxl_be.h"
#include "vxl_be_c.h"
#include "vxl_be_svo_buffer.h"

#include <rsys/ref_count.h>

#include <limits.h>

/* Generate the array of SVO buffers */
#define DARRAY_NAME buffer
#define DARRAY_DATA struct svo_buffer
#define DARRAY_FUNCTOR_INIT svo_buffer_init
#define DARRAY_FUNCTOR_RELEASE svo_buffer_release
#include <rsys/dynamic_array.h>

struct vxl_be_svo {
  struct darray_buffer buffers;
  struct svo_index root;

  float lower[3], upper[3]; /* World space AABB */
  size_t nvoxels; /* Total number of finest voxels */
  uint16_t definition; /* Par axis number of voxels */

  struct mem_allocator allocator; /* Internal allocator */

  struct vxl_be_device* dev;
  ref_T ref;
};

static FINLINE char
svo_is_empty(const struct vxl_be_svo* svo)
{
  ASSERT(svo);
  return (char)SVO_INDEX_EQ(&svo->root, &SVO_INDEX_NULL);
}

static FINLINE struct svo_xnode*
svo_get_node(struct vxl_be_svo* svo, const struct svo_index* id)
{
  struct svo_buffer* buf;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));
  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  return svo_buffer_get_node(buf, id);
}

static FINLINE struct svo_index*
svo_get_far_index(struct vxl_be_svo* svo, const struct svo_index* id)
{
  struct svo_buffer* buf;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));
  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  return svo_buffer_get_far_index(buf, id);
}

static FINLINE struct svo_node_attrib*
svo_get_node_attrib(struct vxl_be_svo* svo, const struct svo_index* id)
{
  struct svo_buffer* buf;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));
  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  return svo_buffer_get_node_attrib(buf, id);
}

static FINLINE struct svo_node_attrib*
svo_get_leaf_attrib(struct vxl_be_svo* svo, const struct svo_index* id)
{
  struct svo_buffer* buf;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));
  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  return svo_buffer_get_leaf_attrib(buf, id);
}

static FINLINE void
svo_get_child_index
  (struct vxl_be_svo* svo,
   const struct svo_index* id,
   const int ichild, /* in [0, 7] */
   struct svo_index* out_id)
{
  struct svo_buffer* buf;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));
  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  svo_buffer_get_child_index(buf, id, ichild, out_id);
}

static FINLINE struct svo_node_attrib*
svo_get_child_attrib
  (struct vxl_be_svo* svo,
   const struct svo_index* id,
   const int ichild)
{
  struct svo_buffer* buf;
  struct svo_xnode* node;
  struct svo_index iattr;
  ASSERT(id->ibuffer < darray_buffer_size_get(&svo->buffers));

  node = svo_get_node(svo, id);

  buf = darray_buffer_data_get(&svo->buffers) + id->ibuffer;
  svo_buffer_get_child_index(buf, id, ichild, &iattr);

  buf = darray_buffer_data_get(&svo->buffers) + iattr.ibuffer;
  return node->is_leaf
    ? svo_buffer_get_leaf_attrib(buf, &iattr)
    : svo_buffer_get_node_attrib(buf, &iattr);
}

/*******************************************************************************
 * Helper function
 ******************************************************************************/
/* The SVO ray traversal may stops at the same SVO level for all rays (e.g. no
 * differential ray and SVO packet tracing). In such case, the size of the
 * intersected voxel can be computed once with respect to the SVO size, its
 * overall definition and the submitted max LOD */
static INLINE float
compute_intersected_voxel_size(struct vxl_be_svo* svo, const uint32_t lod_max)
{
  int leaf_depth; /* Depth of a SVO leaf */
  float size;
  ASSERT(IS_POW2(svo->definition));

  leaf_depth = log2i(svo->definition);
  leaf_depth = (int)MMIN((uint32_t)leaf_depth, lod_max);

  size = svo->upper[0] - svo->lower[0]; /* Overall SVO size */
  /* Assume that the voxel is cubic */
  ASSERT(eq_epsf(size, svo->upper[1] - svo->lower[1], 1.e-3f));
  ASSERT(eq_epsf(size, svo->upper[2] - svo->lower[2], 1.e-3f));

  return size / (float)(1 << leaf_depth);
}

#endif /* VXL_BE_SVO_C_H */

