/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_be_svo_c.h"
#include "vxl_be_svo_buffer.h"
#include "vxl_be_svo_builder.h"
#include "vxl_be_voxel.h"

/*******************************************************************************
 * Local function
 ******************************************************************************/
#ifndef NDEBUG
static void /* Check the coherency of a sub-tree */
check_svo_xnode
  (struct vxl_be_svo* svo,
   const struct svo_index* id,
   const int depth)
{
  struct svo_xnode* xnode;
  int i;
  ASSERT(svo && id);

  xnode = svo_get_node(svo, id);

  if(depth == 0) {
    if(!xnode->is_leaf && xnode->is_leaf != xnode->is_valid)
      FATAL("Unexpected non-leaf node\n");
  } else {
    if(xnode->is_leaf)
      FATAL("Unexpected leaf node\n");
    FOR_EACH(i, 0, 8) {
      if(xnode->is_valid & BIT(i)) {
        struct svo_index child_id;
        svo_get_child_index(svo, id, i, &child_id);
        check_svo_xnode(svo, &child_id, depth - 1);
      }
    }
  }
}
#endif

/* Write the leaf attributes in the SVO buffer */
static INLINE res_T
leafs_write
  (const struct svo_node_attrib* leafs,
   const size_t nleafs,
   struct svo_buffer* buffer,
   struct svo_index* first_leaf) /* Index of the first written leaf */
{
  struct svo_node_attrib* attrs;
  size_t ileaf;
  res_T res;
  ASSERT(leafs && buffer && first_leaf && nleafs > 0 && nleafs <= 8);

  res = svo_buffer_alloc_leaf_attribs
    (buffer, (size_t)nleafs, first_leaf, &attrs);
  if(res != RES_OK) return res;

  FOR_EACH(ileaf, 0, nleafs) attrs[ileaf] = leafs[ileaf];
  return RES_OK;
}

/* Write the stacked non empty nodes in the SVO buffer */
static FINLINE res_T
stack_write
  (struct stack* stack,
   const int ilvl,
   struct vxl_be_svo* svo,
   struct svo_buffer* buffer,
   struct svo_index* out_index) /* May be NULL */
{
  struct svo_index node_id;
  int inode;
  res_T res = RES_OK;
  ASSERT(stack && svo && buffer && stack->mask && stack->len);
  (void)ilvl, (void)svo;

  do {
    struct svo_xnode* xnodes = NULL;
    struct svo_node_attrib* attribs = NULL;

    res = svo_buffer_alloc_nodes
      (buffer, (size_t)stack->len, &node_id, &xnodes, &attribs);
    if(res != RES_OK) goto error;

    /* Encode and store stacked nodes */
    FOR_EACH(inode, 0, stack->len) {
      const struct svo_node* node = stack->nodes + inode;
      size_t offset = SIZE_MAX;

      if(node->ichildren.ipage == node_id.ipage
      && node->ichildren.ibuffer == node_id.ibuffer) {
        offset = (node_id.inode + (size_t)inode) - node->ichildren.inode;
      }

      if(offset > SVO_XNODE_MAX_CHILDREN_OFFSET) {
        struct svo_index index;
        struct svo_index* far_id;

        /* Not enough memory in the current page. The far index cannot be
         * stored relatively to its associated node. Stop the write process and
         * rewrite the whole stacked nodes in a new page. */
        res = svo_buffer_alloc_far_index(buffer, &index, &far_id);
        if(res != RES_OK) break;

        *far_id = node->ichildren;
        offset = (index.inode - (node_id.inode + (size_t)inode));
        offset = offset | SVO_XNODE_FAR_INDEX;
      }

      ASSERT(!stack->nodes[inode].is_leaf || ilvl == 0);
      xnodes[inode].is_valid = stack->nodes[inode].is_valid;
      xnodes[inode].is_leaf = stack->nodes[inode].is_leaf;
      xnodes[inode].children_offset = (svo_offset_T)offset;
      attribs[inode].color[0] = stack->nodes[inode].color[0];
      attribs[inode].color[1] = stack->nodes[inode].color[1];
      attribs[inode].color[2] = stack->nodes[inode].color[2];
      xnormal_set(&attribs[inode].normal, stack->nodes[inode].normal);
#ifndef NDEBUG
      if(buffer->type == SVO_BUFFER_INCORE) { /* Does not work on OOC buffer */
        struct svo_index tmp_id = node_id;
        tmp_id.inode = (node_id_T)(tmp_id.inode + inode);
        check_svo_xnode(svo, &tmp_id, ilvl);
      }
#endif
    }
  } while(inode != stack->len);

  /* Return the node index */
  if(out_index) *out_index = node_id;

exit:
  return res;
error:
  goto exit;
}

/* Build a SVO node from the registered stack nodes */
static INLINE void
stack_setup_node(struct stack* stack, struct svo_node* node)
{
  uint32_t color[3] = { 0, 0, 0 };
  float normal[3] = { 0.f, 0.f, 0.f };
  uint32_t nnodes = 0;
  int ichild;
  ASSERT(stack && stack->mask != 0 && node);

  /* Setup the data of the parent node to register */
  node->ichildren = SVO_INDEX_NULL;
  node->is_valid = (uint8_t)stack->mask;
  node->is_leaf = 0; /* Leaf nodes are treated by the svo_build function */

  FOR_EACH(ichild, 0, 8) {
    if((stack->mask & BIT(ichild)) == 0) continue;

    /* Accumulate the attribs of the children node */
    color[0] += stack->nodes[nnodes].color[0];
    color[1] += stack->nodes[nnodes].color[1];
    color[2] += stack->nodes[nnodes].color[2];
    normal[0] += stack->nodes[nnodes].normal[0];
    normal[1] += stack->nodes[nnodes].normal[1];
    normal[2] += stack->nodes[nnodes].normal[2];
    ++nnodes;
  }

  /* Setup the parent attribs as the average of the children attribs */
  ASSERT(nnodes);
  node->color[0] = (unsigned char)(color[0] / nnodes);
  node->color[1] = (unsigned char)(color[1] / nnodes);
  node->color[2] = (unsigned char)(color[2] / nnodes);
  node->normal[0] = normal[0] / (float)nnodes;
  node->normal[1] = normal[1] / (float)nnodes;
  node->normal[2] = normal[2] / (float)nnodes;
  f3_normalize(node->normal, node->normal);
}

static FINLINE void
stack_clear(struct stack* stack)
{
  int i;
  ASSERT(stack);
  stack->mask = stack->len = 0;
  FOR_EACH(i, 0, 8) {
    stack->nodes[i].is_valid = 0;
    stack->nodes[i].is_leaf = 0;
  }
}

/*******************************************************************************
 * SVO builder functions
 ******************************************************************************/
void
svo_builder_init
  (struct svo_builder* bldr,
   struct vxl_be_svo* svo, /* SVO into which data are written */
   const size_t ibuffer, /* Id of the SVO buffer into which data are written */
   const size_t definition, /* May be != svo->definition */
   const uint64_t mcode) /* First morton code of the SVO */
{
  int ilvl;
  ASSERT(bldr && svo && IS_POW2(definition) && IS_POW2(svo->definition));
  memset(bldr, 0, sizeof(struct svo_builder));

  ASSERT(ibuffer < darray_buffer_size_get(&svo->buffers));
  bldr->buffer = darray_buffer_data_get(&svo->buffers)+ibuffer;

  bldr->svo = svo;
  bldr->svo_depth = log2i((int)definition);
  bldr->node = &bldr->stacks[0].nodes[0];
  bldr->mcode = mcode;
  ASSERT(bldr->svo_depth <= SVO_DEPTH_MAX);
  FOR_EACH(ilvl, 0, bldr->svo_depth) {
    stack_clear(&bldr->stacks[ilvl]);
  }
}

void
svo_builder_release(struct svo_builder* bldr)
{
  /* Do nothing */
  (void)bldr;
}

res_T
svo_builder_add_voxel(struct svo_builder* bldr, const struct voxel* vxl)
{
  uint64_t mcode_xor;
  int ilvl;
  uint8_t ichild;
  res_T res = RES_OK;
  ASSERT(bldr && vxl && vxl->mcode >= bldr->mcode);

  /* Define if the bits in [4 .. 63] of the previous and the next Morton
   * codes are the same */
  mcode_xor = bldr->mcode ^ vxl->mcode;

  /* The next voxel is not in the current node => Register the node if it
   * contains voxels */
  if(mcode_xor >= 8 && bldr->nnode_vxls != 0) {

    /* Write the leaf attributes */
    res = leafs_write
      (bldr->leafs, bldr->nnode_vxls, bldr->buffer, &bldr->node->ichildren);
    if(res != RES_OK) goto error;

    /* Define the node attribs as the average of the children attribs */
    bldr->node->color[0] = (uint8_t)(bldr->color[0] / bldr->nnode_vxls);
    bldr->node->color[1] = (uint8_t)(bldr->color[1] / bldr->nnode_vxls);
    bldr->node->color[2] = (uint8_t)(bldr->color[2] / bldr->nnode_vxls);
    bldr->node->normal[0] = bldr->normal[0] / (float)bldr->nnode_vxls;
    bldr->node->normal[1] = bldr->normal[1] / (float)bldr->nnode_vxls;
    bldr->node->normal[2] = bldr->normal[2] / (float)bldr->nnode_vxls;
    f3_normalize(bldr->node->normal, bldr->node->normal);

    bldr->node->is_leaf = bldr->node->is_valid;
    bldr->stacks[0].mask |= 1 << ((bldr->mcode >> 3) & 7);
    bldr->stacks[0].len += 1;
    ASSERT(bldr->stacks[0].len <= 8);

    if(bldr->stacks[0].len >= 8 || mcode_xor >= 64) {
      bldr->node = &bldr->stacks[0].nodes[0];
    } else {
      bldr->node = &bldr->stacks[0].nodes[bldr->stacks[0].len];
    }

    /* Reset data */
    bldr->nnode_vxls = 0;
    bldr->color[0] = bldr->color[1] = bldr->color[2] = 0;
    bldr->normal[0] = bldr->normal[1] = bldr->normal[2] = 0;
  }

  /* Flush the stack of the SVO level that does not contain the next voxel.
   * The last SVO level is actually the level that contains the root node
   * while the penultimate describes the root node itself. These 2 levels
   * contain all the voxels and can be skipped */
  FOR_EACH(ilvl, 0, bldr->svo_depth-2/*The 2 last leaves contain all voxels*/) {
    struct svo_node* stack_node;
    uint64_t mcode_max_lvl;

    /* Compute the maximum morton code value for the current SVO level */
    mcode_max_lvl = 8lu/*#children*/ * (1lu<<(3*(ilvl+1)))/*#vxls per children*/;

    if(mcode_xor < mcode_max_lvl) break;

    if(bldr->stacks[ilvl].mask == 0) continue; /* Discard empty stack */

    /* The next voxel is not in the i^th stack. Setup the parent node of the
     * nodes registered into the stack */
    stack_node = &bldr->stacks[ilvl+1].nodes[bldr->stacks[ilvl+1].len];
    stack_setup_node(&bldr->stacks[ilvl], stack_node);
    bldr->stacks[ilvl+1].mask |= 1 << ((bldr->mcode >> (3*(ilvl+2))) & 7);
    bldr->stacks[ilvl+1].len += 1;
    ASSERT(bldr->stacks[ilvl+1].len <= 8);

    /* Write the nodes of the stack of the current SVO level into the SVO */
    res = stack_write(&bldr->stacks[ilvl], ilvl, bldr->svo, bldr->buffer,
      &stack_node->ichildren);
    if(res != RES_OK) goto error;

    /* Reset the current stack */
    stack_clear(&bldr->stacks[ilvl]);
  }

  bldr->mcode = vxl->mcode;
  ichild = bldr->mcode & 0x7;
  bldr->node->is_valid |= (uint8_t)(bldr->node->is_valid | (1 << ichild));

  /* Setup the leaf attributes */
  bldr->leafs[bldr->nnode_vxls].color[0] = vxl->col[0];
  bldr->leafs[bldr->nnode_vxls].color[1] = vxl->col[1];
  bldr->leafs[bldr->nnode_vxls].color[2] = vxl->col[2];
  xnormal_set(&bldr->leafs[bldr->nnode_vxls].normal, vxl->normal);

  /* Accumulate the attribs of the children node */
  bldr->color[0] += vxl->col[0];
  bldr->color[1] += vxl->col[1];
  bldr->color[2] += vxl->col[2];
  bldr->normal[0] += vxl->normal[0];
  bldr->normal[1] += vxl->normal[1];
  bldr->normal[2] += vxl->normal[2];

  ++bldr->nnode_vxls;
  ++bldr->nvxls;

exit:
  return res;
error:
  goto exit;
}

res_T
svo_builder_finalize
  (struct svo_builder* bldr,
   const int write_root_node,
   struct svo_index* root_index, /* Set if write_root_node. May be NULL */
   struct svo_node* root_node) /* May be NULL */
{
  res_T res = RES_OK;
  int ilvl;
  ASSERT(bldr);

  if(!bldr->nvxls) goto exit;

  /* Register the current tracked leaf node into the stack */
  if(bldr->nnode_vxls) {
    /* Write the leaf attributes */
    res = leafs_write
      (bldr->leafs, bldr->nnode_vxls, bldr->buffer, &bldr->node->ichildren);
    if(res != RES_OK) goto error;

    /* Define the node attribs as the average of the children attribs */
    bldr->node->color[0] = (uint8_t)(bldr->color[0] / bldr->nnode_vxls);
    bldr->node->color[1] = (uint8_t)(bldr->color[1] / bldr->nnode_vxls);
    bldr->node->color[2] = (uint8_t)(bldr->color[2] / bldr->nnode_vxls);
    bldr->node->normal[0] = bldr->normal[0] / (float)bldr->nnode_vxls;
    bldr->node->normal[1] = bldr->normal[1] / (float)bldr->nnode_vxls;
    bldr->node->normal[2] = bldr->normal[2] / (float)bldr->nnode_vxls;
    f3_normalize(bldr->node->normal, bldr->node->normal);

    bldr->node->is_leaf = bldr->node->is_valid;
    bldr->stacks[0].mask |= 1 << ((bldr->mcode >> 3) & 7);
    bldr->stacks[0].len += 1;
    ASSERT(bldr->stacks[0].len <= 8);
  }

  /* Flush the stacked nodes, exepted the root level */
  FOR_EACH(ilvl, 0, bldr->svo_depth-1) {
    struct svo_node* stack_node = NULL;

    if(bldr->stacks[ilvl].mask == 0) continue;

    /* Setup the parent node of the nodes registered into the current stack
     * level. Use the Morton code of the last voxel to define the children mask
     * of the parent node. */
    stack_node = &bldr->stacks[ilvl+1].nodes[bldr->stacks[ilvl+1].len];
    stack_setup_node(&bldr->stacks[ilvl], stack_node);
    bldr->stacks[ilvl+1].mask |= 1 << ((bldr->mcode >> (3*(ilvl+2))) & 7);
    bldr->stacks[ilvl+1].len += 1;

    /* Write the nodes of the stack of the current SVO level into the SVO */
    res = stack_write(&bldr->stacks[ilvl], ilvl, bldr->svo, bldr->buffer,
      &stack_node->ichildren);
    if(res != RES_OK) goto error;
  }

  if(root_node) {
    /* Return the root node */
    ASSERT(bldr->stacks[bldr->svo_depth-1].len == 1);
    *root_node = bldr->stacks[bldr->svo_depth-1].nodes[0];
  }

  if(write_root_node) {
    /* Write the stack of the root level */
    res = stack_write(&bldr->stacks[bldr->svo_depth-1], bldr->svo_depth-1,
      bldr->svo, bldr->buffer, root_index);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * High Level SVO builder
 ******************************************************************************/
void
hl_svo_builder_init
  (struct hl_svo_builder* bldr,
   struct vxl_be_svo* svo,
   const size_t ibuffer,
   const size_t definition, /* != svo->definition */
   const int ibase_lvl)
{
  int ilvl;
  ASSERT(bldr && svo && IS_POW2(definition) && IS_POW2(svo->definition));
  ASSERT(ibuffer < darray_buffer_size_get(&svo->buffers));
  memset(bldr, 0, sizeof(struct hl_svo_builder));

  bldr->svo_depth = log2i((int)definition);
  bldr->ibase_lvl = ibase_lvl;
  bldr->svo = svo;
  bldr->buffer = darray_buffer_data_get(&svo->buffers)+ibuffer;
  bldr->mcode = 0;
  ASSERT(bldr->svo_depth <= SVO_DEPTH_MAX);
  FOR_EACH(ilvl, 0, bldr->svo_depth) {
    stack_clear(&bldr->stacks[ilvl]);
  }
}

void
hl_svo_builder_release(struct hl_svo_builder* bldr)
{
  /* Do nothing */
  (void)bldr;
}

res_T
hl_svo_builder_add_node
  (struct hl_svo_builder* bldr,
   const struct svo_node* node,
   const uint64_t mcode) /* Morton code of the node */
{
  uint64_t mcode_xor;
  int ilvl;
  uint8_t ichild;
  res_T res = RES_OK;
  ASSERT(bldr && mcode >= bldr->mcode);

  mcode_xor = bldr->mcode ^ mcode;

  /* Flush the stack of the SVO level that does not contain the next sub SVO.
   * The last SVO level is actually the level that contains the global root
   * node. This level contain all the voxels and can be skipped */
  FOR_EACH(ilvl, 0, bldr->svo_depth-1/*The last level contain all sub SVOs */) {
    struct svo_node* stack_node;
    uint64_t mcode_max_lvl;

    /* Compute the maximum morton code value for the current SVO level */
    mcode_max_lvl = 8lu/*#sub SVOs*/ * (1lu<<(3*ilvl))/*#sub SVOs per children*/;

    if(mcode_xor < mcode_max_lvl) break;

    if(bldr->stacks[ilvl].mask == 0) continue; /* Discard empty stack */

    /* The next voxel is not in the i^th stack. Setup the parent node of the
     * nodes registered into the stack */
    stack_node = &bldr->stacks[ilvl+1].nodes[bldr->stacks[ilvl+1].len];
    stack_setup_node(&bldr->stacks[ilvl], stack_node);
    bldr->stacks[ilvl+1].mask |= 1 << ((bldr->mcode >> (3*(ilvl+1))) & 7);
    bldr->stacks[ilvl+1].len += 1;
    ASSERT(bldr->stacks[ilvl+1].len <= 8);

    /* Write the nodes of the stack of the current SVO level into the SVO */
    res = stack_write(&bldr->stacks[ilvl], bldr->ibase_lvl+ilvl, bldr->svo,
      bldr->buffer, &stack_node->ichildren);
    if(res != RES_OK) goto error;

    /* Reset the current stack */
    stack_clear(&bldr->stacks[ilvl]);
  }

  ichild = mcode & 0x7;
  bldr->stacks[0].nodes[bldr->stacks[0].len] = *node;
  bldr->stacks[0].mask |= 1 << ichild;
  bldr->stacks[0].len += 1;
  ASSERT(bldr->stacks[0].len <= 8);

  bldr->mcode = mcode;

exit:
  return res;
error:
  goto exit;
}

res_T
hl_svo_builder_finalize(struct hl_svo_builder* bldr)
{
  int ilvl;
  res_T res = RES_OK;
  ASSERT(bldr);

  /* Flush the stacked nodes */
  FOR_EACH(ilvl, 0, bldr->svo_depth) {
    struct svo_node* stack_node = NULL;

    if(bldr->stacks[ilvl].mask == 0) continue;

    /* Setup the parent node of the nodes registered into the current stack
     * level. Use the Morton code of the last voxel to define the children mask
     * of the parent node. */
    stack_node = &bldr->stacks[ilvl+1].nodes[bldr->stacks[ilvl+1].len];
    stack_setup_node(&bldr->stacks[ilvl], stack_node);
    bldr->stacks[ilvl+1].mask |= 1 << ((bldr->mcode >> (3*(ilvl+1))) & 7);
    bldr->stacks[ilvl+1].len += 1;

    /* Write the nodes of the stack of the current SVO level into the SVO */
    res = stack_write(&bldr->stacks[ilvl], bldr->ibase_lvl+ilvl, bldr->svo,
      bldr->buffer, &stack_node->ichildren);
    if(res != RES_OK) goto error;
  }

  /* Write the stack entry of the root level */
  ASSERT(bldr->stacks[bldr->svo_depth].len == 1);
  res = stack_write(&bldr->stacks[bldr->svo_depth],
    bldr->ibase_lvl+bldr->svo_depth, bldr->svo, bldr->buffer,
    &bldr->svo->root);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

