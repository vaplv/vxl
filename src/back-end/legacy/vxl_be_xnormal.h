/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_XNORMAL_H
#define VXL_BE_XNORMAL_H

#include <rsys/float3.h>

/*
 * The normal are projected on a cube centered in 0 of size 2. The intersected
 * face is encoded with the `axis' and the `sign' fields of the xnormal
 * structure. `axis' defines the axis orthogonal to the intersected face. Its
 * value is 0, 1 or 2 if the intersected face is orthogonal to the X, Y or Z
 * axis, respectively. `sign' defines which of the lower or the upper cube side
 * is intersected. Its value is 0 if the normal intersects the upper bound and
 * 1 otherwise. The coordinates of the intersection of the normal with the cube
 * face is encoded with the `u' and `v' parameter.
 */

/* Normal encoded on 32 bits */
struct xnormal {
  uint32_t sign: 1;
  uint32_t axis: 2;
  uint32_t u: 15;
  uint32_t v: 14;
};
#define XNORMAL_NULL__ {0}
static const struct xnormal XNORMAL_NULL = XNORMAL_NULL__;

static FINLINE void
xnormal_set(struct xnormal* xN, const float N[3])
{
  float absN[3];
  float u, v;
  float sign;
  uint32_t axis[3];
  ASSERT(xN && N);

  absN[0] = absf(N[0]);
  absN[1] = absf(N[1]);
  absN[2] = absf(N[2]);

  axis[0] = absN[0] > absN[1]
    ? (absN[0] > absN[2] ? 0 : 2)
    : (absN[1] > absN[2] ? 1 : 2);
  axis[1] = axis[0] == 2 ? 0 : axis[0] + 1;
  axis[2] = axis[1] == 2 ? 0 : axis[1] + 1;

  sign = signf(N[axis[0]]);
  xN->axis = (uint32_t)(axis[0] & ((1u<<2)-1));
  xN->sign = sign < 0;

  u = sign * N[axis[1]] / N[axis[0]]; /* Project u on the cube face */
  u = u * 0.5f + 0.5f; /* Map u in [0, 1] */
  u = u * (float)((1<<15)-1); /* Map u in [0, 2^15[ */
  xN->u = (uint32_t)(u + 0.5/*round*/) & ((1u<<15)-1);

  v = sign * N[axis[2]] / N[axis[0]]; /* Project v on the cube face */
  v = v * 0.5f + 0.5f; /* Map v in [0, 1] */
  v = v * (float)((1<<14)-1); /* Map v in [0, 2^14[ */
  xN->v = (uint32_t)(v + 0.5/*round*/) & ((1u<<14)-1);
}

static FINLINE void
xnormal_get(const struct xnormal* xN, float N[3])
{
  uint32_t axis[3];
  float u, v;
  ASSERT(xN && N);

  axis[0] = xN->axis;
  axis[1] = axis[0] == 2 ? 0 : axis[0] + 1;
  axis[2] = axis[1] == 2 ? 0 : axis[1] + 1;

  u = (float)xN->u / (float)((1u<<15) - 1); /* Map in [0, 1] */
  u = u * 2.0f - 1.0f; /* Map in [-1, 1] */

  v = (float)xN->v / (float)((1u<<14) - 1); /* Map in [0, 1] */
  v = v * 2.0f - 1.0f; /* Map in [-1, 1] */

  N[axis[0]] = xN->sign ? -1 : 1;
  N[axis[1]] = u;
  N[axis[2]] = v;
  f3_normalize(N, N);
}

#endif /* VXL_BE_QNORMAL_H */

