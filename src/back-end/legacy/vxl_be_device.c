/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxl_c.h"
#include "vxl_be_device_c.h"

#include <rsys/mem_allocator.h>

#include <omp.h>
#include <string.h>
#include <unistd.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
be_device_release(ref_T* ref)
{
  struct vxl_be_device* dev = NULL;
  ASSERT(ref);
  dev = CONTAINER_OF(ref, struct vxl_be_device, ref);
  MEM_RM(dev->allocator, dev);
}

/*******************************************************************************
 * Backend public functions
 ******************************************************************************/
VXL_DECLARE_REF_FUNCS(be_device)

res_T
vxl_be_device_create
  (struct mem_allocator* mem_allocator,
   const unsigned nthreads_hint,
   struct vxl_be_device** out_dev)
{
  struct vxl_be_device* dev = NULL;
  struct mem_allocator* allocator = NULL;
  long pagesize;
  res_T res = RES_OK;

  if(!out_dev || !nthreads_hint) {
    res = RES_BAD_ARG;
    goto error;
  }
  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  dev = MEM_CALLOC(allocator, 1, sizeof(struct vxl_be_device));
  if(!dev) {
    res = RES_MEM_ERR;
    goto error;
  }
  dev->allocator = allocator;
  ref_init(&dev->ref);
  dev->nthreads = MMIN(nthreads_hint, (unsigned)omp_get_num_procs());

  pagesize = sysconf(_SC_PAGESIZE);
  if(pagesize <= 0) {
    res = RES_UNKNOWN_ERR;
    goto error;
  }
  dev->pagesize = (size_t)pagesize;

exit:
  if(out_dev) *out_dev = dev;
  return res;
error:
  if(dev) {
    VXL(be_device_ref_put(dev));
    dev = NULL;
  }
  goto exit;
}

res_T
vxl_be_device_get_nthreads(const struct vxl_be_device* dev, unsigned* nthreads)
{
  if(!dev || !nthreads) return RES_BAD_ARG;
  *nthreads = dev->nthreads;
  return RES_OK;
}

