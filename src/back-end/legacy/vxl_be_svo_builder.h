/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_BUILDER_H
#define VXL_BE_SVO_BUILDER_H

#include <rsys/rsys.h>

#define SVO_DEPTH_MAX 16 /* Maximum depth of a SVO */

/* Forward declaration */
struct voxel;
struct svo_buffer;

enum svo_memory_layout {
  SVO_IN_CORE,
  SVO_OUT_OF_CORE
};

/* Raw SVO node */
struct svo_node {
  struct svo_index ichildren; /* Id toward the 1st child */
  uint8_t is_valid; /* Mask defining whether the children are valid or not */
  uint8_t is_leaf; /* Mask defining whether the children are leafs or not */
  float normal[3];
  unsigned char color[3];
};

/* Stack registering the nodes of a SVO level */
struct stack {
  int mask; /* Mask of valid children nodes (0 = empty) */
  int len; /* Number of valid children nodes in [0, 8) */
  struct svo_node nodes[8]; /* List of registered children nodes */
};

/*******************************************************************************
 * SVO builder. It stores the build context and incrementally build the SVO
 * from submitted voxels. The voxels must be submitted wrt their morton order.
 ******************************************************************************/
struct svo_builder {
  struct svo_node_attrib leafs[8]; /* Stacked leaf attributes */
  struct stack stacks[SVO_DEPTH_MAX]; /* Stacked nodes */
  struct svo_node* node; /* Current node */
  int svo_depth; /* Max depth of the SVO */

  struct vxl_be_svo* svo; /* SVO to which the buffer belongs to */
  struct svo_buffer* buffer; /* SVO Buffer to store data */

  uint32_t color[3]; /* Accumulated node color */
  float normal[3]; /* Accumulated node normal */

  uint64_t mcode; /* Last registered morton code */
  size_t nnode_vxls; /* #vxls in the current node */
  size_t nvxls; /* Overall number of registered voxels */
};

extern LOCAL_SYM void
svo_builder_init
  (struct svo_builder* bldr,
   struct vxl_be_svo* svo, /* SVO into which data are written */
   const size_t ibuffer, /* Id of the SVO buffer into which data are written */
   const size_t definition, /* May be != svo->definition */
   const uint64_t mcode); /* First morton code of the SVO */

extern LOCAL_SYM void
svo_builder_release
  (struct svo_builder* bldr);

extern LOCAL_SYM res_T
svo_builder_add_voxel
  (struct svo_builder* bldr,
   const struct voxel* vxl);

extern LOCAL_SYM res_T
svo_builder_finalize
  (struct svo_builder* bldr,
   const int write_root_node, /* If the root node must be registered ? */
   struct svo_index* root_index, /* Set if write_root_node is true. May be NULL */
   struct svo_node* root_node); /* May be NULL */

/*******************************************************************************
 * High level SVO builder. It stores the build context to incrementally build a
 * SVO whose leafs are svo_nodes and not voxels. The nodes must be submitted wrt
 * their morton order.
 ******************************************************************************/
struct hl_svo_builder {
  struct stack stacks[SVO_DEPTH_MAX]; /* Stack nodes */
  int svo_depth; /* Max depth of the High Level svo */
  int ibase_lvl; /* Id of the fines SVO level <=> SVO level offset */

  struct vxl_be_svo* svo; /* SVO to which the buffer belongs to */
  struct svo_buffer* buffer; /* Buffer into which the data are written */
  uint64_t mcode; /* Last registered morton code */
};

extern LOCAL_SYM void
hl_svo_builder_init
  (struct hl_svo_builder* bldr,
   struct vxl_be_svo* svo,
   const size_t ibuffer,
   const size_t definition, /* != svo->definition */
   const int ibase_lvl);

extern LOCAL_SYM void
hl_svo_builder_release
  (struct hl_svo_builder* bldr);

extern LOCAL_SYM res_T
hl_svo_builder_add_node
  (struct hl_svo_builder* bldr,
   const struct svo_node* node,
   const uint64_t mcode);

extern LOCAL_SYM res_T
hl_svo_builder_finalize
  (struct hl_svo_builder* bldr);

#endif /* VXL_BE_SVO_BUILDER_H */

