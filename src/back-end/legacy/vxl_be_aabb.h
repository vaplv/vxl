/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_AABB_H
#define VXL_BE_AABB_H

#include <rsys/float3.h>
#include <float.h>

struct aabb { float lower[3], upper[3]; }; /* Axis Aligned Bounding Box */

static FINLINE struct aabb*
aabb_reset(struct aabb* aabb)
{
  ASSERT(aabb);
  f3_splat(aabb->lower, FLT_MAX);
  f3_splat(aabb->upper,-FLT_MAX);
  return aabb;
}

static FINLINE struct aabb*
aabb_add_point(struct aabb* dst, const struct aabb* aabb, const float pt[3])
{
  ASSERT(aabb && pt);
  f3_min(dst->lower, aabb->lower, pt);
  f3_max(dst->upper, aabb->upper, pt);
  return dst;
}

static FINLINE struct aabb*
aabb_add_aabb(struct aabb* dst, const struct aabb* a, const struct aabb* b)
{
  ASSERT(dst && a && b);
  f3_min(dst->lower, a->lower, b->lower);
  f3_max(dst->upper, a->upper, b->upper);
  return dst;
}

static FINLINE float
aabb_half_area(const struct aabb* aabb)
{
  float size[3];
  ASSERT(aabb);
  f3_sub(size, aabb->upper, aabb->lower);
  return size[0]*(size[1] + size[2]) + size[1]*size[2];
}

static FINLINE char
aabb_holds_point(const struct aabb* aabb, const float pt[3])
{
  int i;
  ASSERT(aabb && pt);
  FOR_EACH(i, 0, 3) {
    if(pt[i] < aabb->lower[i] || pt[i] > aabb->upper[i])
      return 0;
  }
  return 1;
}

static FINLINE char
aabb_holds_aabb(const struct aabb* aabb0, const struct aabb* aabb1)
{
  int i;
  ASSERT(aabb0 && aabb1);
  FOR_EACH(i, 0, 3) {
    if(aabb1->lower[i] < aabb0->lower[i] || aabb1->upper[i] > aabb0->upper[i])
      return 0;
  }
  return 1;
}

static FINLINE struct aabb*
aabb_translate(struct aabb* dst, const struct aabb* aabb, const float offset[3])
{
  ASSERT(dst && aabb && offset);
  f3_add(dst->lower, aabb->lower, offset);
  f3_add(dst->upper, aabb->upper, offset);
  return dst;
}

#endif /* VXL_BE_AABB_H */
