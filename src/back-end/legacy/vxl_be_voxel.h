/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_VOXEL_C_H
#define VXL_BE_VOXEL_C_H

#include <rsys/dynamic_array.h>
#include <rsys/hash_table.h>

/* Raw voxel data */
struct voxel {
  uint64_t mcode; /* Morton code of the voxel */
  float normal[3]; /* Normal of the voxel */
  unsigned char col[3]; /* RGB color of the voxel */
};

/* Declare the dynamic array of voxels */
#define DARRAY_NAME voxel
#define DARRAY_DATA struct voxel
#include <rsys/dynamic_array.h>

/* Declare the dynamic array of voxel sets */
#define DARRAY_NAME voxels_list
#define DARRAY_DATA struct darray_voxel
#define DARRAY_FUNCTOR_INIT darray_voxel_init
#define DARRAY_FUNCTOR_RELEASE darray_voxel_release
#define DARRAY_FUNCTOR_COPY darray_voxel_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_voxel_copy_and_release
#include <rsys/dynamic_array.h>

/* Used to accumulate the per voxel attribs */
struct voxacc {
  uint64_t mcode; /* Morton code of the voxel. Use to sort voxels */
  float normal[3]; /* Accumulated normal */
  uint32_t naccums; /* Accumulation count */
  uint16_t col[3]; /* Accumulated color */
};

/* Declare the htable_voxacc struct which map a voxel morton code to its
 * accumulated data */
#define HTABLE_NAME voxacc
#define HTABLE_DATA size_t
#define HTABLE_KEY uint64_t
#include <rsys/hash_table.h>

/* List of voxels data to accumulate */
#define DARRAY_NAME voxacc
#define DARRAY_DATA struct voxacc
#include <rsys/dynamic_array.h>

/* List of voxacc dynamic arrays */
#define DARRAY_NAME voxaccs_list
#define DARRAY_DATA struct darray_voxacc
#define DARRAY_FUNCTOR_INIT darray_voxacc_init
#define DARRAY_FUNCTOR_RELEASE darray_voxacc_release
#define DARRAY_FUNCTOR_COPY darray_voxacc_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_voxacc_copy_and_release
#include <rsys/dynamic_array.h>

/* List of voxacc hash table */
#define DARRAY_NAME voxaccs_htbl
#define DARRAY_DATA struct htable_voxacc
#define DARRAY_FUNCTOR_INIT htable_voxacc_init
#define DARRAY_FUNCTOR_RELEASE htable_voxacc_release
#define DARRAY_FUNCTOR_COPY htable_voxacc_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE htable_voxacc_copy_and_release
#include <rsys/dynamic_array.h>

/* Normalize and sort the voxel attributes of `voxacc' into `voxels' */
extern LOCAL_SYM res_T
flush_accumulated_voxels
  (const struct voxacc* voxaccs, /* Per voxel accumulated attribs */
   const size_t nvxls, /* #voxels to flush */
   const size_t bin_def, /* Definition of a bin along the 3 axis */
   struct darray_voxel* scratch, /* Temp array to radix sort the voxels */
   struct darray_voxel* voxels); /* Output list of Morton-sorted voxels */

#endif /* VXL_BE_VOXEL_C_H */

