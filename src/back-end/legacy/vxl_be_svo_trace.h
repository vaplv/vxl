/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_TRACE_H
#define VXL_BE_SVO_TRACE_H

#include "vxl.h"

struct vxl_be_svo;
struct svo_index;

struct svo_ray {
  float org[3];
  float range[2];
  float ts[3]; /* 1 / - abs(dir) */
  float footprint; /* < 0 <=> No footprint */
  uint32_t octant_mask;
};

/*******************************************************************************
 * SVO trace functions
 ******************************************************************************/
extern LOCAL_SYM res_T
svo_setup_ray
  (const struct vxl_be_svo* svo,
   const float org[3],
   const float dir[3],
   const float dir_dx[3], /* May be NULL <=> no differential ray */
   const float dir_dy[3], /* May be NULL <=> no differential ray */
   const float range[3],
   struct svo_ray* ray);

extern LOCAL_SYM res_T
svo_trace_ray
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const float org[3],
   const float dir[3],
   const float dir_dx[3], /* May be NULL <=> no differential ray tracing */
   const float dir_dy[3], /* May be NULL <=> no differential ray tracing */
   const float range[2],
   struct vxl_hit* hit);

extern LOCAL_SYM res_T
svo_node_trace_ray
  (struct vxl_be_svo*  svo,
   const struct svo_index* inode,
   const float node_lower[3], /* Node lower bound in [1, 2] space */
   const float node_size, /* Node size in [1, 2] space */
   const uint32_t lod_range[2],
   const struct svo_ray* ray,
   struct vxl_hit* hit);

extern LOCAL_SYM res_T
svo_trace_rays
  (struct vxl_be_svo* svo,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float* orgs,
   const float* dirs,
   const float* dirs_dx, /* May be NULL <=> no differential ray tracing */
   const float* dirs_dy, /* May be NULL <=> no differential ray tracing */
   const float* ranges,
   struct vxl_hit* hits);

extern LOCAL_SYM res_T
svo_trace_packets
  (struct vxl_be_svo* svo,
   const enum vxl_rt_coherency coherency,
   const uint32_t lod_range[2],
   const size_t nrays,
   const int mask,
   const float org[3], /* Single origin */
   const float* dirs,
   const float* ranges,
   struct vxl_hit* hits);

#endif /* VXL_BE_SVO_TRACE_H */

