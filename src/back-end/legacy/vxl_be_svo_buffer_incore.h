/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_BUFFER_INCORE_H
#define VXL_BE_SVO_BUFFER_INCORE_H

#include "vxl_be_c.h"
#include "vxl_be_xnormal.h"

#include <rsys/dynamic_array.h>
#include <rsys/mem_allocator.h>

/*
 * Incore buffer of SVO data partitioned in fixed sized memory pages.
 *
 * The nodes are stored in pages whose memory size is defined at the setup of
 * the buffer. The node children are indexed relatively to their parent node
 * excepted if the relative offset is too high regarding the encoding precision
 * of the offset, or if the children are stored in another page. In such cases,
 * the node relatively references a 'svo_index', allocated in the same page,
 * that defines the absolute position of the children in the buffer.
 *
 * The node attributes are arranged following the same layout of the node data
 * and are thus accessed with the same 'svo_index'. Leaf attributes are stored
 * in a separate buffer and are indexed either relatively, or absolutely by the
 * node storing leafs, following the same aforementioned indexing procedure of
 * the node children.
 */

#define SVO_XNODE_FAR_INDEX (1u<<31)
#define SVO_XNODE_MAX_CHILDREN_OFFSET ((1u<<31)-1)
#define SVO_XNODE_MASK 0x7FFFFFFFu

typedef uint32_t svo_offset_T;

/* "Compressed" SVO node */
struct svo_xnode {
  /* Relative offset to retrieve the node children. If the SVO_XNODE_FAR_INDEX
   * bit is not set, the node children are stored `children_offset' nodes
   * *before* the node. If SVO_XNODE_FAR_INDEX is set, the index toward the
   * children are stored N bytes *after* the node with N defined as :
   *    N = (children_offset & SVO_XNODE_MASK)*sizeof(struct svo_xnode) */
  svo_offset_T children_offset;
  uint8_t is_valid; /* Mask defining if the children are valid */
  uint8_t is_leaf; /* Mask defining if the children are leafs */
  uint16_t dummy; /* Ensure that the node is encoded on 64 bits */
};

#define SVO_XNODE_NULL__ {0, 0, 0, 0}
static const struct svo_xnode SVO_XNODE_NULL = SVO_XNODE_NULL__;

/* Per node attribute */
struct svo_node_attrib {
  struct xnormal normal;
  unsigned char color[3];
  char dummy; /* Ensure that the node attrib is encoded on 64 bits */
};

STATIC_ASSERT
  (sizeof(struct svo_xnode)==sizeof(struct svo_node_attrib),
   Unexpected_size_of_SVO_node_and_attribs);

#define SVO_NODE_ATTRIB_NULL__ {XNORMAL_NULL__, {0,0,0}, 0}
static const struct svo_node_attrib SVO_NODE_ATTRIB_NULL =
  SVO_NODE_ATTRIB_NULL__;

#define SVO_INDEX_INODE_MAX UINT16_MAX
#define SVO_INDEX_IPAGE_MAX UINT32_MAX
#define SVO_INDEX_IBUFFER_MAX UINT16_MAX
typedef uint16_t node_id_T;
typedef uint32_t page_id_T;
typedef uint16_t buffer_id_T;

/* Absolute reference toward a node data */
struct svo_index {
  page_id_T ipage; /* Identifier of the page in the buffer */
  node_id_T inode; /* Identifier of the node in the buffer page */
  buffer_id_T ibuffer; /* Buffer identifier */
};
STATIC_ASSERT
  (  sizeof(struct svo_index) == sizeof(struct svo_xnode)
  && sizeof(struct svo_index) == 8, Unexpected_sizeof_svo_index);

#define SVO_INDEX_NULL__ {                                                     \
  SVO_INDEX_IPAGE_MAX,                                                         \
  SVO_INDEX_INODE_MAX,                                                         \
  SVO_INDEX_IBUFFER_MAX                                                        \
}
static const struct svo_index SVO_INDEX_NULL = SVO_INDEX_NULL__;

/* Helper macro */
#define SVO_INDEX_EQ(A, B) ((A)->inode==(B)->inode && (A)->ipage==(B)->ipage)

/* Define a list of node pages */
#define DARRAY_NAME node_page
#define DARRAY_DATA char*
#include <rsys/dynamic_array.h>

/* Define a list of node attribute pages */
#define DARRAY_NAME attr_page
#define DARRAY_DATA struct svo_node_attrib*
#include <rsys/dynamic_array.h>

struct svo_buffer_incore {
  size_t pagesize; /* Memory page size in bytes */

  /* Number of per page nodes. Note that since the size of svo_xnode and
   * svo_node_attrib are the same there is the same number of per page attribs
   * than the number of per page nodes. */
  size_t page_nnodes;

  struct darray_node_page node_pages; /* Pages of node data */
  struct darray_attr_page attr_pages; /* Pages of node attributes */
  struct darray_attr_page leaf_pages; /* Pages of leaf attributes */
  struct svo_index node_head; /* Index of the next valid node */
  struct svo_index leaf_head; /* Index of the next valid leaf attribute  */
  struct mem_allocator* allocator;
};

extern LOCAL_SYM void
svo_buffer_incore_init
  (struct mem_allocator* allocator,
   struct svo_buffer_incore* buf);

extern LOCAL_SYM void
svo_buffer_incore_release
  (struct svo_buffer_incore* buf);

extern LOCAL_SYM res_T
svo_buffer_incore_setup
  (struct svo_buffer_incore* buf,
   const uint16_t identifier,
   const size_t pagesize);

extern LOCAL_SYM res_T
svo_buffer_incore_copy
  (struct svo_buffer_incore* dst,
   const struct svo_buffer_incore* src);

extern LOCAL_SYM res_T
svo_buffer_incore_copy_and_clear
  (struct svo_buffer_incore* dst,
   struct svo_buffer_incore* src);

extern LOCAL_SYM res_T
svo_buffer_incore_copy_and_release
  (struct svo_buffer_incore* dst,
   struct svo_buffer_incore* src);

extern LOCAL_SYM res_T
svo_buffer_incore_alloc_nodes
  (struct svo_buffer_incore* buf,
   const size_t nnodes,
   struct svo_index* first_node, /* Index toward the 1st allocated node */
   struct svo_xnode** nodes, /* Allocated nodes */
   struct svo_node_attrib** attribs); /* Allocated attributes */

extern LOCAL_SYM res_T
svo_buffer_incore_alloc_leaf_attribs
  (struct svo_buffer_incore* buf,
   const size_t nleafs,
   struct svo_index* first_leaf, /* Index toward the 1st allocated attrib */
   struct svo_node_attrib** attribs); /* Allocated leaf attributes */

/* Allocate a node index in the current buffer page. Return RES_MEM_ERR if
 * the node index cannot be allocated in the current page. In this case one
 * have to alloc new nodes */
extern LOCAL_SYM res_T
svo_buffer_incore_alloc_far_index
  (struct svo_buffer_incore* buf,
   struct svo_index* index, /* Index toward the far index */
   struct svo_index** far_index); /* Allocated far index */

extern LOCAL_SYM void
svo_buffer_incore_clear
  (struct svo_buffer_incore* buf);

static INLINE int
svo_buffer_incore_is_empty(const struct svo_buffer_incore* buf)
{
  ASSERT(buf);
  return darray_node_page_size_get(&buf->node_pages) == 0;
}

static FINLINE struct svo_xnode*
svo_buffer_incore_get_node
  (struct svo_buffer_incore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT((size_t)id->ipage < darray_node_page_size_get(&buf->node_pages));
  ASSERT(id->ibuffer == buf->node_head.ibuffer);
  mem = darray_node_page_data_get(&buf->node_pages)[id->ipage];
  return ((struct svo_xnode*)mem) + id->inode;
}

static FINLINE struct svo_index*
svo_buffer_incore_get_far_index
  (struct svo_buffer_incore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT((size_t)id->ipage < darray_node_page_size_get(&buf->node_pages));
  ASSERT(id->ibuffer == buf->node_head.ibuffer);
  mem = darray_node_page_data_get(&buf->node_pages)[id->ipage];
  mem += id->inode * sizeof(struct svo_xnode);
  return (struct svo_index*)mem;
}

static FINLINE struct svo_node_attrib*
svo_buffer_incore_get_node_attrib
  (struct svo_buffer_incore* buf,
   const struct svo_index* id)
{
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(id->ipage < darray_attr_page_size_get(&buf->attr_pages));
  ASSERT(id->ibuffer == buf->node_head.ibuffer);
  return darray_attr_page_data_get(&buf->attr_pages)[id->ipage] + id->inode;
}

static FINLINE struct svo_node_attrib*
svo_buffer_incore_get_leaf_attrib
  (struct svo_buffer_incore* buf,
   const struct svo_index* id)
{
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(id->ipage < darray_attr_page_size_get(&buf->leaf_pages));
  ASSERT(id->ibuffer == buf->leaf_head.ibuffer);
  return darray_attr_page_data_get(&buf->leaf_pages)[id->ipage] + id->inode;
}

static INLINE void
svo_buffer_incore_get_child_index
  (struct svo_buffer_incore* buf,
   const struct svo_index* id,
   const int ichild, /* in [0, 7] */
   struct svo_index* out_id)
{
  struct svo_xnode* node = NULL;
  const int ichild_flag = BIT(ichild);
  int ichild_off;
  ASSERT(ichild >= 0 && ichild < 8 && id && buf && out_id);
  ASSERT(id->ibuffer == buf->node_head.ibuffer);

  node = svo_buffer_incore_get_node(buf, id);
  ASSERT(node->is_valid & ichild_flag);

  /* Compute the child offset from the first child node */
  ichild_off = popcount((uint8_t)((ichild_flag-1) & (int)node->is_valid));

  if(!(node->children_offset & SVO_XNODE_FAR_INDEX)) {
    out_id->ipage = id->ipage;
    out_id->inode =
      (node_id_T)(id->inode - node->children_offset + (size_t)ichild_off);
    out_id->ibuffer = buf->node_head.ibuffer;
  } else {
    const uint32_t offset = node->children_offset & SVO_XNODE_MASK;
    *out_id = *((struct svo_index*)(node + offset));
    out_id->inode = (node_id_T)(out_id->inode + (size_t)ichild_off);
  }
}

#endif /* VXL_BE_SVO_BUFFER_INCORE_H */

