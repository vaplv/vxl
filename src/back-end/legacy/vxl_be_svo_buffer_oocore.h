/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXL_BE_SVO_BUFFER_OOCORE_H
#define VXL_BE_SVO_BUFFER_OOCORE_H

#include "vxl_be_svo_buffer_incore.h" /* For svo_index */
#include <rsys/rsys.h>

/*
 * Out-of-Core SVO buffer. Once initialised, only allocation functions can be
 * invoked. To access the buffer data, one has to call the
 * svo_buffer_oocore_finalize function
 */

struct svo_buffer_oocore {
  size_t pagesize; /* Memory page size in bytes */

  /* Number of per page nodes. Note that since the size of svo_xnode and
   * svo_node_attrib are the same there is the same number of per page attribs
   * than the number of per page nodes. */
  size_t page_nnodes;

  FILE* stream_nodes;
  FILE* stream_attrs;
  FILE* stream_leafs;

  char* nodes; /* Raw memory containing nodes far indices */
  char* attrs; /* Raw memory containing node attribs */
  char* leafs; /* Raw memory containing leaf attribs */

  /* Indices toward an in memory entry. Before the finalization, they index the
   * next free entry. After the finalization, they are no more use */
  struct svo_index node_head;
  struct svo_index leaf_head;
  struct svo_index attr_head;

  /* Index toward the end of the buffer. These indices are initialize during the
   * finalization of the buffer */
  struct svo_index node_tail;
  struct svo_index attr_tail;
  struct svo_index leaf_tail;

  /* Memory mapping of the out-of-core data. The pointers are initialized on
   * buffer finalization */
  void* nodes_mmap;
  void* attrs_mmap;
  void* leafs_mmap;

  /* Size of the mapped memory */
  size_t nodes_mmap_len;
  size_t attrs_mmap_len;
  size_t leafs_mmap_len;

  struct mem_allocator* allocator;
};

extern LOCAL_SYM void
svo_buffer_oocore_init
  (struct mem_allocator* allocator,
   struct svo_buffer_oocore* buf);

extern LOCAL_SYM void
svo_buffer_oocore_release
  (struct svo_buffer_oocore* buf);

extern LOCAL_SYM res_T
svo_buffer_oocore_setup
  (struct svo_buffer_oocore* buf,
   const uint16_t identifier,
   const size_t pagesize);

extern LOCAL_SYM res_T
svo_buffer_oocore_alloc_nodes
  (struct svo_buffer_oocore* buf,
   const size_t nnodes,
   struct svo_index* first_node, /* Index toward the 1st allocated node */
   struct svo_xnode** nodes, /* Allocated nodes */
   struct svo_node_attrib** attribs); /* Allocated node attribs */

/* Allocate a node index in the current buffer page. Return RES_MEM_ERR if
 * the node index cannot be allocated in the current page. In this case one
 * have to alloc new nodes */
extern LOCAL_SYM res_T
svo_buffer_oocore_alloc_far_index
  (struct svo_buffer_oocore* buf,
   struct svo_index* index, /* Index toward the allocated far index */
   struct svo_index** far_index); /* Allocated far index */

extern LOCAL_SYM res_T
svo_buffer_oocore_alloc_leaf_attribs
  (struct svo_buffer_oocore* buf,
   const size_t nleafs,
   struct svo_index* first_attrib, /* Index toward the 1st allocated attrib */
   struct svo_node_attrib** attribs); /* Allocated leaf attributes */

/* Once finalized, the buffer data can be randomly accessed through getters */
extern LOCAL_SYM res_T
svo_buffer_oocore_finalize
  (struct svo_buffer_oocore* buf);

extern LOCAL_SYM res_T
svo_buffer_oocore_copy_incore
  (struct svo_buffer_oocore* src,
   struct svo_buffer_incore* dst);

extern LOCAL_SYM int
svo_buffer_oocore_is_finalized
  (const struct svo_buffer_oocore* buf);

static FINLINE struct svo_xnode*
svo_buffer_oocore_get_node
  (struct svo_buffer_oocore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(svo_buffer_oocore_is_finalized(buf));
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(buf->node_head.ibuffer == id->ibuffer);
  mem = (char*)buf->nodes_mmap + id->ipage*buf->pagesize;
  return (struct svo_xnode*)mem + id->inode;
}

static FINLINE struct svo_node_attrib*
svo_buffer_oocore_get_node_attrib
  (struct svo_buffer_oocore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(svo_buffer_oocore_is_finalized(buf));
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(buf->attr_head.ibuffer == id->ibuffer);
  mem = (char*)buf->attrs_mmap + id->ipage*buf->pagesize;
  return (struct svo_node_attrib*)mem + id->inode;
}

static FINLINE struct svo_index*
svo_buffer_oocore_get_far_index
  (struct svo_buffer_oocore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(svo_buffer_oocore_is_finalized(buf));
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(buf->node_head.ibuffer == id->ibuffer);
  mem = (char*)buf->nodes_mmap + id->ipage*buf->pagesize;
  return (struct svo_index*)mem + id->inode;
}

static FINLINE struct svo_node_attrib*
svo_buffer_oocore_get_leaf_attrib
  (struct svo_buffer_oocore* buf,
   const struct svo_index* id)
{
  char* mem;
  ASSERT(svo_buffer_oocore_is_finalized(buf));
  ASSERT(buf && id && id->inode < buf->page_nnodes);
  ASSERT(buf->leaf_head.ibuffer == id->ibuffer);
  mem = (char*)buf->leafs_mmap + id->ipage*buf->pagesize;
  return (struct svo_node_attrib*)mem + id->inode;
}

static FINLINE void
svo_buffer_oocore_get_child_index
  (struct svo_buffer_oocore* buf,
   const struct svo_index* id,
   const int ichild, /* in [0, 7] */
   struct svo_index* out_id)
{
  struct svo_xnode* node = NULL;
  const int ichild_flag = BIT(ichild);
  int ichild_off;
  ASSERT(ichild >= 0 && ichild < 8 && id && buf && out_id);
  ASSERT(id->ibuffer == buf->node_head.ibuffer);

  node = svo_buffer_oocore_get_node(buf, id);
  ASSERT(node->is_valid & ichild_flag);

  /* Compute the child offset from the first child node */
  ichild_off = popcount((uint8_t)((ichild_flag-1) & (int)node->is_valid));

  if(!(node->children_offset & SVO_XNODE_FAR_INDEX)) {
    out_id->ipage = id->ipage;
    out_id->inode =
      (node_id_T)(id->inode - node->children_offset + (size_t)ichild_off);
    out_id->ibuffer = buf->node_head.ibuffer;
  } else {
    const uint32_t offset = node->children_offset & SVO_XNODE_MASK;
    *out_id = *((struct svo_index*)(node + offset));
    out_id->inode = (node_id_T)(out_id->inode + (size_t)ichild_off);
  }
}

#endif /* VXL_BE_SVO_BUFFER_OOCORE_H */

